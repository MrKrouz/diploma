﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public abstract class Acceso
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public abstract void AddRole(Acceso item);
        public abstract void RemoveRole(Acceso item);
        public abstract void RemoveAll();
        public abstract bool HasRole(Acceso items, string role);
        public abstract List<Acceso> GetRoles();

    }
}
