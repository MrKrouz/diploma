﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class Permiso : Acceso
    {
        
        public Permiso(int id, string descripcion)
        {
            this.Id = id;
            this.Descripcion = descripcion;
        }
        public Permiso() { }

        public override bool Equals(object obj)
        {
            if (obj is Permiso)
            {
                var that = obj as Permiso;
                return this.Id == that.Id && this.Descripcion == that.Descripcion;
            }
            return false;
        }

        public override void AddRole(Acceso item)
        {
            throw new NotImplementedException();
        }

        public override void RemoveRole(Acceso item)
        {
            throw new NotImplementedException();
        }

        public override void RemoveAll()
        {
            throw new NotImplementedException();
        }

        public override bool HasRole(Acceso item, string role)
        {
            throw new NotImplementedException();
        }

        public override List<Acceso> GetRoles()
        {
            throw new NotImplementedException();
        }
    }
}
