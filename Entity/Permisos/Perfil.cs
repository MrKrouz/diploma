﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class Perfil : Acceso
    {
        private List<Acceso> listaPermisos = new List<Acceso>();

        public Perfil(int id, string descripcion)
        {
            this.Id = id;
            this.Descripcion = descripcion;
        }
        public Perfil()
        {
        }

        public override bool Equals(object obj)
        {
            if (obj is Perfil)
            {
                var that = obj as Perfil;
                return this.Id == that.Id && this.Descripcion == that.Descripcion;
            }
            return false;
        }

        public override void AddRole(Acceso item)
        {
            listaPermisos.Add(item);
        }

        public override void RemoveRole(Acceso item)
        {
            listaPermisos.Remove(item);
        }

        public override void RemoveAll()
        {
            listaPermisos.Clear();
        }

        public override bool HasRole(Acceso items, string role)
        {
            bool tiene = false;
            foreach (var item in items.GetRoles())
            {
                if (item is Permiso)
                {
                    if (item.Descripcion.Equals(role)) return true;
                }
                else if (item is Perfil)
                {
                    if (item.HasRole(item, role) == false)
                    {
                        tiene = false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return tiene;
        }

        public override List<Acceso> GetRoles()
        {
            return this.listaPermisos;
        }
    }
}
