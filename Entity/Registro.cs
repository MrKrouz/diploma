﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class Registro
    {
        public int IdRegistro {get; set;}
        public string Evento { get; set; }
        public DateTime Fecha { get; set; }
    }
}
