﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class Cliente : Persona
    {
        public string Direccion { get; set; }
        public int Telefono { get; set; }
        public string ClienteCMB
        {
            get
            {
                return this.Apellido + "-" + this.Dni;
            }
        }
    }
}
