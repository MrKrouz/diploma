﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class Mueble : Elemento
    {
        public EE.MateriaPrima Madera { get; set; } = new EE.MateriaPrima();
        public double MedidaH { get; set; }
        public double MedidaL { get; set; }
        public double MedidaP { get; set; }
    }
}
