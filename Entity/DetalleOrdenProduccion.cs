﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class DetalleOrdenProduccion
    {
        public int Id { get; set; }
        public Mueble Mueble { get; set; }
        public int Cantidad { get; set; }
    }
}
