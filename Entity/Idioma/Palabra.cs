﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    public class Palabra : Lenguaje
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        public Palabra (string nombre, string control)
        {
            this.Descripcion = nombre;
            this.Control = control;
        }

        public override void ActualizarPalabra(Lenguaje PalabraVieja, Lenguaje PalabraNueva)
        {
        }

        public override void AgregarPalabra(Lenguaje Palabra)
        {
        }

        public override List<Lenguaje> ListarPalabras()
        {
            return null;
        }

        public override void RemoverPalabra(Lenguaje Palabra)
        {
        }

        public override bool Equals(object obj)
        {
            if (obj is Palabra)
            {
                var that = obj as Palabra;
                return this.ID == that.ID && this.Descripcion == that.Descripcion && this.Control == that.Control;
            }
            return false;
        }
    }
}
