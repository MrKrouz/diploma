﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    public class Idioma : Lenguaje
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        private List<Lenguaje> palabras = new List<Lenguaje>();

        public Idioma()
        {
        }
        public Idioma (string nombre, System.Globalization.CultureInfo cultura)
        {
            this.Descripcion = nombre;
            this.Cultura = cultura;
        }

        public override void ActualizarPalabra(Lenguaje PalabraVieja, Lenguaje PalabraNueva)
        {
            palabras.RemoveAt(palabras.FindIndex(p => p.ID == PalabraVieja.ID));
            palabras.Add(PalabraNueva);
        }

        public override void AgregarPalabra(Lenguaje Palabra)
        {
            palabras.Add(Palabra);
        }

        public override List<Lenguaje> ListarPalabras()
        {
            return palabras;
        }

        public override void RemoverPalabra(Lenguaje Palabra)
        {
            palabras.RemoveAt(palabras.FindIndex(p => p.ID == Palabra.ID));
        }

        public override bool Equals(object obj)
        {
            if (obj is Idioma)
            {
                var that = obj as Idioma;
                return this.ID == that.ID && this.Descripcion == that.Descripcion && this.Cultura == that.Cultura;
            }
            return false;
        }
    }
}
