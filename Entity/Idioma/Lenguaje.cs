﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public abstract class Lenguaje
    {
        public int ID { get; set; }
        public string  Descripcion { get; set; }
        public string Control { get; set; }
        public System.Globalization.CultureInfo Cultura { get; set; }

        public abstract void AgregarPalabra(Lenguaje Palabra);
        public abstract void RemoverPalabra(Lenguaje Palabra);
        public abstract void ActualizarPalabra(Lenguaje PalabraVieja, Lenguaje PalabraNueva);
        public abstract List<Lenguaje> ListarPalabras();

    }
}
