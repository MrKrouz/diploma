﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class Usuario : Persona
    {
        public string UserName { get; set; }
        public byte[] Password { get; set; }
        public Acceso Perfil { get; set; }
        public Lenguaje Idioma { get; set; }
        public override string ToString()
        {
            return Nombre + "-" + Apellido + "-" + UserName + "-" + Dni;
        }
    }
}
