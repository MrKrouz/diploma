﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EE
{
    public interface iObservador
    {
        void Subscribirse(System.Windows.Forms.Form form);
        void DeSubscribirse(System.Windows.Forms.Form form);
        void Limpiar();
    }
}
