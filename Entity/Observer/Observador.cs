﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EE
{
    public class Observador : iObservador
    {
        private List<Form> Forms = new List<Form>();
        public void DeSubscribirse(Form form)
        {
            Forms.Remove(form);
        }

        public void Limpiar()
        {
            Forms.Clear();
        }

        public void Subscribirse(Form form)
        {
            Forms.Add(form);
        }

        public List<Form> DevolverSubscriptos()
        {
            return this.Forms;
        }

        public void ActualizarForms(Lenguaje lenguaje)
        {
            if (lenguaje != null)
            {
                foreach (Form item in this.DevolverSubscriptos())
                {
                    var controles = item.Controls;
                    foreach (var item2 in lenguaje.ListarPalabras())
                    {
                        if (item2 is Palabra)
                        {
                            var controlMod = controles.Find(item2.Control, true).FirstOrDefault();
                            if (controlMod != null)
                                controlMod.Text = item2.Descripcion;
                        }
                    }
                }
                Application.CurrentCulture = lenguaje.Cultura;
            }
        }
    }
}
