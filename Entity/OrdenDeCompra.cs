﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class OrdenDeCompra
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public Usuario Empleado { get; set; }
        public List<DetalleOrdenDeCompra> Detalle { get; set; } = new List<DetalleOrdenDeCompra>();
        private double _montoTotal { get; set; }
        public double MontoTotal
        {
            get
            {
                if (_montoTotal == 0)
                {
                    foreach (var item in Detalle)
                    {
                        _montoTotal += item.PrecioTotal;
                    }
                }
                return _montoTotal;
            }
        }
        public InformeMRP Informe { get; set; }
    }
}
