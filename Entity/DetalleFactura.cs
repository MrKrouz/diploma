﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class DetalleFactura
    {
        public int Id { get; set; }
        private double _PrecioTotal { get; set; }
        public double PrecioTotal
        {
            get
            {
                return Mueble.Precio * Cantidad;
            }
        }
        public Mueble Mueble { get; set; }
        public int Cantidad { get; set; }
    }
}
