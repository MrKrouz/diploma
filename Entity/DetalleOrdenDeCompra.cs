﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class DetalleOrdenDeCompra
    {
        public int Id { get; set; }
        public double Cantidad { get; set; }
        public MateriaPrima Material { get; set; }
        public double PrecioTotal
        {
            get
            {
                return Material.Precio * Cantidad;
            }
        }
    }
}
