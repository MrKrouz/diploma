﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class InformeMRP
    {
        public int Id { get; set; }
        public DateTime FechaGenerado { get; set; }
        public List<DetalleMRP> Detalle { get; set; } = new List<DetalleMRP>();
    }
}
