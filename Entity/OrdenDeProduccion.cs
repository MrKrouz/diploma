﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class OrdenDeProduccion
    {
        public int Id { get; set; }
        public List<DetalleOrdenProduccion> Detalle = new List<DetalleOrdenProduccion>();
        public Usuario Empleado { get; set; } = new Usuario();
        public DateTime Fecha { get; set; }

        public string DetalleCMB
        {
            get
            {
                return Fecha.ToShortDateString() + " " + Empleado.Apellido;
            }
        }
    }
}
