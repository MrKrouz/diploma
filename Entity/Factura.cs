﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE
{
    public class Factura
    {
        public int Id { get; set; }
        public Usuario Empleado { get; set; } = new Usuario();
        public Cliente Cliente { get; set; } = new Cliente();
        private double _ImporteTotal;
        public double ImporteTotal
        {
            get
            {
                if (_ImporteTotal == 0)
                {
                    foreach (var item in Detalle)
                    {
                        _ImporteTotal += item.PrecioTotal;
                    }
                }
                return _ImporteTotal;
            }
        }
        public List<EE.DetalleFactura> Detalle { get; set; } = new List<DetalleFactura>();


    }
}
