﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VentasUIGenerarFactura
{
    public partial class VentasUIGenerarFactura : Form
    {
        EE.Factura fac = new EE.Factura();

        public VentasUIGenerarFactura()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text != string.Empty && textBox1.Text != string.Empty)
            {
                EE.Mueble item = (EE.Mueble)comboBox2.SelectedItem;
                if (fac.Detalle.Find(i => i.Mueble.Id == (int)comboBox2.SelectedValue) == null)
                {
                    fac.Detalle.Add(new EE.DetalleFactura { Cantidad = Convert.ToInt32(textBox1.Text), Mueble = item });
                    listBox1.Items.Add(item.Descripcion + " " + textBox1.Text);
                }
                else
                {
                    var cantidad = fac.Detalle.Find(i => i.Mueble.Id == (int)comboBox2.SelectedValue).Cantidad;
                    listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + cantidad));
                    listBox1.Items.Add(item.Descripcion + " " + (cantidad + Convert.ToInt32(textBox1.Text)));
                    fac.Detalle.Find(i => i.Mueble.Id == (int)comboBox2.SelectedValue).Cantidad += Convert.ToInt32(textBox1.Text);

                }
            }
            else
            {
                MessageBox.Show("Error al cargar el mueble a la factura");
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            EE.Mueble item = (EE.Mueble)comboBox2.SelectedItem;

            if (fac.Detalle.Find(i => i.Mueble.Id == (int)comboBox2.SelectedValue) == null)
            {
                MessageBox.Show("No existen muebles cargados");
            }
            else
            {
                var detalle = fac.Detalle.Find(i => i.Mueble.Id == (int)comboBox2.SelectedValue);
                if (detalle.Cantidad == 0 || detalle.Cantidad - Convert.ToInt32(textBox1.Text) < 0)
                {
                    listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + detalle.Cantidad));
                    fac.Detalle.Remove(detalle);
                }
                else
                {
                    listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + detalle.Cantidad));
                    detalle.Cantidad -= Convert.ToInt32(textBox1.Text);
                    listBox1.Items.Add(item.Descripcion + " " + detalle.Cantidad);
                }

            }
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            var coreVentas = new CoreVentas.coreVentas();
            var coreProd = new CoreProduccion.bllProduccion();
            fac.Empleado.Id = SecUtils.Singleton.Instance.GetInstance().Id;
            fac.Cliente = (EE.Cliente)comboBox1.SelectedItem;
            foreach (var item in fac.Detalle)
            {
                item.Mueble = coreProd.GetMuebles().Find(m => m.Id == item.Mueble.Id);
            }
            if (fac.Detalle.Count > 0)
            {
                coreVentas.GenerarFactura(fac);
            }
            else
            {
                MessageBox.Show("No se puede generar una factura sin elementos");
            }
            if (fac.Id != 0)
            {
                string outputFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Factura Nro - " + fac.Id.ToString() + ".pdf");
                using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (Document doc = new Document(PageSize.A4))
                    {
                        using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                        {
                            doc.Open();
                            PdfPTable t1 = new PdfPTable(4);
                            t1.DefaultCell.Border = 0;
                            t1.AddCell("Numero:");
                            t1.AddCell(fac.Id.ToString());
                            t1.AddCell("Fecha:");
                            t1.AddCell(System.DateTime.Now.ToShortDateString());
                            doc.Add(t1);
                            PdfPTable tcentro = new PdfPTable(5);
                            tcentro.DefaultCell.Border = 0;
                            tcentro.DefaultCell.BorderWidthBottom = 1;
                            tcentro.DefaultCell.BorderColorBottom = BaseColor.RED;
                            tcentro.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            tcentro.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            tcentro.AddCell(new PdfPCell(new Phrase("B")) { Border = 0 });
                            tcentro.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            tcentro.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            doc.Add(tcentro);

                            PdfPTable t2 = new PdfPTable(4);
                            t2.DefaultCell.Border = 0;
                            t2.DefaultCell.BorderWidthBottom = 1;
                            t2.DefaultCell.BorderColorBottom = BaseColor.RED;
                            t2.AddCell(new PdfPCell(new Phrase("Cliente")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell("Nombre");
                            t2.AddCell(fac.Cliente.Nombre);
                            t2.AddCell("Apellido:");
                            t2.AddCell(fac.Cliente.Apellido);
                            t2.AddCell("Direccion:");
                            t2.AddCell(fac.Cliente.Direccion);
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            doc.Add(t2);

                            PdfPTable t3 = new PdfPTable(1);
                            t3.DefaultCell.Border = 0;
                            t3.AddCell("Detalle");
                            t3.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            doc.Add(t3);

                            PdfPTable t4 = new PdfPTable(4);
                            t4.DefaultCell.Border = 0;
                            t4.DefaultCell.BorderWidthBottom = 1;
                            t4.DefaultCell.BorderColorBottom = BaseColor.RED;
                            t4.AddCell("Cantidad");
                            t4.AddCell("Descripcion");
                            t4.AddCell("Precio U.");
                            t4.AddCell("Total");
                            doc.Add(t4);

                            PdfPTable t = new PdfPTable(4);
                            t.DefaultCell.Border = 0;
                            t.DefaultCell.BorderWidthBottom = 1;
                            t.DefaultCell.BorderColorBottom = BaseColor.RED;
                            foreach (var item in fac.Detalle)
                            {
                                t.AddCell(item.Cantidad.ToString());
                                t.AddCell(item.Mueble.Descripcion);
                                t.AddCell(item.Mueble.Precio.ToString());
                                t.AddCell(item.PrecioTotal.ToString());
                                t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                                t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                                t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                                t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            }
                            doc.Add(t);

                            PdfPTable tfin = new PdfPTable(4);
                            tfin.DefaultCell.Border = 0;
                            tfin.DefaultCell.BorderWidthBottom = 1;
                            tfin.DefaultCell.BorderColorBottom = BaseColor.RED;
                            tfin.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            tfin.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            tfin.AddCell("Importe total");
                            tfin.AddCell(fac.ImporteTotal.ToString());
                            doc.Add(tfin);
                            doc.Close();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Factura no generada debido a falta de stock.");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "VentasUIHome").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();
        }

        private void VentasUIGenerarFactura_Load(object sender, EventArgs e)
        {
            var coreVentas = new CoreVentas.coreVentas();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "ClienteCMB";
            comboBox1.DataSource = coreVentas.GetClientes();
            comboBox1.Text = string.Empty;
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Descripcion";
            comboBox2.DataSource = coreProd.GetMuebles();
            comboBox2.Text = string.Empty;
        }
    }
}
