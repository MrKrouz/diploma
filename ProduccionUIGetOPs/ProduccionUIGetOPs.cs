﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIGetOPs
{
    public partial class ProduccionUIGetOPs : FormBase.FormBase
    {
        public ProduccionUIGetOPs()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Id";
            comboBox1.DataSource = coreProd.ListarOrdenesDeProduccion();
            comboBox1.Text = string.Empty;
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            listBox1.Items.Clear();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            EE.OrdenDeProduccion item = (EE.OrdenDeProduccion)comboBox1.SelectedItem;
            if (item.Empleado != null)
            {
                textBox1.Text = item.Empleado.Apellido;
            }
            textBox2.Text = item.Fecha.ToString();
            foreach(var item2 in item.Detalle)
            {
                listBox1.Items.Add(item2.Mueble.Descripcion + " " + item2.Cantidad);
            }
            
        }

        private void btnRechazar_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
