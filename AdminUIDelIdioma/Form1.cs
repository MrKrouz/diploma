﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIDelIdioma
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = new CoreLang.CoreLang().GetLangs();
            comboBox1.Text = string.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var item = (EE.Idioma)comboBox1.SelectedItem;
            new CoreLang.CoreLang().DelLang(item.ID);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();
        }
    }
}
