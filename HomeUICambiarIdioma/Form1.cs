﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeUICambiarIdioma
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var coreLang = new CoreLang.CoreLang();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreLang.GetLangs();
            comboBox1.Text = string.Empty;
        }

        private void btnUpd_Click(object sender, EventArgs e)
        {
            if(comboBox1.Text != string.Empty)
            {

            SecUtils.Singleton.Instance.GetInstance().Idioma = (EE.Idioma)comboBox1.SelectedItem;
            var core = new CoreSec.CoreSec();
            core.UpdUser(SecUtils.Singleton.Instance.GetInstance());
            MessageBox.Show("Idioma seleccionado");
            }
            else
            {
                MessageBox.Show("Debe seleccionar un idioma");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "Home").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
