﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilidades
{
    public class BackUps
    {
        protected readonly string filePath = Path.GetDirectoryName(Environment.CurrentDirectory);
        public void GenerateBackUp()
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().CreateBackUp;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@Path", filePath);
            cnX.Open();
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
    }
}
