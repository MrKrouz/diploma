﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Utilidades
{
    public class Logger
    {
        protected readonly object lockObj = new object();
        string outputFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Logs.txt");
        public void LogDB(string mensaje, DateTime fechaLog)
        {
            try
            {
                var cn = new ConnectionString().GetConnection();
                var db = new SqlConnection(cn);
                var cmd = new SqlCommand(new StoredProcedures().WriteLog, db) { CommandType = System.Data.CommandType.StoredProcedure };
                db.Open();
                cmd.Parameters.AddWithValue("@Log", mensaje);
                cmd.Parameters.AddWithValue("@Fecha", fechaLog);
                cmd.ExecuteNonQuery();
                db.Close();
            }
            catch (SqlException ex)
            {
                LogFile(mensaje, fechaLog, ex.Message);
            }
        }
        public void LogFile(string mensaje, DateTime fechaLog, string errorDB)
        {
            lock (lockObj)
            {
                using (StreamWriter streamWriter = new StreamWriter(outputFile, true))
                {
                    streamWriter.WriteLine(mensaje + " " + fechaLog + " " + errorDB);
                    streamWriter.Close();
                }
            }
        }

        public List<EE.Registro> GetLogs(DateTime fecha)
        {
            var cn = new ConnectionString().GetConnection();
            var db = new SqlConnection(cn);
            var cmd = new SqlCommand(new StoredProcedures().GetLogs, db);
            cmd.Parameters.AddWithValue("@Fecha", fecha);
            db.Open();
            var reader = cmd.ExecuteReader();
            List<EE.Registro> lista = new List<EE.Registro>();
            while (reader.Read())
            {
                lista.Add(new EE.Registro { IdRegistro = reader.GetInt32(0), Evento = reader.GetString(1), Fecha = reader.GetDateTime(2) });
            }
            db.Close();
            return lista;
        }
    }
}
