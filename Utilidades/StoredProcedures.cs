﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilidades
{
    public sealed class StoredProcedures
    {
        #region User
        public readonly string GetUserPassword = "GetUserPassword";
        public readonly string ChangePassword = "ChangePassword";
        public readonly string GetUsuarios = "GetUsers";
        public readonly string AddUser = "AddUser";
        public readonly string UpdUser = "UpdateUser";
        public readonly string DelUser = "DelUser";
        public readonly string GetDVV = "GetDVV";
        public readonly string AddDVV = "AddDVV";
        #endregion
        #region Permiso
        public readonly string AddPermiso = "AddPermiso";
        public readonly string AsignarPermiso = "AsignarPermiso";
        public readonly string UpdPermiso = "UpdPermiso";
        public readonly string DelPermiso = "DelPermiso";
        public readonly string GetPermisos = "GetPermisos";
        #endregion
        #region Perfil
        public readonly string AsignarPerfil = "AsignarPerfil";
        public readonly string RelacionarPerfil = "RelacionarPerfil";
        public readonly string AddPerfil = "AddPerfil";
        public readonly string AddPerfilCompuesto = "AddPerfilCompuesto";
        public readonly string UpdPerfil = "UpdPerfil";
        public readonly string DelPerfil = "DelPerfil";
        public readonly string BorrarRelaciones = "BorrarRelaciones";
        public readonly string GetPerfiles = "GetPerfiles";
        public readonly string GetPerfilesRel = "GetPerfilesRel";
        public readonly string GetPerfilesBase = "GetPerfilesBase";
        public readonly string GetPerfilByUsuario = "GetPerfilByUsuario";
        #endregion
        #region Logs
        public readonly string WriteLog = "WriteLog";
        public readonly string GetLogs = "GetLogs";
        #endregion
        #region Sec
        public readonly string GetDVH = "GetDVH";
        public readonly string SaveDVH = "SaveDVH";
        public readonly string GenerateBackUp = "GenerarBackUp";
        #endregion
        #region Language
        public readonly string AddLang = "AddLang";
        public readonly string AddWords = "AddWord";
        public readonly string UpdLang = "UpdateLang";
        public readonly string UpdWords = "UpdWords";
        public readonly string DelLang = "DelLang";
        public readonly string GetLangs = "GetLangs";
        public readonly string GetWords = "GetWords";
        #endregion
        #region BackUps
        public readonly string CreateBackUp = "CreateBackUp";
        public readonly string RestoreBackUp = "RestoreBackUp";

        #endregion
        #region Produccion
        public readonly string AddOrdenDeCompra = "AddOrdenDeCompra";
        public readonly string AddDetalleOC = "AddDetalleOC";
        public readonly string EliminarOrdenDeCompra = "EliminarOrdenDeCompra";
        public readonly string ModificarOrdenDeCompra = "ModificarOrdenDeCompra";
        public readonly string ModificarDetalleOC = "ModificarDetalleOC";
        public readonly string GetOrdenesDeProduccion = "GetOrdenesDeProduccion";
        public readonly string GetDetallesOP = "GetDetallesOP";
        public readonly string GetOrdenesDeCompra = "GetOrdenesDeCompra";
        public readonly string GetDetallesOC = "GetDetallesOC";
        public readonly string AddMaterial = "AddMaterial";
        public readonly string UpdMaterial = "UpdMaterial";
        public readonly string DelMaterial = "DelMaterial";
        public readonly string GetMateriales = "GetMateriales";
        public readonly string AddMueble = "AddMueble";
        public readonly string UpdMueble = "UpdMueble";
        public readonly string DelMueble = "DelMueble";
        public readonly string GetMuebles = "GetMuebles";
        #endregion
        #region Informes
        public readonly string GenerarInforme = "GenerarInforme";
        public readonly string CargarDetalle = "CargarDetalle";
        public readonly string GetInforme = "GetInforme";
        public readonly string GetDetalle = "GetDetalle";
        #endregion
        #region Ventas
        public readonly string CargarFactura = "CargarFactura";
        public readonly string CargarDetalleFactura = "CargarDetalleFactura";
        public readonly string AddCliente = "AddCliente";
        public readonly string UpdCliente = "UpdCliente";
        public readonly string GetCliente = "GetCliente";
        public readonly string AddOrdenProduccion = "AddOrdenProduccion";
        public readonly string AddDetalleOP = "AddDetalleOP";
        #endregion
    }
}
