﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Utilidades
{
    public sealed class ConnectionString
    {
        public string GetConnection()
        {
            //DB Usuarios
            return ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString;
        }
    }
}
