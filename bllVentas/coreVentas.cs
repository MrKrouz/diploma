﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreVentas
{
    public class coreVentas
    {
        public void AltaCliente(EE.Cliente cliente)
        {
            var dbVentas = new DbVentas.dbVentas();
            dbVentas.AddCliente(cliente);
        }
        public void GenerarFactura(EE.Factura factura)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            var muebles = coreProd.GetMuebles();
            foreach(var item in factura.Detalle)
            {
                var mueble = muebles.Find(m => m.Id == item.Mueble.Id);
                if (mueble.Stock >= item.Cantidad)
                {
                    mueble.Stock -= item.Cantidad;
                }
                else
                {
                    return;
                }
                coreProd.UpdMueble(mueble);
            }
            var dbVentas = new DbVentas.dbVentas();
            dbVentas.CargarFactura(factura);
        }
        public void ModificarCliente(EE.Cliente cliente)
        {
            var dbVentas = new DbVentas.dbVentas();
            dbVentas.ModificarCliente(cliente);
        }
        public List<EE.Cliente> GetClientes()
        {
            var dbVentas = new DbVentas.dbVentas();
            return dbVentas.GetClientes();
        }
        public void GenerarOrdenDeProduccion(EE.OrdenDeProduccion OP)
        {
            var dbVentas = new DbVentas.dbVentas();
            dbVentas.GenerarOrdenDeProduccion(OP);
        }

        public void generarLogAYUDAAAA()
        {
            foreach(var item in SecUtils.Singleton.watcher.DevolverSubscriptos())
            {
                new Utilidades.Logger().LogFile(item.Name, System.DateTime.Now, " ");
            }
        }
    }
}
