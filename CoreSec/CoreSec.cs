﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSec
{
    public class CoreSec
    {
        #region UserRelated
        public bool LogIn(string userName, string password)
        {
            var user = new EE.Usuario();
            user.Password = new SecUtils.Crypto().Encriptar(password);
            user.UserName = userName;
            if (new DBSec.DBSec().LogIn(user.UserName, user.Password))
            {
                user = GetUsers().Where(u => u.UserName == user.UserName.ToLower()).FirstOrDefault();
                SecUtils.Singleton.Instance.LogIn(user);
                return true;
            }
            else
            {
                return false;
            }
        }
        public void LogOff(EE.Usuario user)
        {
            if (SecUtils.Singleton.Instance.GetInstance() != null)
            {
                SecUtils.Singleton.Instance.LogOut();
            }
        }
        public void ChangePassword(EE.Usuario user, string newPass)
        {
            new DBSec.DBSec().ChangePassword(user, new SecUtils.Crypto().Encriptar(newPass));
        }
        public List<EE.Usuario> GetUsers()
        {
            var dbSec = new DBSec.DBSec();
            var listaUsuarios = dbSec.GetUsers();
            var coreLang = new CoreLang.CoreLang();
            foreach (var item in listaUsuarios)
            {
                GetPerfilByUsuario(item);
                item.Idioma = coreLang.GetLangById(item.Idioma.ID);
            }
            return listaUsuarios;
        }
        public List<EE.Usuario> GetUsersClean()
        {
            var dbSec = new DBSec.DBSec();
            var listaUsuarios = dbSec.GetUsers();
            return listaUsuarios;
        }
        public void AddUser(EE.Usuario user)
        {
            new DBSec.DBSec().AddUser(user);
            new DBSec.DBSec().AddDVV(new SecUtils.Digitos().GenerarDVV(GetUsers()));
            VerificarDVV();
        }
        private void VerificarDVV()
        {
            var dvv = new SecUtils.Digitos().GenerarDVV(GetUsers());
            if (new DBSec.DBSec().GetDVV() != dvv)
            {
                new Utilidades.Logger().LogDB("Tabla de usuarios corrompida.", System.DateTime.Now);
            }
        }
        public void UpdUser(EE.Usuario user)
        {
            new DBSec.DBSec().UpdUser(user);
        }
        public void DelUser(EE.Usuario user)
        {
            new DBSec.DBSec().DelUser(user);
        }
        #endregion
        #region Permisos
        public void AltaPermiso(EE.Permiso permiso, int idPerfil)
        {
            new DBSec.DBSec().AltaPermiso(permiso, idPerfil);
        }
        public void ModificarPermisos(EE.Permiso permiso)
        {
            new DBSec.DBSec().ModificarPermisos(permiso);
        }
        public void BajaPermiso(EE.Permiso permiso)
        {
            new DBSec.DBSec().BajaPermiso(permiso);
        }
        #endregion
        #region Perfiles
        public void AltaPerfil(EE.Acceso perfil)
        {
            new DBSec.DBSec().AltaPerfil(perfil);
        }
        public void AsignarPerfil(EE.Acceso perfil, EE.Usuario usuario)
        {
            new DBSec.DBSec().AsignarPerfil(perfil.Id, usuario.Id);
        }
        public void ModificarPerfil(EE.Acceso perfil)
        {
            foreach (var item in perfil.GetRoles())
            {
                if (item is EE.Permiso)
                {
                    new DBSec.DBSec().AsignarPermiso(item.Id, perfil.Id);
                }
                if (item is EE.Perfil)
                {
                    new DBSec.DBSec().RelacionarPerfiles(perfil.Id, item.Id);
                }
            }
        }
        public void BajaPerfil(EE.Acceso perfil)
        {
            new DBSec.DBSec().BajaPerfil(perfil);
        }
        public void BorrarRelaciones(int IdPerfil)
        {
            new DBSec.DBSec().BorrarRelaciones(IdPerfil);
        }
        public List<EE.Acceso> GetPerfilesBase()
        {
            return new DBSec.DBSec().GetPerfilesBase();
        }
        public List<EE.Acceso> GetPerfiles()
        {
            return new DBSec.DBSec().GetPerfiles();
        }
        public void GetPerfilByUsuario(EE.Usuario usuario)
        {
            var dbSec = new DBSec.DBSec();
            usuario.Perfil = GetPerfiles().Where(p => p.Id == usuario.Perfil.Id).FirstOrDefault();
            var listaPerfiles = dbSec.GetPerfilByUsuario(usuario.Id);
            foreach (var item in listaPerfiles)
            {
                usuario.Perfil.AddRole(item);
            }
        }
        #endregion
        #region BackUps
        public void CreateBackUp()
        {
            new DBSec.DBSec().GenerateBackUp();
        }
        public void RestoreBackUp()
        {
            new DBSec.DBSec().RestoreBackUp();
        }
        #endregion
    }
}
