﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EE;
using SecUtils;

namespace FormBase
{
    public partial class FormBase : Form
    {
        public FormBase()
        {
            InitializeComponent();
        }

        private void FormBase_Load(object sender, EventArgs e)
        {

        }

        public void ActualizarControles()
        {
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);
        }

        private void españolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var coreLang = new CoreLang.CoreLang();
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            coreLang.ChangeLang(item.Name);
            SecUtils.Singleton.watcher.ActualizarForms(coreLang.GetLangs().Find(l => l.Descripcion == Name));
        }

        private void inglesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var coreLang = new CoreLang.CoreLang();
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            coreLang.ChangeLang(item.Name);
            SecUtils.Singleton.watcher.ActualizarForms(coreLang.GetLangs().Find(l => l.Descripcion == Name));
        }
    }
}
