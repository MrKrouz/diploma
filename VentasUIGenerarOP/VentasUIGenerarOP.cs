﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VentasUIGenerarOP
{
    public partial class VentasUIGenerarOP : Form
    {
        EE.OrdenDeProduccion op = new EE.OrdenDeProduccion();
        public VentasUIGenerarOP()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreProd.GetMuebles();
            comboBox1.Text = string.Empty;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != string.Empty)
            {
                int numero;
                if (textBox2.Text != string.Empty && int.TryParse(textBox2.Text, out numero))
                {

                    EE.Mueble item = (EE.Mueble)comboBox1.SelectedItem;
                    if (op.Detalle.Find(i => i.Mueble.Id == (int)comboBox1.SelectedValue) == null)
                    {
                        op.Detalle.Add(new EE.DetalleOrdenProduccion { Cantidad = Convert.ToInt32(textBox2.Text), Mueble = item });
                        listBox1.Items.Add(item.Descripcion + " " + textBox2.Text);
                    }
                    else
                    {
                        var cantidad = op.Detalle.Find(i => i.Mueble.Id == (int)comboBox1.SelectedValue).Cantidad;
                        listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + cantidad));
                        listBox1.Items.Add(item.Descripcion + " " + (cantidad + Convert.ToInt32(textBox2.Text)));
                        op.Detalle.Find(i => i.Mueble.Id == (int)comboBox1.SelectedValue).Cantidad += Convert.ToInt32(textBox2.Text);

                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar una cantidad");
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un mueble");
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != string.Empty)
            {
                int numero;
                if (textBox2.Text != string.Empty && int.TryParse(textBox2.Text, out numero))
                {

                    EE.Mueble item = (EE.Mueble)comboBox1.SelectedItem;

                    if (op.Detalle.Find(i => i.Mueble.Id == (int)comboBox1.SelectedValue) == null)
                    {
                        MessageBox.Show("No existen muebles cargados");
                    }
                    else
                    {
                        var detalle = op.Detalle.Find(i => i.Mueble.Id == (int)comboBox1.SelectedValue);
                        if (detalle.Cantidad == 0 || detalle.Cantidad - Convert.ToInt32(textBox2.Text) < 0)
                        {
                            listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + detalle.Cantidad));
                            op.Detalle.Remove(detalle);
                        }
                        else
                        {
                            listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + detalle.Cantidad));
                            detalle.Cantidad -= Convert.ToInt32(textBox2.Text);
                            listBox1.Items.Add(item.Descripcion + " " + detalle.Cantidad);
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Debe ingresar una cantidad");
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar un mueble");
            }
        }

    private void btnGenerar_Click(object sender, EventArgs e)
    {
        var coreVentas = new CoreVentas.coreVentas();
        var coreProd = new CoreProduccion.bllProduccion();
        op.Empleado.Id = SecUtils.Singleton.Instance.GetInstance().Id;
        foreach (var item in op.Detalle)
        {
            item.Mueble = coreProd.GetMuebles().Find(m => m.Id == item.Mueble.Id);
        }
        if (op.Detalle.Count > 0)
        {

            coreVentas.GenerarOrdenDeProduccion(op);
            string outputFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Orden de produccion - " + op.Id + ".pdf");
            using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (Document doc = new Document(PageSize.A4))
                {
                    using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                    {
                        doc.Open();
                        PdfPTable t = new PdfPTable(2);
                        t.DefaultCell.Border = 0;
                        t.DefaultCell.BorderWidthBottom = 1;
                        t.DefaultCell.BorderColorBottom = BaseColor.RED;
                        foreach (var item in op.Detalle)
                        {
                            t.AddCell("Descripcion: ");
                            t.AddCell(item.Mueble.Descripcion);
                            t.AddCell("Cantidad: ");
                            t.AddCell(item.Cantidad.ToString());
                            t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                        }
                        doc.Add(t);
                        doc.Close();
                    }
                }
            }
            MessageBox.Show("Orden de produccion generada. Archivo en el escritorio");
        }
        else
        {
            MessageBox.Show("Orden de produccion no generada debida a falta de elementos.");
        }

    }

    private void btnBack_Click(object sender, EventArgs e)
    {
        SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "VentasUIHome").Show();
        SecUtils.Singleton.watcher.DeSubscribirse(this);

        this.Close();
    }
}
}

