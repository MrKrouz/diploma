﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIDelMat
{
    public partial class ProduccionUIDelMat : FormBase.FormBase
    {
        public ProduccionUIDelMat()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            ActualizarCB();
        }

        private void ActualizarCB()
        {
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreProd.GetMateriales();
            comboBox1.Text = string.Empty;
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            var mat = coreProd.GetMateriales().Find(m => m.Id == (int)comboBox1.SelectedValue);
            textBox1.Text = mat.Descripcion;
            textBox2.Text = mat.Precio.ToString();
        }

        private void btnDelMat_Click(object sender, EventArgs e)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            if (comboBox1.Text != string.Empty)
            {
                coreProd.DelMaterial(new EE.MateriaPrima { Id = (int)comboBox1.SelectedValue });
                ActualizarCB();
                MessageBox.Show("Materia eliminado");
            }
            else
            {
                MessageBox.Show("Se debe seleccionar un mueble para eliminar");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CerrarForm();
        }

        private void CerrarForm()
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
