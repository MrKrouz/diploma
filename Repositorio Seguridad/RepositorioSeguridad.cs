﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Seguridad
{
    public class RepositorioSeguridad
    {

        public Perfil AltaPerfil(Perfil perfil)
        {
            return AltaPerfilDB(perfil);
        }

        public void AltaPermisos(int idPerfil, int idPermiso)
        {
            AsignarPermisosDB(idPerfil, idPermiso);
        }

        public void RemoverPermiso(int idPerfil)
        {
            BajaPermisoDB(idPerfil);
        }

        public void BajaPerfil(int idPerfil)
        {
            BajaPerfilDB(idPerfil);
        }

        public string Logearse(string usuario)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - Logearse {0} ->", usuario), DateTime.Now);
            return LogearseDB(usuario);
        }

        public List<Perfil> ObtenerPermisosUsuario(string usuario)
        {
            return ListarPermisosDB(usuario);
        }

        public List<Perfil> ObtenerPerfiles()
        {
            return ObtenerPerfilesDB();
        }

        public List<Permiso> ObtenerPermisos(int idPerfil)
        {
            return ObtenerPermisosDB(idPerfil);
        }

        public List<Permiso> ObtenerPermisos()
        {
            return ObtenerPermisosDB();
        }

        private string LogearseDB(string usuario)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - LogearseDB {0} - Inicio ->", usuario), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            string pw = "";
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("Logearse", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@Nombre", usuario);
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            pw = dr.GetString(0);
                        }
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - LogearseDB {0} - Fin ->", usuario), DateTime.Today);
                    return pw;

                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, ex.Message, DateTime.Today);
                    return null;
                }
            }
        }

        private List<Perfil> ObtenerPerfilesDB()
        {
            List<Perfil> Permisos = new List<Perfil>();
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Seguridad - Obtener Perfiles DB - Inicio ->", DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetPerfiles", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            Permisos.Add(new Perfil { idPerfil = dr.GetInt32(0), Descripcion = dr.GetString(1) });
                        }
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Seguridad - Obtener Perfiles DB - Fin ->", DateTime.Now);
                    return Permisos;

                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, ex.Message, DateTime.Today);
                    return null;
                }
            }
        }

        private List<Permiso> ObtenerPermisosDB()
        {
            List<Permiso> Permisos = new List<Permiso>();
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Seguridad - Obtener Permisos DB - Inicio ->", DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetPermisos", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            Permisos.Add(new Permiso { idPermiso = dr.GetInt32(0), Descripcion = dr.GetString(1) });
                        }
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Seguridad - Obtener Permisos DB - Fin ->", DateTime.Now);
                    return Permisos;

                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, ex.Message, DateTime.Today);
                    return null;
                }
            }
        }
        private List<Permiso> ObtenerPermisosDB(int idPerfil)
        {
            List<Permiso> Permisos = new List<Permiso>();
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Seguridad - Obtener Permisos DB - Inicio ->", DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetPermisos", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@idPerfil", idPerfil);
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            Permisos.Add(new Permiso { idPermiso = dr.GetInt32(0), Descripcion = dr.GetString(1) });
                        }
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Seguridad - Obtener Permisos DB - Fin ->", DateTime.Now);
                    return Permisos;
                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, ex.Message, DateTime.Today);
                    return null;
                }
            }
        }

        private List<Perfil> ListarPermisosDB(string usuario)
        {
            List<Perfil> Permisos = new List<Perfil>();
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - ListarPermisosDB {0} - Inicio ->", usuario), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetPerfil", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@Nombre", usuario);
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            Permisos.Add(new Perfil { idPerfil = dr.GetInt32(0), Descripcion = dr.GetString(1) });
                        }
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - ListarPermisosDB {0} - Fin ->", usuario), DateTime.Now);

                    foreach(Perfil perfil in Permisos)
                    {
                        CargarPermisosPerfil(perfil);
                    }


                    return Permisos;

                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, ex.Message, DateTime.Today);
                    return null;
                }
            }
        }

        private void CargarPermisosPerfil(Perfil perfil)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - ListarPermisosDB {0} - Inicio ->", perfil.Descripcion), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetPermisosUsuario", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@IdPerfil", perfil.idPerfil);
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            perfil.Add(new Permiso { idPermiso = dr.GetInt32(0), Descripcion = dr.GetString(1) });
                        }
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - ListarPermisosDB {0} - Fin ->", perfil.Descripcion), DateTime.Today);
                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, ex.Message, DateTime.Today);
                }
            }
        }

        private Perfil AltaPerfilDB(Perfil perfil)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - AltaPerfilDB {0} - Inicio ->", perfil.Descripcion), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("AltaPerfil", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@Perfil", perfil.Descripcion);
                        SqlParameter returnParameter = cm.Parameters.Add("@return_Value", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;
                        cm.ExecuteNonQuery();
                        int returnValue = (int)returnParameter.Value;
                        perfil.idPerfil = returnValue;
                        
                    }
                    cn.Close();
                }
                catch (Exception ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Repositorio Seguridad - AltaPerfilDB {0} - Error ->", ex.Message), DateTime.Now);
                }
                return perfil;
            }
        }

        private void AsignarPermisosDB(int idPerfil, int idPermiso)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - Asignar Permisos DB Perfil {0}, Permiso {1} - Inicio ->", idPerfil, idPermiso), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("AsignarPerfil", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@idPerfil", idPerfil);
                        cm.Parameters.AddWithValue("@idPermiso", idPermiso);
                        cm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Repositorio Seguridad - Asignar Permisos DB {0} - Error ->", ex.Message), DateTime.Now);
                }
            }
        }

        private void BajaPermisoDB(int idPerfil)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - Modificar Permisos DB Perfil {0} - Inicio ->", idPerfil), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("BajaPermiso", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@idPerfil", idPerfil);
                        cm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Repositorio Seguridad - Modificar Permisos DB {0} - Error ->", ex.Message), DateTime.Now);
                }
            }
        }

        private void BajaPerfilDB(int idPerfil)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Seguridad - Baja Perfil DB Perfil {0} - Inicio ->", idPerfil), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("BajaPerfil", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@idPerfil", idPerfil);
                        cm.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Repositorio Seguridad - Modificar Permisos DB {0} - Error ->", ex.Message), DateTime.Now);
                }
            }
        }
    }
}
