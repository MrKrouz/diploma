﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Seguridad
{
    public class Datos
    {
        public string Logearse(string usuario)
        {
            string mensaje = string.Format("Repositorio Seguridad - Datos - Logearse {0} ->", usuario);
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, mensaje , System.DateTime.Today);
            var db = new RepositorioSeguridad();
            return db.Logearse(usuario);
        }

        public List<Perfil> ObtenerPermisosUsuario(string usuario)
        {
            var db = new RepositorioSeguridad();
            return db.ObtenerPermisosUsuario(usuario);
        }

        public List<Perfil> ObtenerPerfiles()
        {
            var db = new RepositorioSeguridad();
            return db.ObtenerPerfiles();
        }
    }
}
