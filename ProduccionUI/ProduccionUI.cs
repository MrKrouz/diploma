﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUI
{
    public partial class ProduccionUI : FormBase.FormBase
    {
        public ProduccionUI()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);

        }

        private void btnAddMaterial_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIAddMat.ProduccionUIAddMat();
            this.Hide(); 
            frm.Show();
        }

        private void btnUpdMaterial_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIUpdMat.ProduccionUIUpdMat();
            this.Hide();
            frm.Show();
        }

        private void btnDelMaterial_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIDelMat.ProduccionUIDelMat();
            this.Hide();
            frm.Show();
        }

        private void btnGetMateriales_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIGetMats.ProduccionUIGetMats();
            this.Hide();
            frm.Show();
        }

        private void btnAddMueble_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIAddMueble.ProduccionUIAddMueble();
            this.Hide();
            frm.Show();
        }

        private void btnUpdMueble_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIUpdMueble.ProduccionUIUpdMueble();
            this.Hide();
            frm.Show();
        }

        private void btnDelMueble_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIDelMueble.ProduccionUIDelMueble();
            this.Hide();
            frm.Show();
        }

        private void btnGetMuebles_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIGetMuebles.ProduccionUIGetMuebles();
            this.Hide();
            frm.Show();
        }

        private void btnAddOC_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIAddOC.ProduccionUIAddOC();
            this.Hide();
            frm.Show();
        }

        private void btnGetOP_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUIGetOPs.ProduccionUIGetOPs();
            this.Hide();
            frm.Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "Home").Show();
            this.Hide();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "Home").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Hide();
        }
    }
}
