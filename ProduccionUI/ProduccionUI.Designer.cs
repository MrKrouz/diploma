﻿namespace ProduccionUI
{
    partial class ProduccionUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProduccionUI));
            this.btnAddMaterial = new System.Windows.Forms.Button();
            this.btnUpdMaterial = new System.Windows.Forms.Button();
            this.btnDelMaterial = new System.Windows.Forms.Button();
            this.btnGetMateriales = new System.Windows.Forms.Button();
            this.btnGetOP = new System.Windows.Forms.Button();
            this.btnAddOC = new System.Windows.Forms.Button();
            this.btnGetMuebles = new System.Windows.Forms.Button();
            this.btnDelMueble = new System.Windows.Forms.Button();
            this.btnUpdMueble = new System.Windows.Forms.Button();
            this.btnAddMueble = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddMaterial
            // 
            this.btnAddMaterial.Location = new System.Drawing.Point(12, 64);
            this.btnAddMaterial.Name = "btnAddMaterial";
            this.btnAddMaterial.Size = new System.Drawing.Size(150, 23);
            this.btnAddMaterial.TabIndex = 0;
            this.btnAddMaterial.Text = "Nuevo Material";
            this.btnAddMaterial.UseVisualStyleBackColor = true;
            this.btnAddMaterial.Click += new System.EventHandler(this.btnAddMaterial_Click);
            // 
            // btnUpdMaterial
            // 
            this.btnUpdMaterial.Location = new System.Drawing.Point(12, 93);
            this.btnUpdMaterial.Name = "btnUpdMaterial";
            this.btnUpdMaterial.Size = new System.Drawing.Size(150, 23);
            this.btnUpdMaterial.TabIndex = 1;
            this.btnUpdMaterial.Text = "Actualizar Material";
            this.btnUpdMaterial.UseVisualStyleBackColor = true;
            this.btnUpdMaterial.Click += new System.EventHandler(this.btnUpdMaterial_Click);
            // 
            // btnDelMaterial
            // 
            this.btnDelMaterial.Location = new System.Drawing.Point(12, 122);
            this.btnDelMaterial.Name = "btnDelMaterial";
            this.btnDelMaterial.Size = new System.Drawing.Size(150, 23);
            this.btnDelMaterial.TabIndex = 2;
            this.btnDelMaterial.Text = "Eliminar Material";
            this.btnDelMaterial.UseVisualStyleBackColor = true;
            this.btnDelMaterial.Click += new System.EventHandler(this.btnDelMaterial_Click);
            // 
            // btnGetMateriales
            // 
            this.btnGetMateriales.Location = new System.Drawing.Point(12, 151);
            this.btnGetMateriales.Name = "btnGetMateriales";
            this.btnGetMateriales.Size = new System.Drawing.Size(150, 23);
            this.btnGetMateriales.TabIndex = 3;
            this.btnGetMateriales.Text = "Listar Materiales";
            this.btnGetMateriales.UseVisualStyleBackColor = true;
            this.btnGetMateriales.Click += new System.EventHandler(this.btnGetMateriales_Click);
            // 
            // btnGetOP
            // 
            this.btnGetOP.Location = new System.Drawing.Point(460, 122);
            this.btnGetOP.Name = "btnGetOP";
            this.btnGetOP.Size = new System.Drawing.Size(166, 23);
            this.btnGetOP.TabIndex = 7;
            this.btnGetOP.Text = "Listar ordenes de produccion";
            this.btnGetOP.UseVisualStyleBackColor = true;
            this.btnGetOP.Click += new System.EventHandler(this.btnGetOP_Click);
            // 
            // btnAddOC
            // 
            this.btnAddOC.Location = new System.Drawing.Point(460, 93);
            this.btnAddOC.Name = "btnAddOC";
            this.btnAddOC.Size = new System.Drawing.Size(166, 23);
            this.btnAddOC.TabIndex = 4;
            this.btnAddOC.Text = "Nueva orden de compra";
            this.btnAddOC.UseVisualStyleBackColor = true;
            this.btnAddOC.Click += new System.EventHandler(this.btnAddOC_Click);
            // 
            // btnGetMuebles
            // 
            this.btnGetMuebles.Location = new System.Drawing.Point(241, 151);
            this.btnGetMuebles.Name = "btnGetMuebles";
            this.btnGetMuebles.Size = new System.Drawing.Size(150, 23);
            this.btnGetMuebles.TabIndex = 11;
            this.btnGetMuebles.Text = "Listar Muebles";
            this.btnGetMuebles.UseVisualStyleBackColor = true;
            this.btnGetMuebles.Click += new System.EventHandler(this.btnGetMuebles_Click);
            // 
            // btnDelMueble
            // 
            this.btnDelMueble.Location = new System.Drawing.Point(241, 122);
            this.btnDelMueble.Name = "btnDelMueble";
            this.btnDelMueble.Size = new System.Drawing.Size(150, 23);
            this.btnDelMueble.TabIndex = 10;
            this.btnDelMueble.Text = "Eliminar Mueble";
            this.btnDelMueble.UseVisualStyleBackColor = true;
            this.btnDelMueble.Click += new System.EventHandler(this.btnDelMueble_Click);
            // 
            // btnUpdMueble
            // 
            this.btnUpdMueble.Location = new System.Drawing.Point(241, 93);
            this.btnUpdMueble.Name = "btnUpdMueble";
            this.btnUpdMueble.Size = new System.Drawing.Size(150, 23);
            this.btnUpdMueble.TabIndex = 9;
            this.btnUpdMueble.Text = "Actualizar Mueble";
            this.btnUpdMueble.UseVisualStyleBackColor = true;
            this.btnUpdMueble.Click += new System.EventHandler(this.btnUpdMueble_Click);
            // 
            // btnAddMueble
            // 
            this.btnAddMueble.Location = new System.Drawing.Point(241, 64);
            this.btnAddMueble.Name = "btnAddMueble";
            this.btnAddMueble.Size = new System.Drawing.Size(150, 23);
            this.btnAddMueble.TabIndex = 8;
            this.btnAddMueble.Text = "Nuevo Mueble";
            this.btnAddMueble.UseVisualStyleBackColor = true;
            this.btnAddMueble.Click += new System.EventHandler(this.btnAddMueble_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(241, 203);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(150, 24);
            this.btnBack.TabIndex = 27;
            this.btnBack.Text = "Atras";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // ProduccionUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ProduccionUI.Properties.Resources.fondo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(638, 239);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnGetMuebles);
            this.Controls.Add(this.btnDelMueble);
            this.Controls.Add(this.btnUpdMueble);
            this.Controls.Add(this.btnAddMueble);
            this.Controls.Add(this.btnGetOP);
            this.Controls.Add(this.btnAddOC);
            this.Controls.Add(this.btnGetMateriales);
            this.Controls.Add(this.btnDelMaterial);
            this.Controls.Add(this.btnUpdMaterial);
            this.Controls.Add(this.btnAddMaterial);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProduccionUI";
            this.Text = "Produccion";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddMaterial;
        private System.Windows.Forms.Button btnUpdMaterial;
        private System.Windows.Forms.Button btnDelMaterial;
        private System.Windows.Forms.Button btnGetMateriales;
        private System.Windows.Forms.Button btnGetOP;
        private System.Windows.Forms.Button btnAddOC;
        private System.Windows.Forms.Button btnGetMuebles;
        private System.Windows.Forms.Button btnDelMueble;
        private System.Windows.Forms.Button btnUpdMueble;
        private System.Windows.Forms.Button btnAddMueble;
        private System.Windows.Forms.Button btnBack;
    }
}

