﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VentasUIAddCliente
{
    public partial class VentasUIAddCliente : Form
    {
        public VentasUIAddCliente()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            var coreVentas = new CoreVentas.coreVentas();
            var cliente = new EE.Cliente();
            bool flag = true;
            #region validacion de campos
            if (textBox1.Text != string.Empty)
            {
                cliente.Nombre = textBox1.Text;
            }
            else
            {
                MessageBox.Show("Ingrese el nombre");
                flag = false;

            }
            if (textBox2.Text != string.Empty)
            {
                cliente.Apellido = textBox2.Text;

            }
            else
            {
                MessageBox.Show("Ingrese el apellido");
                flag = false;

            }
            if (textBox3.Text != string.Empty)
            {
                cliente.Direccion = textBox3.Text;
            }
            else
            {
                MessageBox.Show("Ingrese la direccion del cliente");
                flag = false;

            }
            int numero;
            if (textBox4.Text != string.Empty && int.TryParse(textBox4.Text, out numero) == true)
            {
                cliente.Dni = numero;
            }
            else
            {
                MessageBox.Show("Ingrese el dni sin puntos ni guiones");
                flag = false;
            }
            if (textBox5.Text != string.Empty && int.TryParse(textBox5.Text, out numero) == true)
            {
                cliente.Telefono = Convert.ToInt32(textBox5.Text);
            }
            else
            {
                MessageBox.Show("Ingrese el telefono del cliente sin guiones");
                flag = false;
            }
            #endregion
            if (flag == true)
            {
                coreVentas.AltaCliente(cliente);
                MessageBox.Show("Cliente generado");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "VentasUIHome").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();
        }

        private void VentasUIAddCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void VentasUIAddCliente_Load(object sender, EventArgs e)
        {

        }
    }
}
