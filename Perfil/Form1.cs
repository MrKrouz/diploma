﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Perfil
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var coreSeg = new CoreSeguridad.GestionSeguridad();
            foreach(var algo in coreSeg.ObtenerPermisos())
            {
                treeView1.Nodes.Add(new TreeNode { Text = algo.Descripcion });
            }
            RefrescarCombos();
        }

        private void RefrescarCombos()
        {
            var coreSeg = new CoreSeguridad.GestionSeguridad();
            comboBox1.DataSource = null;
            comboBox1.DataSource = coreSeg.ObtenerPerfiles();
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.ValueMember = "idPerfil";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var Perfil = new Entidades.Perfil();
            Perfil.Descripcion = textBox1.Text;
            var coreSeg = new CoreSeguridad.GestionSeguridad();
            var listaPermisos = new List<Entidades.Permiso>();
            foreach (TreeNode item in treeView1.Nodes)
            {
                if (item.Checked == true)
                {
                    Perfil.Add(new Entidades.Permiso { Descripcion = item.Text, idPermiso = coreSeg.ObtenerPermisos().Where(p => p.Descripcion == item.Text).Select(x => x.idPermiso).FirstOrDefault()});
                }
            }
            coreSeg.AltaPerfil(Perfil);
            RefrescarCombos();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coreSeg = new CoreSeguridad.GestionSeguridad();
            treeView2.Nodes.Clear();
            foreach (var algo in coreSeg.ObtenerPermisos())
            {
                treeView2.Nodes.Add(new TreeNode { Text = algo.Descripcion });
            }
            foreach (var permisos in coreSeg.ObtenerPermisos(comboBox1.SelectedIndex + 1))
            {
                foreach (TreeNode item in treeView2.Nodes)
                {
                    if (item.Text == permisos.Descripcion)
                    {
                        item.Checked = true;
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var coreSeg = new CoreSeguridad.GestionSeguridad();
            coreSeg.BajaPerfil(comboBox1.SelectedIndex + 1);
            RefrescarCombos();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            var Perfil = new Entidades.Perfil();
            Perfil.Descripcion = textBox1.Text;
            var coreSeg = new CoreSeguridad.GestionSeguridad();
            var listaPermisos = new List<Entidades.Permiso>();
            foreach (TreeNode item in treeView2.Nodes)
            {
                if (item.Checked == true)
                {
                    listaPermisos.Add(new Entidades.Permiso { Descripcion = item.Text, idPermiso = coreSeg.ObtenerPermisos().Where(p => p.Descripcion == item.Text).Select(x => x.idPermiso).FirstOrDefault() });
                }
            }
            coreSeg.ModificarPerfil(Perfil);
            RefrescarCombos();

        }
    }
}
