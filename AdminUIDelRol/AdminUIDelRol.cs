﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIDelRol
{
    public partial class AdminUIDelRol : Form
    {
        public AdminUIDelRol()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var coreSec = new CoreSec.CoreSec();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreSec.GetPerfiles().Where(l=> !coreSec.GetPerfilesBase().Any(p=> p.Id == l.Id) && l.Descripcion != "Admin").ToList();
            comboBox1.Text = string.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            if(comboBox1.Text != string.Empty)
            {
                coreSec.BorrarRelaciones((int)comboBox1.SelectedValue);
                coreSec.BajaPerfil((EE.Perfil)comboBox1.SelectedItem);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();

        }
    }
}
