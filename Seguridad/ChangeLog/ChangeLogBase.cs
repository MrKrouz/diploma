﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.ChangeLog
{
    public abstract class ChangeLogBase
    {
        protected readonly object lockObj = new object();
        public abstract void ChangeLog(string clase, string estadoAnterior, string estadoActual, string accion);
    }
}
