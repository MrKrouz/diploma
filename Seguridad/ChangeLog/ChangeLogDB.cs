﻿using CoreServicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.ChangeLog
{
    public class ChangeLogDB : ChangeLogBase
    {
        public override void ChangeLog(string clase, string estadoAnterior, string estadoActual, string accion)
        {
            lock (lockObj)
            {
                var coreLog = new GestionLogs();
                coreLog.GenerarChangeLog(clase, estadoAnterior, estadoActual, accion);
            }
        }
    }
}
