﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.ChangeLog
{
    public class ChangeLogHelper
    {
        private ChangeLogBase changelogger = null;

        public static void ChangeLog(string clase, string estadoAnterior, string estadoActual,  string accion)
        {
            var ChangeLogger = new ChangeLogDB();
            ChangeLogger.ChangeLog(clase, estadoAnterior, estadoActual, accion);
        }
    }
}
