﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Servicios;

namespace Servicios.Logger
{
    public class LogHelper
    {
        private static LogBase logger = null;
        public static void Log(Destino destino, string mensaje, DateTime fechaLog)
        {
            switch (destino)
            {
                case Destino.Archivo:
                    logger = new FileLogger();
                    logger.Log(mensaje, fechaLog);
                    break;
                case Destino.BaseDeDatos:
                    logger = new DBLogger();
                    logger.Log(mensaje, fechaLog);
                    break;

                default:
                    return;
            }
        }
    }
}
