﻿using CoreServicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Logger
{
    public class DBLogger : LogBase
    {
        public override void Log(string mensaje, DateTime fechaLog)
        {
            lock (lockObj)
            {
                var coreLog = new GestionLogs();
                coreLog.GenerarLog(mensaje, fechaLog);
            }
        }

    }
}
