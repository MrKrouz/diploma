﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Logger
{
    class FileLogger : LogBase
    {
        public string filePath = Path.GetDirectoryName(Environment.CurrentDirectory) + "Logs.txt";
        public override void Log(string mensaje, DateTime fechaLog)
        {
            lock (lockObj)
            {
                using (StreamWriter streamWriter = new StreamWriter(filePath, true))
                {
                    streamWriter.WriteLine(mensaje + " " + fechaLog);
                    streamWriter.Close();
                }
            }
        }
    }
}
