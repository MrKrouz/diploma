﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIIdiomaMid
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var frm = new AdminUIAddIdioma.Form1();
            frm.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var frm = new AdminUIUpdIdioma.Form1();
            frm.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var frm = new AdminUIDelIdioma.Form1();
            frm.Show();
            this.Hide();
        }
    }
}
