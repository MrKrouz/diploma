﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIGIDelU
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            coreSec.DelUser(new EE.Usuario { Id = (int)comboBox2.SelectedValue });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "UserName";
            comboBox2.DataSource = new CoreSec.CoreSec().GetUsers();
            comboBox2.Text = string.Empty;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUIMid").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
