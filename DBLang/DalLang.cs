﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace DBLang
{
    public class DalLang
    {
        public void NewLang(EE.Lenguaje Lenguaje)
        {
            try
            {

            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().AddLang;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = System.Data.CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@Descripcion", Lenguaje.Descripcion);
            cmd.Parameters.AddWithValue("@Cultura", Lenguaje.Cultura.ToString());
            SqlParameter returnv = new SqlParameter("@id", SqlDbType.Int);
            returnv.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnv);
            cmd.ExecuteNonQuery();
            Lenguaje.ID = (int)cmd.Parameters["@id"].Value;
            cnX.Close();
            foreach(var item in Lenguaje.ListarPalabras())
            {
                AddWords(Lenguaje.ID, item);
            }
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al cargar el idioma -> " + ex.Message, System.DateTime.Now);

            }
        }
        public void DelLang(int IdLang)
        {
            try
            {

            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().DelLang;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@Id", IdLang);
            cmd.ExecuteNonQuery();
            cnX.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al eliminar el idioma -> " + ex.Message, System.DateTime.Now);

            }
        }
        public void UpdLang(EE.Lenguaje Lenguaje)
        {
            try
            {

            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().UpdLang;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = System.Data.CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@Id", Lenguaje.ID);
            cmd.Parameters.AddWithValue("@Cultura", Lenguaje.Cultura.ToString());
            cmd.Parameters.AddWithValue("@Descripcion", Lenguaje.Descripcion);
            cmd.ExecuteNonQuery();
            cnX.Close();
            foreach (var item in Lenguaje.ListarPalabras())
            {
                UpdateWords(item.ID, item.Descripcion);       
            }
            }
            catch(SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al actualizar el idioma -> " + ex.Message, System.DateTime.Now);

            }
        }
        public List<EE.Idioma> GetLangs()
        {
            try
            { 
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().GetLangs;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX);
            cnX.Open();
            List<EE.Idioma> Langs = new List<EE.Idioma>();
            var dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Langs.Add(new EE.Idioma { ID = dr.GetInt32(0), Descripcion = dr.GetString(1), Cultura = (new System.Globalization.CultureInfo(dr.GetString(2)))});
            }
            foreach(var item in Langs)
            {
                GetWords(item);
            }
            return Langs;
            }
            catch(SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener idiomas -> " + ex.Message, System.DateTime.Now);
                return new List<EE.Idioma>();
            }
        }
        public void GetWords(EE.Lenguaje lenguaje)
        {
            try
            {

            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().GetWords;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@IdIdioma", lenguaje.ID);
            var dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lenguaje.AgregarPalabra(new EE.Palabra(dr.GetString(1), dr.GetString(2)) { ID = dr.GetInt32(0) });
            }
            cnX.Close();
            }
            catch(SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las palabras -> " + ex.Message, System.DateTime.Now);
            }
        }
        private void AddWords(int ID, EE.Lenguaje palabra)
        {
            try
            {

            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().AddWords;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = System.Data.CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@IdIdioma", ID);
            cmd.Parameters.AddWithValue("@Control", palabra.Control);
            cmd.Parameters.AddWithValue("@Descripcion", palabra.Descripcion);
            SqlParameter returnv = new SqlParameter("@IdOutput", SqlDbType.Int);
            returnv.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnv);
            cmd.ExecuteNonQuery();
            palabra.ID = (int)cmd.Parameters["@IdOutput"].Value;
            cnX.Close();
            }
            catch(SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al agregar palabras -> " + ex.Message, System.DateTime.Now);
            }
        }
        private void UpdateWords(int PalabraId, string descripcion)
        {
            try
            { 
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().UpdWords;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = System.Data.CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@IdPalabra", PalabraId);
            cmd.Parameters.AddWithValue("@Valor", descripcion);
            cmd.ExecuteNonQuery();
            cnX.Close();
            }
            catch(SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al actualizar las palabras -> " + ex.Message, System.DateTime.Now);

            }
        }
    }
}
