﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIUpdMueble
{
    public partial class ProduccionUIUpdMueble : FormBase.FormBase
    {
        public ProduccionUIUpdMueble()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);
        }

        private void ActualizarCB()
        {
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Descripcion";
            comboBox2.DataSource = coreProd.GetMuebles();
            comboBox2.Text = string.Empty;
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;
            textBox5.Text = string.Empty;
            textBox6.Text = string.Empty;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            ActualizarCB();
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreProd.GetMateriales();
            comboBox1.Text = string.Empty;
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            #region ValidacionDeCampos
            bool flag = true;
            double item = 0;
            if (double.TryParse(textBox1.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (double.TryParse(textBox2.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (double.TryParse(textBox3.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (double.TryParse(textBox5.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (textBox4.Text == string.Empty)
            {
                MessageBox.Show("Debe ingresar una descripcion");
                flag = false;
            }
            int item2 = 0;
            if (int.TryParse(textBox6.Text, out item2) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }

            #endregion
            if (flag == true)
            {
                var coreProd = new CoreProduccion.bllProduccion();
                var mueble = new EE.Mueble();
                mueble.Id = (int)comboBox2.SelectedValue;
                mueble.Madera.Id = (int)comboBox1.SelectedValue;
                mueble.MedidaH = Convert.ToDouble(textBox1.Text);
                mueble.MedidaL = Convert.ToDouble(textBox2.Text);
                mueble.MedidaP = Convert.ToDouble(textBox3.Text);
                mueble.Descripcion = textBox4.Text;
                mueble.Precio = Convert.ToDouble(textBox5.Text);
                mueble.Stock = Convert.ToInt32(textBox6.Text);
                coreProd.UpdMueble(mueble);
                MessageBox.Show("Mueble actualizado");
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            var mueble = coreProd.GetMuebles().Find(m => m.Id == (int)comboBox2.SelectedValue);
            comboBox1.Text = mueble.Madera.Descripcion;
            textBox1.Text = mueble.MedidaH.ToString();
            textBox2.Text = mueble.MedidaL.ToString();
            textBox3.Text = mueble.MedidaP.ToString();
            textBox4.Text = mueble.Descripcion;
            textBox5.Text = mueble.Precio.ToString();
            textBox6.Text = mueble.Stock.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
