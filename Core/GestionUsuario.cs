﻿using Entidades;
using Repositorio;
using Seguridad;
using System.Collections.Generic;

namespace Core
{
    public class GestionUsuario
    {
        public void AltaUsuario(Usuario user, int perfil)
        {
            var datos = new Datos();
            Servicios.ChangeLog.ChangeLogHelper.ChangeLog("Usuario", null, user.ToString(), "Alta");
            datos.AltaUsuario(user, perfil);
        }

        public void ModificarUsuario(Usuario userOld, Usuario userNew, int perfil)
        {
            var datos = new Datos();
            Servicios.ChangeLog.ChangeLogHelper.ChangeLog("Usuario", userOld.ToString(), userNew.ToString(), "Modificacion");
            datos.ModificarUsuario(userNew, perfil);
        }

        public void BajaUsuario(Usuario user)
        {
            var datos = new Datos();
            Servicios.ChangeLog.ChangeLogHelper.ChangeLog("Usuario", user.ToString(), "Dado de baja", "Baja");
            datos.BajaUsuario(user);
        }

        public List<Usuario> ObtenerUsuarios()
        {
            var datos = new Datos();
            return datos.ObtenerUsuarios();
        }

        public Usuario ObtenerUsuario(int idUsuario)
        {
            var datos = new Datos();
            return datos.ObtenerUsuario(idUsuario);
        }
    }
}
