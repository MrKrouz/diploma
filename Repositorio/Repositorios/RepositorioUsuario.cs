﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Repositorios
{
    public class RepositorioUsuario
    {
        #region Metodos Publicos
        public void AltaUsuario(Usuario user, int perfil)
        {
            AltaUsuarioDB(user, perfil);
        }

        public void ModificarUsuario(Usuario user, int perfil)
        {
            ModificarUsuarioDB(user, perfil);
        }

        public void BajaUsuario(int nombreUsuario)
        {
            BajaUsuarioDB(nombreUsuario);
        }

        public List<Usuario> Todos()
        {
            return ObtenerUsuariosDB();
        }

        public Usuario ObtenerUsuario(int idUsuario)
        {
            return Todos().Where(u => u.IdUsuario == idUsuario).FirstOrDefault();
        }
        #endregion

        #region Metodos Privados
        private void AltaUsuarioDB(Usuario user, int perfil)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Usuario - Alta - Inicio -> {0}, {1}, {2}", user.NombreUsuario, user.Nombre, user.Apellido), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    string dvh = GenerarDVH(user.Nombre, user.Password, user.NombreUsuario);
                    using (SqlCommand cm = new SqlCommand("AltaUsuario", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@Nombre", user.NombreUsuario);
                        cm.Parameters.AddWithValue("@NombreUsuario", user.Nombre);
                        cm.Parameters.AddWithValue("@Apellido", user.Apellido);
                        cm.Parameters.AddWithValue("@Password", user.Password);
                        cm.Parameters.AddWithValue("@idPerfil", perfil);
                        cm.Parameters.AddWithValue("@dvh", dvh);
                        SqlParameter returnParameter = cm.Parameters.Add("RetVal", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;
                        cm.ExecuteNonQuery();

                        int returnValue = (int)returnParameter.Value;
                        user.IdUsuario = returnValue;
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Usuario - Alta - Fin ", DateTime.Now);
                    AsignarPerfil(user.IdUsuario, perfil); 
                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Error al generar el alta del usuario -> {0} debido a {1}", user.NombreUsuario, ex.Message), DateTime.Now);
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Error al generar el alta del usuario -> {0} debido a {1}", user.NombreUsuario, ex.Message), DateTime.Now);
                    cn.Close();
                }
            }
        }


        private void AsignarPerfil(int idUsuario, int idperfil)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Usuario - Alta Perfil - Inicio -> {0}, {1}", idUsuario, idperfil), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("AltaPerfil", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@idPerfil", idperfil);
                        cm.Parameters.AddWithValue("@idUsuario", idUsuario);
                        cm.ExecuteNonQuery();
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Usuario - Alta Perfil - Fin ", DateTime.Now);
                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Error al generar el alta del perfil de usuario -> {0} debido a {1}", idUsuario, ex.Message), DateTime.Now);
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Error al generar el alta del perfil de usuario -> {0} debido a {1}", idUsuario, ex.Message), DateTime.Now);
                    cn.Close();
                }
            }
        }
        private void ModificarUsuarioDB(Usuario user, int perfil)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Usuario - Modificar - Inicio -> {0}, {1}, {2}", user.NombreUsuario, user.Nombre, user.Apellido), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    string dvh = GenerarDVH(user.Nombre, user.Password, user.NombreUsuario);
                    using (SqlCommand cm = new SqlCommand("ModificarUsuario", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@IdUsuario", user.IdUsuario);
                        cm.Parameters.AddWithValue("@Nombre", user.Nombre);
                        cm.Parameters.AddWithValue("@NombreUsuario", user.NombreUsuario);
                        cm.Parameters.AddWithValue("@Apellido", user.Apellido);
                        cm.Parameters.AddWithValue("@Password", user.Password);
                        cm.Parameters.AddWithValue("@dvh", dvh);
                        cm.Parameters.AddWithValue("@idPerfil", perfil);
                        cm.ExecuteNonQuery();
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Repositorio Usuario - Modificar - Fin ", DateTime.Now);
                    ObtenerCadena();
                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Error al generar la modificacion del usuario -> {0} debido a {1}", user.NombreUsuario, ex.Message), DateTime.Now);
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Error al generar la modificacion del usuario -> {0} debido a {1}", user.NombreUsuario, ex.Message), DateTime.Now);
                    cn.Close();
                }
            }
        }

        private void BajaUsuarioDB(int NombreUsuario)
        {
            Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Usuario - Baja - Inicio -> {0}", NombreUsuario), DateTime.Now);
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("BajaUsuario", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@IdUsuario", NombreUsuario);
                        cm.ExecuteNonQuery();
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Repositorio Usuario - Baja - Fin -> {0}", NombreUsuario), DateTime.Now);
                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Error al generar la baja del usuario -> {0} debido a {1}", NombreUsuario, ex.Message), DateTime.Now);
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Error al generar la baja del usuario -> {0} debido a {1}", NombreUsuario, ex.Message), DateTime.Now);
                    cn.Close();
                }
            }
        }

        private List<Usuario> ObtenerUsuariosDB()
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            List<Usuario> listaUsuarios = new List<Usuario>();
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Obtener Usuarios DB - Inicio ", DateTime.Now);
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetUsuarios", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            listaUsuarios.Add(new Usuario {IdUsuario = dr.GetInt32(0), Nombre = dr.GetString(1), Apellido = dr.GetString(2), NombreUsuario = dr.GetString(3) });
                        }
                    }
                    cn.Close();
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, "Obtener Usuarios DB - Fin", DateTime.Now);
                    return listaUsuarios;
                }
                catch(SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, string.Format("Se ha producido un error al obtener los usuarios - Obtener Todos DB -> {0} ", ex.Message), DateTime.Now);
                    cn.Close();
                    return null;
                }
            }
        }

        private string[] ObtenerCadena()
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            string cadena = "";
            string tabla = "";
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetUsuarios", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            cadena = cadena + dr.GetString(4);
                            tabla = dr.GetString(5);
                        }
                    }
                    var encrip = new Seguridad.CryptoManager();
                    insertarDVV(encrip.CalculateMD5Hash(cadena), tabla);
                    return null;
                }
                catch (SqlException ex)
                {
                    cn.Close();
                    return null;
                }
            }
        }

        private void insertarDVV(string cadena, string tabla)
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            string[] datos = { };
            var crypto = new Seguridad.CryptoManager();
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("InsertarDVV", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@dvv", cadena);
                        cm.Parameters.AddWithValue("@tabla", tabla);
                        cm.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.Archivo, ex.Message, DateTime.Now);
                    cn.Close();
                }
            }
        }

        private string GenerarDVH(string nombre, string password, string nombreUsuario)
        {
            var crypto = new Seguridad.CryptoManager();
            return crypto.CalculateMD5Hash(nombre + password + nombre);
        }
        #endregion
    }
}
