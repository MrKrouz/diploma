﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public class Datos
    {
        public void AltaUsuario(Usuario user, int perfil)
        {
            var db = new Repositorios.RepositorioUsuario();
            var crypto = new Seguridad.CryptoManager();
            user.Password = crypto.CalculateMD5Hash(user.Password);
            db.AltaUsuario(user, perfil);
        }

        public void ModificarUsuario(Usuario user, int perfil)
        {
            var db = new Repositorios.RepositorioUsuario();
            var crypto = new Seguridad.CryptoManager();
            user.Password = crypto.CalculateMD5Hash(user.Password);
            db.ModificarUsuario(user, perfil);
        }

        public void BajaUsuario(Usuario user)
        {
            var db = new Repositorios.RepositorioUsuario();
            db.BajaUsuario(user.IdUsuario);
        }

        public List<Usuario> ObtenerUsuarios()
        {
            var db = new Repositorios.RepositorioUsuario();
            return db.Todos();
        }

        public Usuario ObtenerUsuario(int idUsuario)
        {
            var db = new Repositorios.RepositorioUsuario();
            return db.ObtenerUsuario(idUsuario);
        }
    }
}
