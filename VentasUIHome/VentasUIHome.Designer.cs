﻿namespace VentasUIHome
{
    partial class VentasUIHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentasUIHome));
            this.btnAddCliente = new System.Windows.Forms.Button();
            this.btnModCliente = new System.Windows.Forms.Button();
            this.btnGenerarFactura = new System.Windows.Forms.Button();
            this.btnGenerarOP = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddCliente
            // 
            this.btnAddCliente.Location = new System.Drawing.Point(26, 32);
            this.btnAddCliente.Name = "btnAddCliente";
            this.btnAddCliente.Size = new System.Drawing.Size(98, 23);
            this.btnAddCliente.TabIndex = 0;
            this.btnAddCliente.Text = "Alta Cliente";
            this.btnAddCliente.UseVisualStyleBackColor = true;
            this.btnAddCliente.Click += new System.EventHandler(this.btnAddCliente_Click);
            // 
            // btnModCliente
            // 
            this.btnModCliente.Location = new System.Drawing.Point(26, 61);
            this.btnModCliente.Name = "btnModCliente";
            this.btnModCliente.Size = new System.Drawing.Size(98, 23);
            this.btnModCliente.TabIndex = 1;
            this.btnModCliente.Text = "Modificar Cliente";
            this.btnModCliente.UseVisualStyleBackColor = true;
            this.btnModCliente.Click += new System.EventHandler(this.btnModCliente_Click);
            // 
            // btnGenerarFactura
            // 
            this.btnGenerarFactura.Location = new System.Drawing.Point(140, 32);
            this.btnGenerarFactura.Name = "btnGenerarFactura";
            this.btnGenerarFactura.Size = new System.Drawing.Size(157, 23);
            this.btnGenerarFactura.TabIndex = 2;
            this.btnGenerarFactura.Text = "Generar factura";
            this.btnGenerarFactura.UseVisualStyleBackColor = true;
            this.btnGenerarFactura.Click += new System.EventHandler(this.btnGenerarFactura_Click);
            // 
            // btnGenerarOP
            // 
            this.btnGenerarOP.Location = new System.Drawing.Point(140, 61);
            this.btnGenerarOP.Name = "btnGenerarOP";
            this.btnGenerarOP.Size = new System.Drawing.Size(157, 23);
            this.btnGenerarOP.TabIndex = 3;
            this.btnGenerarOP.Text = "Generar orden de produccion";
            this.btnGenerarOP.UseVisualStyleBackColor = true;
            this.btnGenerarOP.Click += new System.EventHandler(this.btnGenerarOP_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(124, 90);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 4;
            this.btnBack.Text = "Atras";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // VentasUIHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::VentasUIHome.Properties.Resources.fondo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(323, 140);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnGenerarOP);
            this.Controls.Add(this.btnGenerarFactura);
            this.Controls.Add(this.btnModCliente);
            this.Controls.Add(this.btnAddCliente);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VentasUIHome";
            this.Text = "Ventas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VentasUIHome_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VentasUIHome_FormClosed);
            this.Load += new System.EventHandler(this.VentasUIHome_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddCliente;
        private System.Windows.Forms.Button btnModCliente;
        private System.Windows.Forms.Button btnGenerarFactura;
        private System.Windows.Forms.Button btnGenerarOP;
        private System.Windows.Forms.Button btnBack;
    }
}

