﻿using CoreVentas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VentasUIHome
{
    public partial class VentasUIHome : Form
    {
        public VentasUIHome()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "Home").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();
        }

        private void btnAddCliente_Click(object sender, EventArgs e)
        {
            var frm = new VentasUIAddCliente.VentasUIAddCliente();
            frm.Show();
            this.Hide();
        }

        private void btnModCliente_Click(object sender, EventArgs e)
        {
            var frm = new VentasUIUpdCliente.VentasUIUpdCliente();
            frm.Show();
            this.Hide();
        }

        private void btnGenerarFactura_Click(object sender, EventArgs e)
        {
            var frm = new VentasUIGenerarFactura.VentasUIGenerarFactura();
            frm.Show();
            this.Hide();
        }

        private void btnGenerarOP_Click(object sender, EventArgs e)
        {
            var frm = new VentasUIGenerarOP.VentasUIGenerarOP();
            frm.Show();
            this.Hide();
        }

        private void VentasUIHome_Load(object sender, EventArgs e)
        {
        }

        private void VentasUIHome_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void VentasUIHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
