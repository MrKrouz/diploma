﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seguridad
{
    public class Singleton
    {
        private static volatile Singleton instance;
        private static object syncRoot = new object();
        private IUsuario user;
        private Singleton() { }

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Singleton();
                    }
                }
                return instance;
            }
        }

        public void Logearse(IUsuario user)
        {
            this.user = user;
        }

        public IUsuario DevolverInstancia()
        {
            if (instance != null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public void Deslogear()
        {
            this.user = null;
        }
    }
}
