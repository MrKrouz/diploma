﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seguridad
{
    public interface IUsuario 
    {
        int IdUsuario { get; set; }

        string NombreUsuario { get; set; }

        string IdiomaSeleccionado { get; set; }

        IEnumerable Permisos { get; set; }
    }
}
