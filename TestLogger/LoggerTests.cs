﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EE;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass()]
    public class LoggerTests
    {
        [TestMethod()]
        public void LogDBT()
        {
            var logger = new Utilidades.Logger();
            logger.LogDB("TestLog", DateTime.Now);

            var logs = logger.GetLogs(DateTime.Now);
            Assert.IsNotNull(logs.Find(l => l.Evento == "TestLog"));
        }

        [TestMethod()]
        public void LogFile()
        {
            var logger = new Utilidades.Logger();
            logger.LogFile("TestLog", DateTime.Now, "Error test");
        }
    }
}