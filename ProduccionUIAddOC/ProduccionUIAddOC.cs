﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIAddOC
{
    public partial class ProduccionUIAddOC : FormBase.FormBase
    {
        EE.OrdenDeCompra oc = new EE.OrdenDeCompra();
        public ProduccionUIAddOC()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);
        }

        private void ProduccionUIAddOC_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreProd.GetMateriales();
            comboBox1.Text = string.Empty;
            oc.Empleado = new EE.Usuario { Id = 1 };
            oc.Informe = new EE.InformeMRP { Id = 1 };
            oc.Fecha = System.DateTime.Now;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != string.Empty)
            {
                int numero;
                if (textBox2.Text != string.Empty && int.TryParse(textBox2.Text, out numero))
                {

                    EE.MateriaPrima item = (EE.MateriaPrima)comboBox1.SelectedItem;
                    if (oc.Detalle.Find(i => i.Material.Id == (int)comboBox1.SelectedValue) == null)
                    {
                        oc.Detalle.Add(new EE.DetalleOrdenDeCompra { Cantidad = Convert.ToInt32(textBox2.Text), Material = item });
                        listBox1.Items.Add(item.Descripcion + " " + textBox2.Text);
                    }
                    else
                    {
                        var cantidad = oc.Detalle.Find(i => i.Material.Id == (int)comboBox1.SelectedValue).Cantidad;
                        listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + cantidad));
                        listBox1.Items.Add(item.Descripcion + " " + (cantidad + Convert.ToInt32(textBox2.Text)));
                        oc.Detalle.Find(i => i.Material.Id == (int)comboBox1.SelectedValue).Cantidad += Convert.ToInt32(textBox2.Text);

                    }
                }
                else
                {
                    MessageBox.Show("Debe indicar un numero de items");
                }
            }
            else
            {
                MessageBox.Show("Se debe seleccionar un item de la lista de materia prima.");
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != string.Empty)
            {
                EE.MateriaPrima item = (EE.MateriaPrima)comboBox1.SelectedItem;
                if (oc.Detalle.Find(i => i.Material.Id == (int)comboBox1.SelectedValue) == null)
                {
                    MessageBox.Show("No existen materiales cargados");
                }
                else
                {
                    var detalle = oc.Detalle.Find(i => i.Material.Id == (int)comboBox1.SelectedValue);
                    if (detalle.Cantidad == 0 || detalle.Cantidad - Convert.ToInt32(textBox2.Text) < 0)
                    {
                        listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + detalle.Cantidad));
                        oc.Detalle.Remove(detalle);
                    }
                    else
                    {
                        listBox1.Items.RemoveAt(listBox1.Items.IndexOf(item.Descripcion + " " + detalle.Cantidad));
                        detalle.Cantidad -= Convert.ToInt32(textBox2.Text);
                        listBox1.Items.Add(item.Descripcion + " " + detalle.Cantidad);
                    }

                }
            }
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            if (oc.Detalle.Count > 0)
            {
                foreach (var item in oc.Detalle)
                {
                    item.Material = coreProd.GetMateriales().Find(m => m.Id == item.Material.Id);
                }
                int numero = 0;
                if (textBox2.Text != string.Empty && int.TryParse(textBox2.Text, out numero))
                {
                    oc.Informe.Id = numero;
                }
                else
                {
                    MessageBox.Show("Ingrese el numero de informe correspondiente");
                }
                oc.Empleado = SecUtils.Singleton.Instance.GetInstance();
                coreProd.GenerarOrdenDeCompra(oc);
                MessageBox.Show("Su orden de compra ha sido generada");
            }
            else
            {
                MessageBox.Show("Orden de compra no generada por falta de elementos");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
