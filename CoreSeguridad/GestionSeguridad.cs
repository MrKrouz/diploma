﻿using Entidades;
using Repositorio_Seguridad;
using Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSeguridad
{
    public class GestionSeguridad
    {
        public bool Logearse(Usuario user)
        {
            var datos = new Datos();
            string pwdb = datos.Logearse(user.NombreUsuario);
            string pwLog = new CryptoManager().CalculateMD5Hash(user.Password);
            if (pwdb == pwLog)
            {
                Singleton.Instance.Logearse(user);
                user.Permisos = datos.ObtenerPermisosUsuario(user.NombreUsuario);
                Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Usuario {0} se ha logeado.", user.NombreUsuario) , DateTime.Now);
                return true;
            }
            else
            {
                Servicios.Logger.LogHelper.Log(Servicios.Logger.Destino.BaseDeDatos, string.Format("Usuario {0} ha tenido un error al logearse.", user.NombreUsuario), DateTime.Now);
                return false;
            }
        }

        public List<Permiso> ObtenerPermisos()
        {
            var repo = new RepositorioSeguridad();
            return repo.ObtenerPermisos();
        }

        public List<Permiso> ObtenerPermisos(int idPerfil)
        {
            var repo = new RepositorioSeguridad();
            return repo.ObtenerPermisos(idPerfil);
        }

        public List<Perfil> ObtenerPerfiles()
        {
            return new Datos().ObtenerPerfiles();
        }

        public void AltaPerfil(Perfil perfil)
        {
            var repo = new RepositorioSeguridad();
            repo.AltaPerfil(perfil);
            foreach(Permiso perfiles in perfil.DevolverLista())
            {
                repo.AltaPermisos(perfil.idPerfil, perfiles.idPermiso);
            }
        }

        public void ModificarPerfil(Perfil perfil)
        {
            var repo = new RepositorioSeguridad();
            repo.RemoverPermiso(perfil.idPerfil);
            foreach(Permiso permiso in perfil.DevolverLista())
            {
                repo.AltaPermisos(perfil.idPerfil, permiso.idPermiso);
            }
        }

        public void BajaPerfil(int idPerfil)
        {
            var repo = new RepositorioSeguridad();
            repo.RemoverPermiso(idPerfil);
            repo.BajaPerfil(idPerfil);
        }
    }
}
