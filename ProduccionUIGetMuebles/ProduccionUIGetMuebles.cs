﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;

namespace ProduccionUIGetMuebles
{
    public partial class ProduccionUIGetMuebles : FormBase.FormBase
    {
        public ProduccionUIGetMuebles()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);

        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            var item = new CoreProduccion.bllProduccion().GetMuebles().Find(m => m.Id == (int)listBox1.SelectedValue);
            textBox1.Text = item.MedidaH.ToString();
            textBox2.Text = item.MedidaL.ToString();
            textBox3.Text = item.MedidaP.ToString();
            textBox4.Text = item.Descripcion;
            textBox5.Text = item.Precio.ToString();
            textBox6.Text = item.Stock.ToString();
            textBox7.Text = item.Madera.Descripcion;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            string outputFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Lista muebles.pdf");
            using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (Document doc = new Document(PageSize.A4))
                {
                    using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                    {
                        doc.Open();
                        PdfPTable t = new PdfPTable(2);
                        t.DefaultCell.Border = 0;
                        t.DefaultCell.BorderWidthBottom = 1;
                        t.DefaultCell.BorderColorBottom = BaseColor.RED;
                        foreach (var item in coreProd.GetMuebles())
                        {
                            t.AddCell("Madera: ");
                            t.AddCell(item.Madera.Descripcion);
                            t.AddCell("MedidaH: ");
                            t.AddCell(item.MedidaH.ToString());
                            t.AddCell("MedidaL: ");
                            t.AddCell(item.MedidaL.ToString());
                            t.AddCell("MedidaP: ");
                            t.AddCell(item.MedidaP.ToString());
                            t.AddCell("Descripcion: ");
                            t.AddCell(item.Descripcion);
                            t.AddCell("Precio: ");
                            t.AddCell(item.Precio.ToString());
                            t.AddCell("Stock: ");
                            t.AddCell(item.Stock.ToString());
                            t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                        }
                        doc.Add(t);
                        doc.Close();
                    }
                }
                MessageBox.Show("Archivo generado, mueble en el escritorio");
            }
        }

        private void ProduccionUIGetMuebles_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            CoreProduccion.bllProduccion core = new CoreProduccion.bllProduccion();
            listBox1.ValueMember = "Id";
            listBox1.DisplayMember = "Descripcion";
            listBox1.DataSource = core.GetMuebles();
        }
    }
}
