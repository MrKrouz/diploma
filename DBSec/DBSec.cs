﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecUtils;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace DBSec
{
    public class DBSec
    {
        #region UserRelated
        public void AddUser(EE.Usuario usuario)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().AddUser;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@userName", usuario.UserName.ToLower());
            cmd.Parameters.AddWithValue("@password", usuario.Password);
            cmd.Parameters.AddWithValue("@nombre", usuario.Nombre);
            cmd.Parameters.AddWithValue("@apellido", usuario.Apellido);
            cmd.Parameters.AddWithValue("@dni", usuario.Dni);
            cmd.Parameters.AddWithValue("@idIdioma", usuario.Idioma.ID);
            cmd.Parameters.AddWithValue("@DVH", new SecUtils.Digitos().GenerarDVHs(usuario));
            SqlParameter returnv = new SqlParameter("@id", SqlDbType.Int);
            returnv.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnv);
            cmd.ExecuteNonQuery();
            usuario.Id = (int)returnv.Value;
            cnX.Close();
        }
        public void AddDVV(string DVV)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().AddDVV;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@DVV", DVV);
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
        public string GetDVV()
        {
            var cnX = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetDVV, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            string dvv = "";
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                dvv = dr.GetString(0);
            }
            cnX.Close();
            return dvv;
        }
        public void UpdUser(EE.Usuario usuario)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().UpdUser;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@idUsuario", usuario.Id);
            cmd.Parameters.AddWithValue("@nombre", usuario.Nombre);
            cmd.Parameters.AddWithValue("@apellido", usuario.Apellido);
            cmd.Parameters.AddWithValue("@dni", usuario.Dni);
            cmd.Parameters.AddWithValue("@DVH", new SecUtils.Digitos().GenerarDVHs(usuario));
            cmd.Parameters.AddWithValue("@idIdioma", usuario.Idioma.ID);
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
        public void DelUser(EE.Usuario usuario)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().DelUser;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@idUsuario", usuario.Id);
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
        public bool LogIn(string userName, byte[] password)
        {
            try
            {
            new Utilidades.Logger().LogDB("User -> " + userName + " intento de LogIn", System.DateTime.Now);
            var db = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetUserPassword, db) { CommandType = CommandType.StoredProcedure };
            byte[] pass;
            cmd.Parameters.AddWithValue("@username", userName);
            db.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                pass = (byte[])dr.GetValue(0);
                if (System.Convert.ToBase64String(password) == System.Convert.ToBase64String(pass))
                {
                    new Utilidades.Logger().LogDB("User -> " + userName + "se ha conectado", System.DateTime.Now);
                    db.Close();
                    return true;
                }
                else
                {
                    new Utilidades.Logger().LogDB("User -> " + userName + "error al iniciar sesion", System.DateTime.Now);
                    db.Close();
                    return false;
                }
            }
            else
            {
                new Utilidades.Logger().LogDB("User -> " + userName + "error de base de datos", System.DateTime.Now);
                db.Close();
                return false;
            }
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogFile("No se ha logrado conectar", System.DateTime.Now, ex.Message);
                return false;
            }
        }
        public void ChangePassword(EE.Usuario user, byte[] newPassword)
        {
            var db = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().ChangePassword);
            db.Open();
            cmd.Parameters.AddWithValue("@userName", user.UserName);
            cmd.Parameters.AddWithValue("@newPassword", newPassword);
            new Utilidades.Logger().LogDB("User -> " + user.UserName + "ha cambiado su contraseña", System.DateTime.Now);
            cmd.ExecuteNonQuery();
            user.Password = newPassword;
            db.Close();

        }
        public List<EE.Usuario> GetUsers()
        {
            if (SecUtils.Singleton.Instance.GetInstance() != null)
            {
                new Utilidades.Logger().LogDB("Se han solicitado los usuarios del sistema", System.DateTime.Now);
            }
            var cs = new Utilidades.ConnectionString().GetConnection();
            var cn = new SqlConnection(cs);
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetUsuarios) { CommandType = CommandType.StoredProcedure, Connection = cn };
            List<EE.Usuario> lst = new List<EE.Usuario>();
            List<string> dvhs = new List<string>();
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lst.Add(new EE.Usuario { Id = dr.GetInt32(0), UserName = dr.GetString(1), Password = (byte[])dr.GetValue(2), Nombre = dr.GetString(3), Apellido = dr.GetString(4), Dni = dr.GetInt32(5), Idioma = new EE.Idioma { ID = dr.GetInt32(6) }, Perfil = new EE.Perfil { Id = dr.GetInt32(7) } });
            }
            cn.Close();
            return lst;
        }
        #endregion
        #region Permisos
        public void AltaPermiso(EE.Acceso permiso, int idPerfil)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().AddPermiso;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@descripcion", permiso.Descripcion);
            cmd.Parameters.AddWithValue("@IdPerfil", idPerfil);
            SqlParameter returnv = new SqlParameter("@id", SqlDbType.Int);
            returnv.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnv);
            cmd.ExecuteNonQuery();
            permiso.Id = (int)returnv.Value;
            cnX.Close();
        }
        public void ModificarPermisos(EE.Acceso permiso)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().UpdPermiso;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@Id", permiso.Id);
            cmd.Parameters.AddWithValue("@Descripcion", permiso.Descripcion);
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
        public void BajaPermiso(EE.Acceso permiso)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().UpdPermiso;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@Id", permiso.Id);
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
        public List<EE.Acceso> GetPermisos(EE.Acceso acceso)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().GetPermisos;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            var lista = new List<EE.Acceso>();
            cmd.Parameters.AddWithValue("@idPerfil", acceso.Id);
            cnX.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lista.Add(new EE.Permiso { Id = dr.GetInt32(0), Descripcion = dr.GetString(1) });
            }
            cnX.Close();

            foreach (var item in lista)
            {
                acceso.AddRole(item);
            }

            return lista;
        }
        #endregion
        #region Perfiles
        public void AltaPerfil(EE.Acceso perfil)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().AddPerfil;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@descripcion", perfil.Descripcion);
            SqlParameter returnv = new SqlParameter("@id", SqlDbType.Int);
            returnv.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnv);
            cmd.ExecuteNonQuery();
            perfil.Id = (int)returnv.Value;
            cnX.Close();

            foreach (var item in perfil.GetRoles())
            {
                if (item is EE.Permiso)
                {
                    AsignarPermiso(item.Id, perfil.Id);
                }
                if(item is EE.Perfil)
                {
                    RelacionarPerfiles(perfil.Id, item.Id);
                }
            }
        }
        public void AsignarPermiso(int idPermiso, int idPerfil)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AsignarPermiso, cn) { CommandType = CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdPermiso", idPermiso);
            cmd.Parameters.AddWithValue("@IdPerfil", idPerfil);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void RelacionarPerfiles(int idPerfilBase, int IdPerfilRel)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().RelacionarPerfil, cn) { CommandType = CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdPerfilBase", idPerfilBase);
            cmd.Parameters.AddWithValue("@IdPerfilRel", IdPerfilRel);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void AsignarPerfil(int IdPerfil, int IdUsuario)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AsignarPerfil, cn) { CommandType = CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil);
            cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void ModificarPerfil(EE.Acceso perfil)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().AddPerfil;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@descripcion", perfil.Descripcion);
            cmd.Parameters.AddWithValue("@id", perfil.Id);
            cmd.ExecuteNonQuery();
            cnX.Close();
            foreach (var item in perfil.GetRoles())
            {
                if (item is EE.Permiso)
                {
                    ModificarPermisos(item);
                }
                else
                {
                    ModificarPerfil(item);
                }
            }
        }
        public void BajaPerfil(EE.Acceso acceso)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().DelPerfil;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@Id", acceso.Id);
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
        public void BorrarRelaciones(int idPerfil)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().BorrarRelaciones;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cnX.Open();
            cmd.Parameters.AddWithValue("@IdPerfil", idPerfil);
            cmd.ExecuteNonQuery();
            cnX.Close();
        }
        public List<EE.Acceso> GetPerfiles()
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().GetPerfiles;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            var lista = new List<EE.Acceso>();
            cnX.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lista.Add(new EE.Perfil { Id = dr.GetInt32(0), Descripcion = dr.GetString(1) });
            }
            cnX.Close();

            foreach (var item in lista)
            {
                GetPermisos(item);
                if(item is EE.Perfil)
                {
                    var lista2 = GetPerfilesRel(item);
                    if (lista2.Count > 0)
                    {
                        foreach(var item2 in lista2)
                        {
                            item.AddRole(lista.Find(i=> i.Id == item2.Id));
                        }
                    }
                }
            }


            return lista;
        }
        private List<EE.Perfil> GetPerfilesRel(EE.Acceso perfil)
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().GetPerfilesRel;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@IdPerfilBase", perfil.Id);
            var lista = new List<EE.Perfil>();
            cnX.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lista.Add(new EE.Perfil { Id = dr.GetInt32(0), Descripcion = dr.GetString(1) });
            }
            cnX.Close();
            return lista;
        }
        public List<EE.Acceso> GetPerfilesBase()
        {
            var cnS = new Utilidades.ConnectionString().GetConnection();
            var sp = new Utilidades.StoredProcedures().GetPerfilesBase;
            var cnX = new SqlConnection(cnS);
            var cmd = new SqlCommand(sp, cnX) { CommandType = CommandType.StoredProcedure };
            var lista = new List<EE.Acceso>();
            cnX.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lista.Add(new EE.Perfil { Id = dr.GetInt32(0), Descripcion = dr.GetString(1) });
            }
            cnX.Close();

            foreach (var item in lista)
            {
                GetPermisos(item);
            }

            return lista;
        }
        public List<EE.Acceso> GetPerfilByUsuario(int IdUsuario)
        {
            var perfilUsuario = new EE.Perfil();
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetPerfilByUsuario, cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);
            var listaPerfiles = new List<EE.Acceso>();
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                listaPerfiles.Add(new EE.Perfil { Id = dr.GetInt32(0), Descripcion = dr.GetString(1) });
            }
            foreach (var item in listaPerfiles)
            {
                GetPermisos(item);
            }
            return listaPerfiles;
        }
        #endregion
        #region BackUps
        public void GenerateBackUp()
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GenerateBackUp, cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@BkNombre", "backUp");
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public void RestoreBackUp()
        {
            string outputFile = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var cn = new SqlConnection("Data Source = localhost; Initial Catalog = master; Integrated Security = True");
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().RestoreBackUp, cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("@BkNombre", "backUp");
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        #endregion
    }
}
