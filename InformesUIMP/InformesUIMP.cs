﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InformesUIMP
{
    public partial class InformesUIMP : Form
    {
        public InformesUIMP()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = new CoreProduccion.bllProduccion().GetMateriales();
            comboBox1.Text = string.Empty;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var item = (EE.MateriaPrima)comboBox1.SelectedItem;
            if (listBox1.Items.Contains(item.Descripcion) == false)
            {
                listBox1.Items.Add(item.Descripcion);
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                var item = listBox1.SelectedItem;
                listBox1.Items.Remove(item);
            }
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            var informe = new EE.InformeMRP();
            if (listBox1.Items.Count > 0)
            {

                foreach (var item in listBox1.Items)
                {
                    var elemento = new CoreProduccion.bllProduccion().GetMateriales().Find(m => m.Descripcion == item.ToString());
                    informe.Detalle.Add(new EE.DetalleMRP { Descripcion = elemento.Descripcion, Stock = elemento.Stock, Precio = elemento.Precio, Id = elemento.Id });
                }
                var coreInforme = new CoreInformes.BllInformes();
                coreInforme.GenerarInforme(informe);

                string outputFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Informe -" + informe.Id.ToString() + ".pdf");
                using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (Document doc = new Document(PageSize.A4))
                    {
                        using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                        {
                            doc.Open();
                            PdfPTable t1 = new PdfPTable(4);
                            t1.DefaultCell.Border = 0;
                            t1.DefaultCell.BorderWidthBottom = 1;
                            t1.DefaultCell.BorderColorBottom = BaseColor.RED;
                            t1.AddCell("Codigo:");
                            t1.AddCell(informe.Id.ToString());
                            t1.AddCell("Fecha generado:");
                            t1.AddCell(System.DateTime.Now.ToString());
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            doc.Add(t1);
                            PdfPTable t2 = new PdfPTable(2);
                            t2.DefaultCell.Border = 0;
                            t2.DefaultCell.BorderWidthBottom = 1;
                            t2.DefaultCell.BorderColorBottom = BaseColor.RED;
                            foreach (var item in informe.Detalle)
                            {
                                t2.AddCell("Descripcion: ");
                                t2.AddCell(item.Descripcion);
                                t2.AddCell("Precio: ");
                                t2.AddCell(item.Precio.ToString());
                                t2.AddCell("Stock: ");
                                t2.AddCell(item.Stock.ToString());
                                t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                                t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            }
                            doc.Add(t2);
                            doc.Close();
                        }
                    }
                }
                MessageBox.Show("Informe generado");
            }
            else
            {
                MessageBox.Show("No se puede generar un informe sin elementos");
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "InformesUI").Show();
            this.Close();
        }
    }
}
