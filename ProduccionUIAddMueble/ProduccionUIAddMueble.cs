﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIAddMueble
{
    public partial class ProduccionUIAddMueble : FormBase.FormBase
    {
        public ProduccionUIAddMueble()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);
        }

        private void ProduccionUIAddMueble_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            ActualizarCB();
        }
        private void ActualizarCB()
        {
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreProd.GetMateriales();
            comboBox1.Text = string.Empty;
        }
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            var mueble = new EE.Mueble();
            var coreProd = new CoreProduccion.bllProduccion();

            #region ValidacionDeCampos
            bool flag = true;
            double item = 0;
            if (double.TryParse(textBox1.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (double.TryParse(textBox2.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (double.TryParse(textBox3.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (double.TryParse(textBox5.Text, out item) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }
            if (textBox4.Text == string.Empty)
            {
                MessageBox.Show("Debe ingresar una descripcion");
                flag = false;
            }
            int item2 = 0;
            if (int.TryParse(textBox6.Text, out item2) == false)
            {
                MessageBox.Show("Debe ingresar un numero");
                flag = false;
            }

            #endregion
            if (flag == true)
            {
                mueble.Madera.Id = (int)comboBox1.SelectedValue;
                mueble.MedidaH = Convert.ToDouble(textBox1.Text);
                mueble.MedidaL = Convert.ToDouble(textBox2.Text);
                mueble.MedidaP = Convert.ToDouble(textBox3.Text);
                mueble.Descripcion = textBox4.Text;
                mueble.Precio = Convert.ToDouble(textBox5.Text);
                mueble.Stock = Convert.ToInt32(textBox6.Text);
                coreProd.AddMueble(mueble);
                MessageBox.Show("Mueble cargado");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
