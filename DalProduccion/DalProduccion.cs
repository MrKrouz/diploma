﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalProduccion
{
    public class DalProduccion
    {
        #region Orden de compra

        public void GenerarOrdenDeCompra(EE.OrdenDeCompra OrdenDeCompra)
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddOrdenDeCompra, cn) { CommandType = System.Data.CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@Fecha", OrdenDeCompra.Fecha);
                cmd.Parameters.AddWithValue("@IdEmpleado", OrdenDeCompra.Empleado.Id);
                cmd.Parameters.AddWithValue("@Total", OrdenDeCompra.MontoTotal);
                cmd.Parameters.AddWithValue("@IdInforme", OrdenDeCompra.Informe.Id);
                SqlParameter returnv = new SqlParameter("@Id", SqlDbType.Int);
                returnv.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnv);
                cmd.ExecuteNonQuery();
                OrdenDeCompra.Id = (int)returnv.Value;
                cn.Close();
                foreach (var item in OrdenDeCompra.Detalle)
                {
                    InsertarDetalleOC(item, OrdenDeCompra.Id);
                }
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al generar la orden de compra -> " + ex.Message, System.DateTime.Now);
            }
        }
        private void InsertarDetalleOC(EE.DetalleOrdenDeCompra doc, int IdOC)
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddDetalleOC, cn) { CommandType = System.Data.CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@IdOrdenDeCompra", IdOC);
                cmd.Parameters.AddWithValue("@Cantidad", doc.Cantidad);
                cmd.Parameters.AddWithValue("@IdMaterial", doc.Material.Id);
                cmd.Parameters.AddWithValue("@PrecioTotal", doc.PrecioTotal);
                SqlParameter returnv = new SqlParameter("@Id", SqlDbType.Int);
                returnv.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnv);
                cmd.ExecuteNonQuery();
                doc.Id = (int)returnv.Value;
                cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al insertar el detalle de la orden de compra -> " + ex.Message, System.DateTime.Now);
            }
        }
        public List<EE.OrdenDeProduccion> ListarOrdenesDeProduccion()
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetOrdenesDeProduccion, cn) { CommandType = System.Data.CommandType.StoredProcedure };
                List<EE.OrdenDeProduccion> listaOP = new List<EE.OrdenDeProduccion>();
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    listaOP.Add(new EE.OrdenDeProduccion { Id = dr.GetInt32(0), Empleado = new EE.Usuario { Id = dr.GetInt32(1) }, Fecha = dr.GetDateTimeOffset(2).DateTime });
                }
                foreach (var item in listaOP)
                {
                    ListarDetalleOP(item);
                }
                cn.Close();
                return listaOP;
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las ordenes de produccion -> " + ex.Message, System.DateTime.Now);
                return new List<EE.OrdenDeProduccion>();
            }
        }
        private void ListarDetalleOP(EE.OrdenDeProduccion op)
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetDetallesOP, cn) { CommandType = System.Data.CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@IdOP", op.Id);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    op.Detalle.Add(new EE.DetalleOrdenProduccion { Id = dr.GetInt32(0), Cantidad = dr.GetInt32(1), Mueble = new EE.Mueble { Id = dr.GetInt32(2) } });
                }
                cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las ordenes de produccion -> " + ex.Message, System.DateTime.Now);
            }
        }
        #endregion
        #region Materiales
        public void AddMaterial(EE.MateriaPrima MP)
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddMaterial, cn) { CommandType = CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@Descripcion", MP.Descripcion);
                cmd.Parameters.AddWithValue("@Precio", MP.Precio);
                cmd.Parameters.AddWithValue("@Stock", MP.Stock);
                SqlParameter returnv = new SqlParameter("@Id", SqlDbType.Int);
                returnv.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnv);
                cmd.ExecuteNonQuery();
                MP.Id = (int)returnv.Value;
                cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las ordenes de produccion -> " + ex.Message, System.DateTime.Now);
            }
        }
        public void UpdMaterial(EE.MateriaPrima MP)
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().UpdMaterial, cn) { CommandType = CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@Id", MP.Id);
                cmd.Parameters.AddWithValue("@Descripcion", MP.Descripcion);
                cmd.Parameters.AddWithValue("@Precio", MP.Precio);
                cmd.Parameters.AddWithValue("@Stock", MP.Stock);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las ordenes de produccion -> " + ex.Message, System.DateTime.Now);

            }
        }
        public void DelMaterial(EE.MateriaPrima MP)
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().DelMaterial, cn) { CommandType = CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@Id", MP.Id);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las ordenes de produccion -> " + ex.Message, System.DateTime.Now);
            }
        }
        public List<EE.MateriaPrima> GetMateriales()
        {
            try
            {

                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBUsers"].ConnectionString);
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetMateriales, cn) { CommandType = CommandType.StoredProcedure };
                var lst = new List<EE.MateriaPrima>();
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    lst.Add(new EE.MateriaPrima { Id = dr.GetInt32(0), Descripcion = dr.GetString(1), Precio = dr.GetDouble(2), Stock = dr.GetInt32(3) });
                }
                cn.Close();
                return lst;
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las ordenes de produccion -> " + ex.Message, System.DateTime.Now);
                return new List<EE.MateriaPrima>();
            }
        }
        #endregion
        #region Muebles
        public void AddMueble(EE.Mueble mueble)
        {
            try
            {

                var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddMueble, cn) { CommandType = CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@Descripcion", mueble.Descripcion);
                cmd.Parameters.AddWithValue("@IdMateriaPrima", mueble.Madera.Id);
                cmd.Parameters.AddWithValue("@MedidaH", mueble.MedidaH);
                cmd.Parameters.AddWithValue("@MedidaL", mueble.MedidaL);
                cmd.Parameters.AddWithValue("@MedidaP", mueble.MedidaP);
                cmd.Parameters.AddWithValue("@Precio", mueble.Precio);
                cmd.Parameters.AddWithValue("@Stock", mueble.Stock);
                SqlParameter returnv = new SqlParameter("@Id", SqlDbType.Int);
                returnv.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(returnv);
                cmd.ExecuteNonQuery();
                mueble.Id = (int)returnv.Value;
                cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al cargar el mueble -> " + ex.Message, System.DateTime.Now);
            }
        }
        public void UpdMueble(EE.Mueble mueble)
        {
            try
            {

                var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().UpdMueble, cn) { CommandType = CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@Id", mueble.Id);
                cmd.Parameters.AddWithValue("@Descripcion", mueble.Descripcion);
                cmd.Parameters.AddWithValue("@IdMateriaPrima", mueble.Madera.Id);
                cmd.Parameters.AddWithValue("@MedidaH", mueble.MedidaH);
                cmd.Parameters.AddWithValue("@MedidaL", mueble.MedidaL);
                cmd.Parameters.AddWithValue("@MedidaP", mueble.MedidaP);
                cmd.Parameters.AddWithValue("@Precio", mueble.Precio);
                cmd.Parameters.AddWithValue("@Stock", mueble.Stock);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al modificar el mueble -> " + ex.Message, System.DateTime.Now);

            }
        }
        public void DelMueble(EE.Mueble mueble)
        {
            try
            {

            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().DelMueble, cn) { CommandType = CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", mueble.Id);
            cmd.ExecuteNonQuery();
            cn.Close();
            }
            catch(SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al eliminar el mueble -> " + ex.Message, System.DateTime.Now);

            }
        }
        public List<EE.Mueble> GetMuebles()
        {
            try
            {

            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetMuebles, cn) { CommandType = CommandType.StoredProcedure };
            List<EE.Mueble> muebles = new List<EE.Mueble>();
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                muebles.Add(new EE.Mueble { Id = dr.GetInt32(0), Descripcion = dr.GetString(1), Precio = dr.GetDouble(2), MedidaH = dr.GetDouble(3), MedidaL = dr.GetDouble(4), MedidaP = dr.GetDouble(5), Madera = new EE.MateriaPrima { Id = dr.GetInt32(6) }, Stock = dr.GetInt32(7) });
            }
            cn.Close();
            foreach (var item in muebles)
            {
                item.Madera = GetMateriales().Find(m => m.Id == item.Madera.Id);
            }
            return muebles;
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al obtener las ordenes de produccion -> " + ex.Message, System.DateTime.Now);
                return new List<EE.Mueble>();
            }
        }
        #endregion

    }
}

