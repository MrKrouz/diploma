﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIAddMat
{
    public partial class ProduccionUIAddMat : FormBase.FormBase
    {
        public ProduccionUIAddMat()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            #region ValidacionDeCampos
            bool flag = true;
            if (textBox1.Text == string.Empty)
            {
                flag = false;
                MessageBox.Show("Debe ingresar una descripcion");
            }
            double item = 0;
            if (Double.TryParse(textBox2.Text, out item) == false)
            {
                flag = false;
                MessageBox.Show("Debe ingresar un numero");
            }
            int item2 = 0;
            if (int.TryParse(textBox3.Text, out item2) == false)
            {
                flag = false;
                MessageBox.Show("Debe ingresar un numero");
            }
            #endregion
            if (flag == true)
            {
                var coreProd = new CoreProduccion.bllProduccion();
                coreProd.AddMaterial(new EE.MateriaPrima { Descripcion = textBox1.Text, Precio = Convert.ToDouble(textBox2.Text), Stock = Convert.ToInt32(textBox3.Text) });
                MessageBox.Show("Material agregado");

            }
        }

        private void CerrarForm()
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CerrarForm();
        }

        private void ProduccionUIAddMat_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);


        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f=> f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
