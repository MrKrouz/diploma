﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Servicios
{
    public class RepositorioIdioma
    {
        public Dictionary<string, string> ObtenerIdioma(string idioma)
        {
            return ObtenerIdiomaDB(idioma);
        }


        private Dictionary<string, string> ObtenerIdiomaDB(string idioma)
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            Dictionary<string, string> Cultura = new Dictionary<string, string>();
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GetIdioma", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@Cultura", idioma);
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            Cultura.Add(dr.GetString(0), dr.GetString(1));
                        }
                    }
                    cn.Close();
                    return Cultura;

                }
                catch (SqlException ex)
                {
                    Cultura.Add("Error:", ex.Message);
                    return Cultura;
                }
            }
        }
    }
}
