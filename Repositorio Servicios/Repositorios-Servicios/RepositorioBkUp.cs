﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Servicios
{
    public class RepositorioBkUp
    {
        public void GenerarBkUp()
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GenerarBackUp", cn) { CommandType = CommandType.StoredProcedure })
                    {
                        cm.ExecuteNonQuery();
                    }
                    cn.Close();
                }
                catch (SqlException ex)
                {
                    cn.Close();   
                }
            }
        }
    }
}
