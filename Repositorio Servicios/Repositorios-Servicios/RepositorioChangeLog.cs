﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Servicios
{
    class RepositorioChangeLog
    {
        public void Agregar(string clase, string estadoAnterior, string estadoActual, string accion)
        {
            AgregarChangeLogDB(clase, estadoAnterior, estadoActual, accion);
        }

        public IEnumerable<string> Buscar(DateTime fecha)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> Todos()
        {
            throw new NotImplementedException();
        }


        private void AgregarChangeLogDB(string clase, string estadoAnterior, string estadoActual, string accion)
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GenerarChangeLog", cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@Clase", clase);
                        cm.Parameters.AddWithValue("@EstadoAnterior", estadoAnterior);
                        cm.Parameters.AddWithValue("@EstadoActual", estadoActual);
                        cm.Parameters.AddWithValue("@Accion", accion);
                        cm.ExecuteNonQuery();
                    }
                    cn.Close();
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (SqlException ex)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                    var repoLogs = new RepositorioLogs();
                    repoLogs.Agregar(ex.Message, DateTime.Today);
                    cn.Close();
                }
            }
        }
    }
}
