﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Servicios
{
    public interface IRepositorio
    {
       void Agregar(string mensaje, DateTime fechaLog);

        IEnumerable<string> Buscar(DateTime fecha);

        IEnumerable<string> Todos();
    }
}
