﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Servicios
{
    public class RepositorioLogs
    {
        #region Metodos Publicos
        public void Agregar(string mensaje, DateTime fechaLog)
        {
            AgregarLogDB(mensaje, fechaLog);
        }

        public IEnumerable<string> Buscar(DateTime fecha)
        {
            return BuscarDB(fecha);
        }

        public IEnumerable<string> Todos()
        {
            return ObtenerTodos();
        }
        #endregion

        #region Metodos Privados
        private void AgregarLogDB(string mensaje, DateTime fechaLog)
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("GenerarLog", cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@Mensaje", mensaje);
                        cm.Parameters.AddWithValue("@Fecha", fechaLog);
                        cm.ExecuteNonQuery();
                    }
                    cn.Close();
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (SqlException ex)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                    cn.Close();
                }
            }
        }

        private IEnumerable<string> ObtenerTodos()
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            List<string> listaLogs = new List<string>();
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("LeerLogs", cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            listaLogs.Add(dr.GetString(0));
                        }
                    }
                    cn.Close();
                    return listaLogs;
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (SqlException ex)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                    cn.Close();
                    return null;
                }
            }
        }

        private IEnumerable<string> BuscarDB(DateTime fechaLog)
        {
            var cns = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;
            List<string> listaLogs = new List<string>();
            using (SqlConnection cn = new SqlConnection(cns))
            {
                try
                {
                    cn.Open();
                    using (SqlCommand cm = new SqlCommand("LeerLogs", cn) { CommandType = System.Data.CommandType.StoredProcedure })
                    {
                        cm.Parameters.AddWithValue("@fechaLog", fechaLog);
                        SqlDataReader dr = cm.ExecuteReader();
                        while (dr.Read())
                        {
                            listaLogs.Add(dr.GetString(0));
                        }
                    }
                    cn.Close();
                    return listaLogs;
                }
                catch (SqlException ex)
                {
                    cn.Close();
                    return null;
                }
            }
        }
        #endregion
    }
}
