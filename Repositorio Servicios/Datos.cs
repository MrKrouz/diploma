﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio_Servicios
{
    public class Datos
    {
        public void GenerarLog(string mensaje, DateTime fecha)
        {
            var db = new RepositorioLogs();
            db.Agregar(mensaje, fecha);
        }

        public void GenerarBackUp()
        {
            var db = new RepositorioBkUp();
            db.GenerarBkUp();
        }

        public void GenerarChangeLog(string clase, string estadoAnterior, string estadoActual, string accion)
        {
            var db = new RepositorioChangeLog();
            db.Agregar(clase, estadoAnterior, estadoActual, accion);
        }

        public List<string> obtenerLogs()
        {
            var db = new RepositorioLogs();
            return db.Todos().ToList();
        }
    }
}
