﻿using Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public abstract class AbstractPerfil
    {
        public abstract void Add(AbstractPerfil item);

        public abstract void Remove(AbstractPerfil item);

        public abstract List<AbstractPerfil> DevolverLista();
    }
}
