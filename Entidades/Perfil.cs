﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Perfil : AbstractPerfil
    {
        private readonly List<AbstractPerfil> _listaPerfiles = new List<AbstractPerfil>();

        public int idPerfil { get; set; }

        public string Descripcion { get; set; }

        public override void Add(AbstractPerfil item)
        {
            _listaPerfiles.Add(item);
        }

        public override List<AbstractPerfil> DevolverLista()
        {
            return _listaPerfiles;
        }

        public override void Remove(AbstractPerfil item)
        {
            _listaPerfiles.Remove(item);
        }
    }
}
