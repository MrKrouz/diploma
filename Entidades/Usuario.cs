﻿using Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Entidades
{
    public class Usuario : IUsuario
    {
        public int IdUsuario { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string NombreUsuario { get; set; }

        public string Password { get; set; }

        public string IdiomaSeleccionado { get; set; }

        public IEnumerable Permisos { get; set; }

        public override string ToString()
        {
            return "Nombre: " + Nombre + "; Apellido:" + Apellido + "; NombreUsuario: " + this.NombreUsuario + "; IdiomaSeleccionado: " + IdiomaSeleccionado + ";";
        }
    }
}
