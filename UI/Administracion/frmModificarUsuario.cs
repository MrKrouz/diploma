﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Administracion
{
    public partial class frmModificarUsuario : Form
    {
        public frmModificarUsuario()
        {
            InitializeComponent();
        }

        private void frmModificarUsuario_Load(object sender, EventArgs e)
        {
            var coreSeguridad = new CoreSeguridad.GestionSeguridad();
            comboBox1.DataSource =  coreSeguridad.ObtenerPerfiles();
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.ValueMember = "idPerfil";

            var coreUsuarios = new Core.GestionUsuario();
            comboBox2.DataSource = coreUsuarios.ObtenerUsuarios();
            comboBox2.DisplayMember = "NombreUsuario";
            comboBox2.ValueMember = "IdUsuario";
            BuscarIdioma(Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado);
        }
        private void BuscarIdioma(string idioma)
        {
            var diccionarioDatos = new CoreServicios.GestionIdioma().ObtenerIdioma(idioma);
            foreach (KeyValuePair<string, string> Idioma in diccionarioDatos)
            {
                foreach (Control control in Controls)
                {
                    if (control.Name != "msHeader")
                    {
                        if (control.Name == Idioma.Key)
                        {
                            control.Text = Idioma.Value;
                        }
                    }
                    else
                    {
                        foreach (ToolStripItem item in msHeader.Items)
                        {
                            if (item.Name == Idioma.Key)
                            {
                                item.Text = Idioma.Value;
                            }
                        }
                    }
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnModificarUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != "")
                {
                    if (textBox2.Text != "")
                    {
                        if (textBox3.Text != "")
                        {
                            var userNew = new Usuario();
                            var core = new Core.GestionUsuario();
                            var userOld = core.ObtenerUsuario((int)comboBox2.SelectedValue);
                            userNew = (Usuario)comboBox2.SelectedItem;
                            userNew.IdUsuario = comboBox2.SelectedIndex + 1;
                            userNew.Apellido = textBox2.Text;
                            userNew.Password = textBox3.Text;
                            userNew.Nombre = textBox1.Text;
                            int perfil = comboBox2.SelectedIndex + 1;
                            core.ModificarUsuario(userOld, userNew, perfil);
                        }
                        else
                        {
                            MessageBox.Show("Debe introducir una contraseña");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe introducir un apellido");
                    }
                }
                else
                {
                    MessageBox.Show("Debe introducir un nombre");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void logOUtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Seguridad.Singleton.Instance.Deslogear();
            var frmLogin = new frmLogin();
            frmLogin.Show();
            this.Close();

        }

        private void enUSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("en-US");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "en-US";
        }

        private void esARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("es-AR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "es-AR";
        }

        private void frFRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("fr-FR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "fr-FR";
        }
    }
}
