﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class frmAdmin : Form
    {
        public frmAdmin()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            BuscarIdioma(Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado);

        }

        private void BuscarIdioma(string idioma)
        {
            var diccionarioDatos = new CoreServicios.GestionIdioma().ObtenerIdioma(idioma);
            foreach (KeyValuePair<string, string> Idioma in diccionarioDatos)
            {
                foreach (Control control in Controls)
                {
                    if (control.Name != "msHeader")
                    {
                        if (control.Name == Idioma.Key)
                        {
                            control.Text = Idioma.Value;
                        }
                    }
                    else
                    {
                        foreach (ToolStripItem item in msHeader.Items)
                        {
                            if (item.Name == Idioma.Key)
                            {
                                item.Text = Idioma.Value;
                            }
                        }
                    }
                }
            }
        }

        private void btnModificarUsuario_Click(object sender, EventArgs e)
        {
            var frmMod = new Administracion.frmModificarUsuario();
            frmMod.Show();
        }

        private void btnAltaUsuario_Click(object sender, EventArgs e)
        {
            var frmAlta = new Administracion.frmAltaUsuario();
            frmAlta.Show();
        }

        private void btnBajaUsuario_Click(object sender, EventArgs e)
        {
            var frmbaja = new Administracion.frmBajaUsuario();
            frmbaja.Show();
        }

        private void btnListarUsuarios_Click(object sender, EventArgs e)
        {
            var coreUsuarios = new Core.GestionUsuario();
            dataGridView1.DataSource = "";
            dataGridView1.DataSource = coreUsuarios.ObtenerUsuarios();
        }

        private void btnBkUp_Click(object sender, EventArgs e)
        {
            var seg = new Seguridad.BackUp();
            seg.GenerarBackUp();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Seguridad.Singleton.Instance.Deslogear();
            var frmLogin = new frmLogin();
            frmLogin.Show();
            this.Close();
        }

        private void esARToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            BuscarIdioma("es-AR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "es-AR";
        }

        private void enUSToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            BuscarIdioma("en-US");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "en-US";

        }

        private void frFRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("fr-FR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "fr-FR";
        }

        private void btnObtenerLogs_Click(object sender, EventArgs e)
        {
            var coreServicios = new CoreServicios.GestionLogs();
           foreach (var item in coreServicios.ObtenerLogs())
            {
                listBox1.Items.Add(item);
            }

        }
    }
}
