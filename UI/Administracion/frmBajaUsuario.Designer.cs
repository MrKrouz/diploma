﻿namespace UI.Administracion
{
    partial class frmBajaUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.btnBajaUsuario = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.msHeader = new System.Windows.Forms.MenuStrip();
            this.idiomaMS = new System.Windows.Forms.ToolStripMenuItem();
            this.esARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enUSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frFRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutMS = new System.Windows.Forms.ToolStripMenuItem();
            this.msHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(60, 61);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 11;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(57, 40);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(43, 13);
            this.lblUsuario.TabIndex = 10;
            this.lblUsuario.Text = "Usuario";
            // 
            // btnBajaUsuario
            // 
            this.btnBajaUsuario.Location = new System.Drawing.Point(60, 105);
            this.btnBajaUsuario.Name = "btnBajaUsuario";
            this.btnBajaUsuario.Size = new System.Drawing.Size(75, 23);
            this.btnBajaUsuario.TabIndex = 12;
            this.btnBajaUsuario.Text = "Baja Usuario";
            this.btnBajaUsuario.UseVisualStyleBackColor = true;
            this.btnBajaUsuario.Click += new System.EventHandler(this.btnBajaUsuario_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(60, 134);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // msHeader
            // 
            this.msHeader.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.idiomaMS,
            this.logoutMS});
            this.msHeader.Location = new System.Drawing.Point(0, 0);
            this.msHeader.Name = "msHeader";
            this.msHeader.Size = new System.Drawing.Size(244, 24);
            this.msHeader.TabIndex = 14;
            this.msHeader.Text = "menuStrip1";
            // 
            // idiomaMS
            // 
            this.idiomaMS.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.esARToolStripMenuItem,
            this.enUSToolStripMenuItem,
            this.frFRToolStripMenuItem});
            this.idiomaMS.Name = "idiomaMS";
            this.idiomaMS.Size = new System.Drawing.Size(56, 20);
            this.idiomaMS.Text = "Idioma";
            // 
            // esARToolStripMenuItem
            // 
            this.esARToolStripMenuItem.Name = "esARToolStripMenuItem";
            this.esARToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.esARToolStripMenuItem.Text = "es-AR";
            this.esARToolStripMenuItem.Click += new System.EventHandler(this.esARToolStripMenuItem_Click);
            // 
            // enUSToolStripMenuItem
            // 
            this.enUSToolStripMenuItem.Name = "enUSToolStripMenuItem";
            this.enUSToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.enUSToolStripMenuItem.Text = "en-US";
            this.enUSToolStripMenuItem.Click += new System.EventHandler(this.enUSToolStripMenuItem_Click);
            // 
            // frFRToolStripMenuItem
            // 
            this.frFRToolStripMenuItem.Name = "frFRToolStripMenuItem";
            this.frFRToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.frFRToolStripMenuItem.Text = "fr-FR";
            this.frFRToolStripMenuItem.Click += new System.EventHandler(this.frFRToolStripMenuItem_Click);
            // 
            // logoutMS
            // 
            this.logoutMS.Name = "logoutMS";
            this.logoutMS.Size = new System.Drawing.Size(60, 20);
            this.logoutMS.Text = "Log out";
            this.logoutMS.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // frmBajaUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 179);
            this.Controls.Add(this.msHeader);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnBajaUsuario);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.lblUsuario);
            this.Name = "frmBajaUsuario";
            this.Text = "frmBajaUsuario";
            this.Load += new System.EventHandler(this.frmBajaUsuario_Load);
            this.msHeader.ResumeLayout(false);
            this.msHeader.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Button btnBajaUsuario;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.MenuStrip msHeader;
        private System.Windows.Forms.ToolStripMenuItem idiomaMS;
        private System.Windows.Forms.ToolStripMenuItem esARToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enUSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frFRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutMS;
    }
}