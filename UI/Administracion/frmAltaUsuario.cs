﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Administracion
{
    public partial class frmAltaUsuario : Form
    {
        public frmAltaUsuario()
        {
            InitializeComponent();
        }

        private void frmAltaUsuario_Load(object sender, EventArgs e)
        {
            var coreSeguridad = new CoreSeguridad.GestionSeguridad();
            comboBox1.DataSource = coreSeguridad.ObtenerPerfiles();
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.ValueMember = "idPerfil";
            BuscarIdioma(Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado);
        }
        private void BuscarIdioma(string idioma)
        {
            var diccionarioDatos = new CoreServicios.GestionIdioma().ObtenerIdioma(idioma);
            foreach (KeyValuePair<string, string> Idioma in diccionarioDatos)
            {
                foreach (Control control in Controls)
                {
                    if (control.Name != "msHeader")
                    {
                        if (control.Name == Idioma.Key)
                        {
                            control.Text = Idioma.Value;
                        }
                    }
                    else
                    {
                        foreach (ToolStripItem item in msHeader.Items)
                        {
                            if (item.Name == Idioma.Key)
                            {
                                item.Text = Idioma.Value;
                            }
                        }
                    }
                }
            }
        }

        private void btnAltaUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != "")
                {
                    if (textBox2.Text != "")
                    {
                        if (textBox3.Text != "")
                        {
                            if (textBox4.Text != "")
                            {
                                var coreUsuario = new Core.GestionUsuario();
                                coreUsuario.AltaUsuario(new Entidades.Usuario { Nombre = textBox1.Text, NombreUsuario = textBox3.Text, Password = textBox4.Text, Apellido = textBox2.Text }, (int)comboBox1.SelectedValue);
                            }
                            else
                            {
                                MessageBox.Show("Debe introducir un apellido");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Debe introducir un nombre");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Debe introducir una contraseña");
                    }
                }
                else
                {
                    MessageBox.Show("Debe introducir un nombre de usuario");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
                        
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void esARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("es-AR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "es-AR";
        }

        private void enUSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("en-US");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "en-US";
        }

        private void frFRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("fr-FR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "fr-FR";
        }
    }
}
