﻿namespace UI
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAltaUsuario = new System.Windows.Forms.Button();
            this.msHeader = new System.Windows.Forms.MenuStrip();
            this.idiomaMS = new System.Windows.Forms.ToolStripMenuItem();
            this.esARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enUSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frFRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnModificarUsuario = new System.Windows.Forms.Button();
            this.btnBajaUsuario = new System.Windows.Forms.Button();
            this.btnListarUsuarios = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnBkUp = new System.Windows.Forms.Button();
            this.logoutMS = new System.Windows.Forms.ToolStripMenuItem();
            this.btnObtenerLogs = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.msHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAltaUsuario
            // 
            this.btnAltaUsuario.Location = new System.Drawing.Point(12, 46);
            this.btnAltaUsuario.Name = "btnAltaUsuario";
            this.btnAltaUsuario.Size = new System.Drawing.Size(179, 23);
            this.btnAltaUsuario.TabIndex = 0;
            this.btnAltaUsuario.Text = "Alta Usuario";
            this.btnAltaUsuario.UseVisualStyleBackColor = true;
            this.btnAltaUsuario.Click += new System.EventHandler(this.btnAltaUsuario_Click);
            // 
            // msHeader
            // 
            this.msHeader.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.idiomaMS,
            this.logoutMS});
            this.msHeader.Location = new System.Drawing.Point(0, 0);
            this.msHeader.Name = "msHeader";
            this.msHeader.Size = new System.Drawing.Size(612, 24);
            this.msHeader.TabIndex = 1;
            this.msHeader.Text = "menuStrip1";
            // 
            // idiomaMS
            // 
            this.idiomaMS.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.esARToolStripMenuItem,
            this.enUSToolStripMenuItem,
            this.frFRToolStripMenuItem});
            this.idiomaMS.Name = "idiomaMS";
            this.idiomaMS.Size = new System.Drawing.Size(56, 20);
            this.idiomaMS.Text = "Idioma";
            // 
            // esARToolStripMenuItem
            // 
            this.esARToolStripMenuItem.Name = "esARToolStripMenuItem";
            this.esARToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.esARToolStripMenuItem.Text = "es-AR";
            this.esARToolStripMenuItem.Click += new System.EventHandler(this.esARToolStripMenuItem_Click_1);
            // 
            // enUSToolStripMenuItem
            // 
            this.enUSToolStripMenuItem.Name = "enUSToolStripMenuItem";
            this.enUSToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.enUSToolStripMenuItem.Text = "en-US";
            this.enUSToolStripMenuItem.Click += new System.EventHandler(this.enUSToolStripMenuItem_Click_1);
            // 
            // frFRToolStripMenuItem
            // 
            this.frFRToolStripMenuItem.Name = "frFRToolStripMenuItem";
            this.frFRToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.frFRToolStripMenuItem.Text = "fr-FR";
            this.frFRToolStripMenuItem.Click += new System.EventHandler(this.frFRToolStripMenuItem_Click);
            // 
            // btnModificarUsuario
            // 
            this.btnModificarUsuario.Location = new System.Drawing.Point(12, 75);
            this.btnModificarUsuario.Name = "btnModificarUsuario";
            this.btnModificarUsuario.Size = new System.Drawing.Size(179, 23);
            this.btnModificarUsuario.TabIndex = 2;
            this.btnModificarUsuario.Text = "Modificar Usuario";
            this.btnModificarUsuario.UseVisualStyleBackColor = true;
            this.btnModificarUsuario.Click += new System.EventHandler(this.btnModificarUsuario_Click);
            // 
            // btnBajaUsuario
            // 
            this.btnBajaUsuario.Location = new System.Drawing.Point(12, 104);
            this.btnBajaUsuario.Name = "btnBajaUsuario";
            this.btnBajaUsuario.Size = new System.Drawing.Size(179, 23);
            this.btnBajaUsuario.TabIndex = 3;
            this.btnBajaUsuario.Text = "Baja Usuario";
            this.btnBajaUsuario.UseVisualStyleBackColor = true;
            this.btnBajaUsuario.Click += new System.EventHandler(this.btnBajaUsuario_Click);
            // 
            // btnListarUsuarios
            // 
            this.btnListarUsuarios.Location = new System.Drawing.Point(12, 133);
            this.btnListarUsuarios.Name = "btnListarUsuarios";
            this.btnListarUsuarios.Size = new System.Drawing.Size(179, 23);
            this.btnListarUsuarios.TabIndex = 4;
            this.btnListarUsuarios.Text = "Listar Usuarios";
            this.btnListarUsuarios.UseVisualStyleBackColor = true;
            this.btnListarUsuarios.Click += new System.EventHandler(this.btnListarUsuarios_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(205, 46);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(395, 155);
            this.dataGridView1.TabIndex = 5;
            // 
            // btnBkUp
            // 
            this.btnBkUp.Location = new System.Drawing.Point(12, 163);
            this.btnBkUp.Name = "btnBkUp";
            this.btnBkUp.Size = new System.Drawing.Size(179, 23);
            this.btnBkUp.TabIndex = 6;
            this.btnBkUp.Text = "Generar Back Up";
            this.btnBkUp.UseVisualStyleBackColor = true;
            this.btnBkUp.Click += new System.EventHandler(this.btnBkUp_Click);
            // 
            // logoutMS
            // 
            this.logoutMS.Name = "logoutMS";
            this.logoutMS.Size = new System.Drawing.Size(62, 20);
            this.logoutMS.Text = "Log Out";
            this.logoutMS.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // btnObtenerLogs
            // 
            this.btnObtenerLogs.Location = new System.Drawing.Point(12, 214);
            this.btnObtenerLogs.Name = "btnObtenerLogs";
            this.btnObtenerLogs.Size = new System.Drawing.Size(179, 23);
            this.btnObtenerLogs.TabIndex = 7;
            this.btnObtenerLogs.Text = "Obtener Logs DB";
            this.btnObtenerLogs.UseVisualStyleBackColor = true;
            this.btnObtenerLogs.Click += new System.EventHandler(this.btnObtenerLogs_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(205, 214);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(395, 147);
            this.listBox1.TabIndex = 8;
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 376);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnObtenerLogs);
            this.Controls.Add(this.btnBkUp);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnListarUsuarios);
            this.Controls.Add(this.btnBajaUsuario);
            this.Controls.Add(this.btnModificarUsuario);
            this.Controls.Add(this.btnAltaUsuario);
            this.Controls.Add(this.msHeader);
            this.MainMenuStrip = this.msHeader;
            this.Name = "frmAdmin";
            this.Text = "Administrador";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.msHeader.ResumeLayout(false);
            this.msHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAltaUsuario;
        private System.Windows.Forms.MenuStrip msHeader;
        private System.Windows.Forms.ToolStripMenuItem idiomaMS;
        private System.Windows.Forms.ToolStripMenuItem esARToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enUSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frFRToolStripMenuItem;
        private System.Windows.Forms.Button btnModificarUsuario;
        private System.Windows.Forms.Button btnBajaUsuario;
        private System.Windows.Forms.Button btnListarUsuarios;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnBkUp;
        private System.Windows.Forms.ToolStripMenuItem logoutMS;
        private System.Windows.Forms.Button btnObtenerLogs;
        private System.Windows.Forms.ListBox listBox1;
    }
}