﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Administracion
{
    public partial class frmBajaUsuario : Form
    {
        public frmBajaUsuario()
        {
            InitializeComponent();
        }

        private void frmBajaUsuario_Load(object sender, EventArgs e)
        {
            var coreUsuarios = new Core.GestionUsuario();
            comboBox2.DataSource = coreUsuarios.ObtenerUsuarios();
            comboBox2.DisplayMember = "NombreUsuario";
            comboBox2.ValueMember = "NombreUsuario";
            BuscarIdioma(Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado);
        }
        private void BuscarIdioma(string idioma)
        {
            var diccionarioDatos = new CoreServicios.GestionIdioma().ObtenerIdioma(idioma);
            foreach (KeyValuePair<string, string> Idioma in diccionarioDatos)
            {
                foreach (Control control in Controls)
                {
                    if (control.Name != "msHeader")
                    {
                        if (control.Name == Idioma.Key)
                        {
                            control.Text = Idioma.Value;
                        }
                    }
                    else
                    {
                        foreach (ToolStripItem item in msHeader.Items)
                        {
                            if (item.Name == Idioma.Key)
                            {
                                item.Text = Idioma.Value;
                            }
                        }
                    }
                }
            }
        }


        private void btnBajaUsuario_Click(object sender, EventArgs e)
        {
            var coreUsuarios = new Core.GestionUsuario();
            var user = (Usuario)comboBox2.SelectedItem;
            coreUsuarios.BajaUsuario(user);
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Seguridad.Singleton.Instance.Deslogear();
            var frmLogin = new frmLogin();
            frmLogin.Show();
            this.Close();
        }

        private void enUSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("en-US");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "en-US";
        }

        private void esARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("es-AR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "es-AR";
        }

        private void frFRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("fr-FR");
            Seguridad.Singleton.Instance.DevolverInstancia().IdiomaSeleccionado = "fr-FR";
        }
    }
}
