﻿using Core;
using CoreSeguridad;
using Entidades;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace UI
{
    public partial class frmLogin : Form
    {
        Usuario user = new Usuario();
        string Cultura = "en-US";
        public frmLogin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "")
            {
                if (textBox2.Text != "")
                {
                    var coreUsuario = new GestionSeguridad();
                    user.NombreUsuario = textBox1.Text;
                    user.Password = textBox2.Text;
                    List<Perfil> listaperfiles = new List<Perfil>();
                    user.Permisos = listaperfiles;
                    user.IdiomaSeleccionado = Cultura;
                    if (coreUsuario.Logearse(user) == true)
                    {
                        MessageBox.Show("Logeado");
                        foreach (Perfil permiso in user.Permisos)
                        {
                            switch (permiso.Descripcion)
                            {
                                case "Administrador":
                                    var frmAdm = new frmAdmin();
                                    frmAdm.Show();
                                    Hide();
                                    break;
                                case "Gerente Ventas":
                                    var frmVentas = new FrmMain();
                                    frmVentas.Show();
                                    break;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error de usuario o contraseña");
                    }
                }
                else
                {
                    MessageBox.Show("Por favor ingrese una contraseña");
                }
            }
            else
            {
                MessageBox.Show("Por favor ingrese un nombre de usuario");
            }

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BuscarIdioma("en-US");
        }

        private void BuscarIdioma(string idioma)
        {
            var diccionarioDatos = new CoreServicios.GestionIdioma().ObtenerIdioma(idioma);
            foreach(KeyValuePair<string, string> Idioma in diccionarioDatos)
            {
                foreach(Control control in Controls)
                {
                    if (control.Name != "msHeader")
                    {
                        if(control.Name == Idioma.Key)
                        {
                            control.Text = Idioma.Value;
                        }
                    }
                    else
                    {
                        foreach (ToolStripItem item in msHeader.Items)
                        {
                            if (item.Name == Idioma.Key)
                            {
                                item.Text = Idioma.Value;
                            }
                        }
                    }
                }
            }
        }

        private void esARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("es-AR");
            Cultura = ("es-AR");
        }

        private void enUSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("en-US");
            Cultura = ("en-US");

        }

        private void frFRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarIdioma("fr-FR");
            Cultura = ("fr-FR");

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
