﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreInformes
{
    public class BllInformes
    {
        public void GenerarInforme(EE.InformeMRP informe)
        {
            var dal = new DalInformes.DalInformes();
            dal.GenerarInforme(informe);
        }
        public EE.InformeMRP VerInforme(int Id)
        {
            var dal = new DalInformes.DalInformes();
            return dal.GetInforme(Id);
        }

    }
}
