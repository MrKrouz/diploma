﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIGenerarRoles
{
    public partial class AdminUIGenerarRoles : Form
    {
        public AdminUIGenerarRoles()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var coreSec = new CoreSec.CoreSec();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreSec.GetPerfilesBase();
            comboBox1.Text = string.Empty;
            var listaPermisos = new List<EE.Acceso>();
            foreach (var item in coreSec.GetPerfilesBase())
            {
                foreach (var item2 in item.GetRoles())
                {
                    listaPermisos.Add(item2);
                }
            }
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Descripcion";
            comboBox2.DataSource = listaPermisos;
            comboBox2.Text = string.Empty;
        }

        private void SacarItemsCMB(EE.Perfil perfil)
        {
            var coreSec = new CoreSec.CoreSec();
            var listaPermisos = new List<EE.Acceso>();
            foreach (var item in coreSec.GetPerfilesBase().Where(i => i.Id != perfil.Id))
            {
                if (listBox1.Items.IndexOf(item.Descripcion) == -1)
                {
                    foreach (var item2 in item.GetRoles())
                    {
                        listaPermisos.Add(item2);
                    }
                }
            }
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Descripcion";
            comboBox2.DataSource = listaPermisos;
            comboBox2.Text = string.Empty;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var item = (EE.Perfil)comboBox1.SelectedItem;
            if (listBox1.Items.IndexOf(item.Descripcion) == -1)
            {
                listBox1.Items.Add(item.Descripcion);
                SacarItemsCMB(item);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            var item = (EE.Permiso)comboBox2.SelectedItem;
            if (listBox1.Items.IndexOf(item.Descripcion) == -1)
            {
                listBox1.Items.Add(item.Descripcion);
            }
        }
        private void AgregarItemsCMB(EE.Acceso perfil)
        {
            var lista = (List<EE.Acceso>)comboBox2.DataSource;

            foreach (var item in perfil.GetRoles())
            {
                if (lista.Contains(item) == false)
                {
                    lista.Add(item);
                }
            }
            comboBox2.DataSource = null;
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Descripcion";
            comboBox2.DataSource = lista;
            comboBox2.Text = string.Empty;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            var lista = coreSec.GetPerfilesBase();
            foreach (var item in lista)
            {
                if (item.Descripcion == listBox1.SelectedItem.ToString())
                {
                    AgregarItemsCMB(item);
                }
            }
            listBox1.Items.Remove(listBox1.SelectedItem);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            EE.Perfil perfil = new EE.Perfil();
            perfil.Descripcion = textBox1.Text;
            var coreSec = new CoreSec.CoreSec();
            foreach(var item in coreSec.GetPerfilesBase())
            {
                if (listBox1.Items.Contains(item.Descripcion))
                {
                    perfil.AddRole(item);
                }
                foreach(var item2 in item.GetRoles())
                {
                    if (listBox1.Items.Contains(item2.Descripcion))
                    {
                        perfil.AddRole(item2);
                    }
                }
            }
            coreSec.AltaPerfil(perfil);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f=> f.Name == "AdminUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();
        }
    }
}
