﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalVentas
{
    public class dalVentas
    {
        #region Factura
        public void CargarFactura(EE.Factura factura)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().CargarFactura, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Importe", factura.ImporteTotal);
            cmd.Parameters.AddWithValue("@IdEmpleado", factura.Empleado.Id);
            cmd.Parameters.AddWithValue("@IdCliente", factura.Cliente.Id);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            factura.Id = (int)returnV.Value;
            cn.Close();
            foreach (var item in factura.Detalle)
            {
                CargarDetalleFactura(item, factura.Id);
            }
        }
        private void CargarDetalleFactura(EE.DetalleFactura detalle, int IdFactura)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().CargarDetalleFactura, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Cantidad", detalle.Cantidad);
            cmd.Parameters.AddWithValue("@Cantidad", detalle.PrecioTotal);
            cmd.Parameters.AddWithValue("@Cantidad", detalle.Mueble.Id);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            detalle.Id = (int)returnV.Value;
            cn.Close();
        }
        #endregion
        #region Cliente
        public void AddCliente(EE.Cliente cliente)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddCliente, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Nombre", cliente.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", cliente.Apellido);
            cmd.Parameters.AddWithValue("@Direccion", cliente.Direccion);
            cmd.Parameters.AddWithValue("@Dni", cliente.Dni);
            cmd.Parameters.AddWithValue("@Telefono", cliente.Telefono);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            cliente.Id = (int)returnV.Value;
            cn.Close();

        }
        public void ModificarCliente(EE.Cliente cliente)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().UpdCliente, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", cliente.Id);
            cmd.Parameters.AddWithValue("@Nombre", cliente.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", cliente.Apellido);
            cmd.Parameters.AddWithValue("@Direccion", cliente.Direccion);
            cmd.Parameters.AddWithValue("@Dni", cliente.Dni);
            cmd.Parameters.AddWithValue("@Telefono", cliente.Telefono);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        public List<EE.Cliente> GetClientes()
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetCliente, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            var lista = new List<EE.Cliente>();
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lista.Add(new EE.Cliente { Id = dr.GetInt32(0), Nombre = dr.GetString(1), Apellido = dr.GetString(2), Direccion = dr.GetString(3), Dni = dr.GetInt32(4), Telefono = dr.GetInt32(5) });
            }
            cn.Close();
            return lista;
        }
        #endregion
        #region Orden de Produccion
        public void GenerarOrdenDeProduccion (EE.OrdenDeProduccion op)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddOrdenProduccion, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdEmpleado", op.Empleado.Id);
            cmd.Parameters.AddWithValue("@Fecha", op.Fecha);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            cmd.ExecuteNonQuery();
            op.Id = (int)returnV.Value;
            cn.Close();
             foreach(var item in op.Detalle)
            {
                CargarDetalleOP(item, op.Id);
            }
        }
        private void CargarDetalleOP(EE.DetalleOrdenProduccion detalle, int op)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddDetalleOP, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdOp", op);
            cmd.Parameters.AddWithValue("@Cantidad", detalle.Cantidad);
            cmd.Parameters.AddWithValue("@IdMueble", detalle.Mueble.Id);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            cmd.ExecuteNonQuery();
            detalle.Id = (int)returnV.Value;
            cn.Close();
        }
        #endregion
    }
}
