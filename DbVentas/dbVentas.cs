﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbVentas
{
    public class dbVentas
    {
        #region Factura
        public void CargarFactura(EE.Factura factura)
        {
            try
            {

                var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
                var cmd = new SqlCommand(new Utilidades.StoredProcedures().CargarFactura, cn) { CommandType = System.Data.CommandType.StoredProcedure };
                cn.Open();
                cmd.Parameters.AddWithValue("@Importe", factura.ImporteTotal);
                cmd.Parameters.AddWithValue("@IdEmpleado", factura.Empleado.Id);
                cmd.Parameters.AddWithValue("@IdCliente", factura.Cliente.Id);
                SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
                returnV.Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add(returnV);
                cmd.ExecuteNonQuery();
                factura.Id = (int)returnV.Value;
                cn.Close();
                foreach (var item in factura.Detalle)
                {
                    CargarDetalleFactura(item, factura.Id);
                }
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error en la carga de factura: " + ex.Message, System.DateTime.Now);
            }
        }
        private void CargarDetalleFactura(EE.DetalleFactura detalle, int IdFactura)
        {
            try
            {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().CargarDetalleFactura, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Cantidad", detalle.Cantidad);
            cmd.Parameters.AddWithValue("@IdFactura", IdFactura);
            cmd.Parameters.AddWithValue("@PrecioTotal", detalle.PrecioTotal);
            cmd.Parameters.AddWithValue("@IdMueble", detalle.Mueble.Id);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            detalle.Id = (int)returnV.Value;
            cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error en la carga del detalle de factura: " + ex.Message, System.DateTime.Now);
            }
        }
        #endregion
        #region Cliente
        public void AddCliente(EE.Cliente cliente)
        {
            try
            {

            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddCliente, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Nombre", cliente.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", cliente.Apellido);
            cmd.Parameters.AddWithValue("@Direccion", cliente.Direccion);
            cmd.Parameters.AddWithValue("@Dni", cliente.Dni);
            cmd.Parameters.AddWithValue("@Telefono", cliente.Telefono);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            cliente.Id = (int)returnV.Value;
            cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error en la carga del cliente: " + ex.Message, System.DateTime.Now);
            }
        }
        public void ModificarCliente(EE.Cliente cliente)
        {
            try
            {

            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().UpdCliente, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", cliente.Id);
            cmd.Parameters.AddWithValue("@Nombre", cliente.Nombre);
            cmd.Parameters.AddWithValue("@Apellido", cliente.Apellido);
            cmd.Parameters.AddWithValue("@Direccion", cliente.Direccion);
            cmd.Parameters.AddWithValue("@Dni", cliente.Dni);
            cmd.Parameters.AddWithValue("@Telefono", cliente.Telefono);
            cmd.ExecuteNonQuery();
            cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error en la modificacion del cliente: " + ex.Message, System.DateTime.Now);
            }
        }
        public List<EE.Cliente> GetClientes()
        {
            try
            {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetCliente, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            var lista = new List<EE.Cliente>();
            cn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lista.Add(new EE.Cliente { Id = dr.GetInt32(0), Nombre = dr.GetString(1), Apellido = dr.GetString(2), Direccion = dr.GetString(3), Dni = dr.GetInt32(4), Telefono = dr.GetInt32(5) });
            }
            cn.Close();
            return lista;
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error en la peticion de clientes: " + ex.Message, System.DateTime.Now);
                return new List<EE.Cliente>();
            }
        }
        #endregion
        #region Orden de Produccion
        public void GenerarOrdenDeProduccion(EE.OrdenDeProduccion op)
        {
            try
            {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddOrdenProduccion, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdEmpleado", op.Empleado.Id);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            op.Id = (int)returnV.Value;
            cn.Close();
            foreach (var item in op.Detalle)
            {
                CargarDetalleOP(item, op.Id);
            }
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al generar la orden de produccion: " + ex.Message, System.DateTime.Now);

            }
        }
        private void CargarDetalleOP(EE.DetalleOrdenProduccion detalle, int op)
        {
            try
            {

            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().AddDetalleOP, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdOp", op);
            cmd.Parameters.AddWithValue("@Cantidad", detalle.Cantidad);
            cmd.Parameters.AddWithValue("@IdMueble", detalle.Mueble.Id);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            detalle.Id = (int)returnV.Value;
            cn.Close();
            }
            catch (SqlException ex)
            {
                new Utilidades.Logger().LogDB("Error al cargar el detalle de la orden de produccion: " + ex.Message, System.DateTime.Now);
            }
        }
        #endregion
    }
}
