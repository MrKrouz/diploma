﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EE;

namespace Home
{
    public partial class Home : Form
    {
        public Home()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var usuario = SecUtils.Singleton.Instance.GetInstance();
            if (usuario != null && usuario.Perfil.Descripcion.Contains("Admin"))
            {
                btnAdmin.Visible = true;   
                btnProduccion.Visible = true;
                btnVentas.Visible = true;
                btnInformes.Visible = true; 
            }
            if (usuario != null && usuario.Perfil.HasRole(usuario.Perfil, "Empleado Produccion"))
            {
                btnProduccion.Visible = true;
                btnInformes.Visible = true;
            }
            if (usuario != null && usuario.Perfil.HasRole(usuario.Perfil, "Empleado Ventas"))
            {
                btnVentas.Visible = true;
                btnInformes.Visible = true;
            }
            if(usuario != null)
            {
                SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);
            }
        }

        private void btnInformes_Click(object sender, EventArgs e)
        {
            var frm = new InformesUI.InformesUI();
            frm.Show();
            this.Hide();
        }

        private void btnProduccion_Click(object sender, EventArgs e)
        {
            var frm = new ProduccionUI.ProduccionUI();
            frm.Show();
            this.Hide();
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            var frm = new VentasUIHome.VentasUIHome();
            frm.Show();
            this.Hide();
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            var frm = new AdminUI.AdminUI();
            frm.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach(Form item in SecUtils.Singleton.watcher.DevolverSubscriptos())
            {
                item.Close();
            }
        }

        private void btnIdioma_Click(object sender, EventArgs e)
        {
            var frm = new HomeUICambiarIdioma.Form1();
            frm.Show();
            this.Hide();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
