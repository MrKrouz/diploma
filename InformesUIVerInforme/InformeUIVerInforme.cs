﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InformesUIVerInforme
{
    public partial class InformeUIVerInforme : Form
    {
        public InformeUIVerInforme()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void btnVerInforme_Click(object sender, EventArgs e)
        {
            var coreInfo = new CoreInformes.BllInformes();
            int numero;
            if (int.TryParse(textBox1.Text, out numero) == true && textBox1.Text != string.Empty)
            {
                var info = coreInfo.VerInforme(Convert.ToInt32(textBox1.Text));
                listBox1.Items.Add("Fecha generado:" + info.FechaGenerado.ToString());
                foreach (var item in info.Detalle)
                {
                    listBox1.Items.Add("Descripcion: " + item.Descripcion);
                    listBox1.Items.Add("Stock: " + item.Stock);
                    listBox1.Items.Add("Precio: " + item.Precio);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "InformesUI").Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var coreInfo = new CoreInformes.BllInformes();
            var informe = coreInfo.VerInforme(Convert.ToInt32(textBox1.Text));
            if (listBox1.Items.Count > 0)
            {

                string outputFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Informe -" + informe.Id.ToString() + ".pdf");
                using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (Document doc = new Document(PageSize.A4))
                    {
                        using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                        {
                            doc.Open();
                            PdfPTable t1 = new PdfPTable(4);
                            t1.DefaultCell.Border = 0;
                            t1.DefaultCell.BorderWidthBottom = 1;
                            t1.DefaultCell.BorderColorBottom = BaseColor.RED;
                            t1.AddCell("Codigo:");
                            t1.AddCell(informe.Id.ToString());
                            t1.AddCell("Fecha generado:");
                            t1.AddCell(informe.FechaGenerado.ToString());
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            t1.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            doc.Add(t1);
                            PdfPTable t2 = new PdfPTable(2);
                            t2.DefaultCell.Border = 0;
                            t2.DefaultCell.BorderWidthBottom = 1;
                            t2.DefaultCell.BorderColorBottom = BaseColor.RED;
                            foreach (var item in informe.Detalle)
                            {
                                t2.AddCell("Descripcion: ");
                                t2.AddCell(item.Descripcion);
                                t2.AddCell("Precio: ");
                                t2.AddCell(item.Precio.ToString());
                                t2.AddCell("Stock: ");
                                t2.AddCell(item.Stock.ToString());
                                t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                                t2.AddCell(new PdfPCell(new Phrase(" ")) { Border = 0 });
                            }
                            doc.Add(t2);
                            doc.Close();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Imposible generar informe sin elementos");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "InformesUI").Show();
            this.Close();
        }

        private void InformeUIVerInforme_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;

        }
    }
}
