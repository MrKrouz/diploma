﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EE;

namespace AdminUIAR
{
    public partial class AdminUIAR : Form
    {
        public AdminUIAR()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var coreSec = new CoreSec.CoreSec();
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = coreSec.GetUsersClean();
            comboBox1.DisplayMember = "UserName";
            comboBox1.Text = string.Empty;
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Descripcion";
            comboBox2.DataSource = coreSec.GetPerfiles();
            comboBox2.Text = string.Empty;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            coreSec.AsignarPerfil((EE.Perfil)comboBox2.SelectedItem, (EE.Usuario)comboBox1.SelectedItem);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var usuario = (EE.Usuario)comboBox1.SelectedItem;
            if(usuario != null)
            {
                comboBox2.SelectedValue = usuario.Perfil.Id;
            }
        }
    }
}
