﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoreLang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EE;

namespace Tests
{
    [TestClass()]
    public class TestIdioma
    {
        [TestMethod()]
        public void T_NewLang()
        {

            var lang = new Idioma("Español", new System.Globalization.CultureInfo("es-AR"));
            var palabra1 = new Palabra("Hola", "cb1");
            var palabra2 = new Palabra("Hola2", "cb2");
            var palabra3 = new Palabra("Hola3", "cb3");
            lang.AgregarPalabra(palabra1);
            lang.AgregarPalabra(palabra2);
            lang.AgregarPalabra(palabra3);
            var dalLang = new DBLang.DalLang();
            dalLang.NewLang(lang);
            var idiomaTest = dalLang.GetLangs().Where(l => l.ID == lang.ID).FirstOrDefault();
            var lista1 = lang.ListarPalabras();
            var lista2 = idiomaTest.ListarPalabras();
            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(lista1[i], lista2[i]);
            }
            Assert.AreEqual(lang, idiomaTest);

        }

        [TestMethod()]
        public void T_UpdLang()
        {
            var lang = new Idioma("Español", new System.Globalization.CultureInfo("es-AR"));
            var palabra1 = new Palabra("Hola", "cb1");
            var palabra2 = new Palabra("Hola2", "cb2");
            var palabra3 = new Palabra("Hola3", "cb3");
            lang.AgregarPalabra(palabra1);
            lang.AgregarPalabra(palabra2);
            lang.AgregarPalabra(palabra3);
            var dalLang = new DBLang.DalLang();

            dalLang.NewLang(lang);

            lang.Descripcion = "Ingles";
            lang.Cultura = System.Globalization.CultureInfo.CurrentCulture;
            lang.ListarPalabras()[0].Descripcion = "Hi1";
            lang.ListarPalabras()[1].Descripcion = "Hi2";
            lang.ListarPalabras()[2].Descripcion = "Hi3";

            dalLang.UpdLang(lang);

            var idiomaTest = dalLang.GetLangs().Where(l => l.ID == lang.ID).FirstOrDefault();
            var lista1 = lang.ListarPalabras();
            var lista2 = idiomaTest.ListarPalabras();

            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(lista1[i], lista2[i]);
            }
            Assert.AreEqual(lang, idiomaTest);

        }

        [TestMethod()]
        public void T_DelLang()
        {
            var lang = new Idioma("Español", new System.Globalization.CultureInfo("es-AR"));
            var palabra1 = new Palabra("Hola", "cb1");
            var palabra2 = new Palabra("Hola2", "cb2");
            var palabra3 = new Palabra("Hola3", "cb3");
            lang.AgregarPalabra(palabra1);
            lang.AgregarPalabra(palabra2);
            lang.AgregarPalabra(palabra3);
            var dalLang = new DBLang.DalLang();
            dalLang.NewLang(lang);
            dalLang.DelLang(lang.ID);
            Assert.IsNull(dalLang.GetLangs().Where(l => l.ID == lang.ID).FirstOrDefault());
        }
    }
}