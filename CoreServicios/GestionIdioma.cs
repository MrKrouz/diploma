﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreServicios
{
    public class GestionIdioma
    {
        public Dictionary<string, string> ObtenerIdioma(string idioma)
        {
            var db = new Repositorio_Servicios.RepositorioIdioma();
            return db.ObtenerIdioma(idioma);
        }
    }
}
