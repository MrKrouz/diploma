﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreServicios
{
    public class GestionBackUp
    {
        public void GenerarBackUp()
        {
            var datos = new Repositorio_Servicios.Datos();
            datos.GenerarBackUp();
        }
    }
}
