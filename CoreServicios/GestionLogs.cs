﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreServicios
{
    public class GestionLogs
    {
        public void GenerarLog(string mensaje, DateTime fecha)
        {
            var db = new Repositorio_Servicios.Datos();
            db.GenerarLog(mensaje, fecha);
        }

        public void GenerarChangeLog(string clase, string estadoAnterior, string estadoActual, string accion)
        {
            var db = new Repositorio_Servicios.Datos();
        }

        public List<string> ObtenerLogs()
        {
            var datos = new Repositorio_Servicios.Datos();
            return datos.obtenerLogs();
        }
    }
}
