﻿using DBLang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreLang
{
    public class CoreLang
    {

        public void NewLang(EE.Lenguaje Lenguaje)
        {
            new DalLang().NewLang(Lenguaje);
        }

        public void DelLang(int IdLang)
        {
            new DalLang().DelLang(IdLang);
        }

        public void UpdLang(EE.Lenguaje Lenguaje)
        {
            new DalLang().UpdLang(Lenguaje);
        }

        public List<EE.Idioma> GetLangs()
        {
            return new DalLang().GetLangs();
        }

        public EE.Idioma GetLangById(int IdLang)
        {
            return new DalLang().GetLangs().Where(id => id.ID == IdLang).FirstOrDefault();
        }

        public void ChangeLang(string Name)
        {
            if(SecUtils.Singleton.Instance.GetInstance() != null)
            SecUtils.Singleton.Instance.GetInstance().Idioma = this.GetLangs().Find(l => l.Descripcion == Name);
        }
    }
}
