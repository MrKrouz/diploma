﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class Login : FormBase.FormBase
    {
        public Login()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            if (coreSec.LogIn(textBox1.Text, textBox2.Text))
            {
                var frm = new Home.Home();
                frm.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Por favor revise los datos ingresados");
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
