﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIAddIdioma
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var lista = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures);
            comboBox1.ValueMember = "Name";
            comboBox1.DisplayMember = "Name";
            comboBox1.DataSource = lista;
            comboBox1.Text = string.Empty;
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void AltaIdioma_Click(object sender, EventArgs e)
        {
            var idioma = new EE.Idioma();
            idioma.Descripcion = txtNombreIdioma.Text;
            idioma.Cultura = (System.Globalization.CultureInfo)comboBox1.SelectedItem;
            idioma.AgregarPalabra(new EE.Palabra(textBox3.Text, "btnUpdMueble"));
            idioma.AgregarPalabra(new EE.Palabra(textBox4.Text, "btnAddMueble"));
            idioma.AgregarPalabra(new EE.Palabra(textBox5.Text, "btnGetMuebles"));
            idioma.AgregarPalabra(new EE.Palabra(textBox6.Text, "btnDelMueble"));
            idioma.AgregarPalabra(new EE.Palabra(textBox7.Text, "btnUpdMaterial"));
            idioma.AgregarPalabra(new EE.Palabra(textBox8.Text, "btnAddMaterial"));
            idioma.AgregarPalabra(new EE.Palabra(textBox9.Text, "btnGetMateriales"));
            idioma.AgregarPalabra(new EE.Palabra(textBox10.Text, "btnDelMaterial"));
            idioma.AgregarPalabra(new EE.Palabra(textBox11.Text, "btnGetOP"));
            idioma.AgregarPalabra(new EE.Palabra(textBox12.Text, "btnAddOC"));
            idioma.AgregarPalabra(new EE.Palabra(textBox13.Text, "btnBack"));
            idioma.AgregarPalabra(new EE.Palabra(textBox14.Text, "lblName"));
            idioma.AgregarPalabra(new EE.Palabra(textBox15.Text, "lblCosto"));
            idioma.AgregarPalabra(new EE.Palabra(textBox16.Text, "lblLargo"));
            idioma.AgregarPalabra(new EE.Palabra(textBox17.Text, "lblAlto"));
            idioma.AgregarPalabra(new EE.Palabra(textBox18.Text, "lblMadera"));
            idioma.AgregarPalabra(new EE.Palabra(textBox19.Text, "btnConfirmar"));
            idioma.AgregarPalabra(new EE.Palabra(textBox20.Text, "lblStock"));
            idioma.AgregarPalabra(new EE.Palabra(textBox21.Text, "btnEliminar"));
            idioma.AgregarPalabra(new EE.Palabra(textBox22.Text, "lblPrecio"));
            idioma.AgregarPalabra(new EE.Palabra(textBox23.Text, "lblUsuario"));
            idioma.AgregarPalabra(new EE.Palabra(textBox24.Text, "lblProfundidad"));
            idioma.AgregarPalabra(new EE.Palabra(textBox25.Text, "btnGenerar"));
            idioma.AgregarPalabra(new EE.Palabra(textBox26.Text, "btnQuitar"));
            idioma.AgregarPalabra(new EE.Palabra(textBox27.Text, "btnAdd"));
            idioma.AgregarPalabra(new EE.Palabra(textBox28.Text, "lblInforme"));
            idioma.AgregarPalabra(new EE.Palabra(textBox29.Text, "lblCantidad"));
            idioma.AgregarPalabra(new EE.Palabra(textBox30.Text, "lblMaterial"));
            idioma.AgregarPalabra(new EE.Palabra(textBox31.Text, "lblMueble"));
            idioma.AgregarPalabra(new EE.Palabra(textBox32.Text, "btnDetalle"));
            idioma.AgregarPalabra(new EE.Palabra(textBox33.Text, "lblFecha"));
            idioma.AgregarPalabra(new EE.Palabra(textBox34.Text, "lblOrdenes"));
            idioma.AgregarPalabra(new EE.Palabra(txtIdioma.Text, "Idioma"));
            bool flag = true;
            foreach (Control item in this.Controls)
            {
                if (item is TextBox)
                {
                    if (item.Text == string.Empty)
                    {
                        MessageBox.Show("Complete todos los campos");
                        flag = false;
                        break;
                    }
                }
            }
            if (flag == true)
            {
                var coreIdioma = new CoreLang.CoreLang();
                coreIdioma.NewLang(idioma);
            }
        }

        private void AtrasIdioma_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();
        }
    }
}
