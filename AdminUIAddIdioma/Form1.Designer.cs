﻿namespace AdminUIAddIdioma
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.AltaIdioma = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombreIdioma = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.btnUpdMueble = new System.Windows.Forms.Label();
            this.btnAddMueble = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btnGetMuebles = new System.Windows.Forms.Label();
            this.btnDelMueble = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.btnUpdMaterial = new System.Windows.Forms.Label();
            this.btnAddMaterial = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.btnGetMateriales = new System.Windows.Forms.Label();
            this.btnDelMaterial = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.btnGetOP = new System.Windows.Forms.Label();
            this.btnAddOC = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.lblCosto = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.lblLargo = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.lblAlto = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.lblMadera = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.btnConfirmar = new System.Windows.Forms.Label();
            this.lblStock = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.lblPrecio = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.lblProfundidad = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.btnGenerar = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.btnQuitar = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.lblInforme = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.btnEliminar = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.lblMueble = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.btnDetalle = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.lblFecha = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.lblOrdenes = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.AtrasIdioma = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txtIdioma = new System.Windows.Forms.TextBox();
            this.btnIdioma = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AltaIdioma
            // 
            this.AltaIdioma.Location = new System.Drawing.Point(84, 950);
            this.AltaIdioma.Name = "AltaIdioma";
            this.AltaIdioma.Size = new System.Drawing.Size(75, 23);
            this.AltaIdioma.TabIndex = 0;
            this.AltaIdioma.Text = "Alta";
            this.AltaIdioma.UseVisualStyleBackColor = true;
            this.AltaIdioma.Click += new System.EventHandler(this.AltaIdioma_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(81, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre idioma";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(81, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Codigo de cultura";
            // 
            // txtNombreIdioma
            // 
            this.txtNombreIdioma.Location = new System.Drawing.Point(286, 25);
            this.txtNombreIdioma.Name = "txtNombreIdioma";
            this.txtNombreIdioma.Size = new System.Drawing.Size(100, 20);
            this.txtNombreIdioma.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(286, 105);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 8;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(286, 78);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 7;
            // 
            // btnUpdMueble
            // 
            this.btnUpdMueble.AutoSize = true;
            this.btnUpdMueble.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdMueble.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdMueble.ForeColor = System.Drawing.Color.White;
            this.btnUpdMueble.Location = new System.Drawing.Point(81, 105);
            this.btnUpdMueble.Name = "btnUpdMueble";
            this.btnUpdMueble.Size = new System.Drawing.Size(122, 15);
            this.btnUpdMueble.TabIndex = 6;
            this.btnUpdMueble.Text = "Actualizar Mueble";
            // 
            // btnAddMueble
            // 
            this.btnAddMueble.AutoSize = true;
            this.btnAddMueble.BackColor = System.Drawing.Color.Transparent;
            this.btnAddMueble.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddMueble.ForeColor = System.Drawing.Color.White;
            this.btnAddMueble.Location = new System.Drawing.Point(81, 78);
            this.btnAddMueble.Name = "btnAddMueble";
            this.btnAddMueble.Size = new System.Drawing.Size(99, 15);
            this.btnAddMueble.TabIndex = 5;
            this.btnAddMueble.Text = "Nuevo Mueble";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(286, 158);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 12;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(286, 131);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 11;
            // 
            // btnGetMuebles
            // 
            this.btnGetMuebles.AutoSize = true;
            this.btnGetMuebles.BackColor = System.Drawing.Color.Transparent;
            this.btnGetMuebles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetMuebles.ForeColor = System.Drawing.Color.White;
            this.btnGetMuebles.Location = new System.Drawing.Point(81, 158);
            this.btnGetMuebles.Name = "btnGetMuebles";
            this.btnGetMuebles.Size = new System.Drawing.Size(102, 15);
            this.btnGetMuebles.TabIndex = 10;
            this.btnGetMuebles.Text = "Listar muebles";
            // 
            // btnDelMueble
            // 
            this.btnDelMueble.AutoSize = true;
            this.btnDelMueble.BackColor = System.Drawing.Color.Transparent;
            this.btnDelMueble.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelMueble.ForeColor = System.Drawing.Color.White;
            this.btnDelMueble.Location = new System.Drawing.Point(81, 131);
            this.btnDelMueble.Name = "btnDelMueble";
            this.btnDelMueble.Size = new System.Drawing.Size(113, 15);
            this.btnDelMueble.TabIndex = 9;
            this.btnDelMueble.Text = "Eliminar Mueble";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(286, 211);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 16;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(286, 184);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 15;
            // 
            // btnUpdMaterial
            // 
            this.btnUpdMaterial.AutoSize = true;
            this.btnUpdMaterial.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdMaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdMaterial.ForeColor = System.Drawing.Color.White;
            this.btnUpdMaterial.Location = new System.Drawing.Point(81, 211);
            this.btnUpdMaterial.Name = "btnUpdMaterial";
            this.btnUpdMaterial.Size = new System.Drawing.Size(127, 15);
            this.btnUpdMaterial.TabIndex = 14;
            this.btnUpdMaterial.Text = "Actualizar Material";
            // 
            // btnAddMaterial
            // 
            this.btnAddMaterial.AutoSize = true;
            this.btnAddMaterial.BackColor = System.Drawing.Color.Transparent;
            this.btnAddMaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddMaterial.ForeColor = System.Drawing.Color.White;
            this.btnAddMaterial.Location = new System.Drawing.Point(81, 184);
            this.btnAddMaterial.Name = "btnAddMaterial";
            this.btnAddMaterial.Size = new System.Drawing.Size(104, 15);
            this.btnAddMaterial.TabIndex = 13;
            this.btnAddMaterial.Text = "Nuevo Material";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(286, 259);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 20;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(286, 237);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 19;
            // 
            // btnGetMateriales
            // 
            this.btnGetMateriales.AutoSize = true;
            this.btnGetMateriales.BackColor = System.Drawing.Color.Transparent;
            this.btnGetMateriales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetMateriales.ForeColor = System.Drawing.Color.White;
            this.btnGetMateriales.Location = new System.Drawing.Point(81, 264);
            this.btnGetMateriales.Name = "btnGetMateriales";
            this.btnGetMateriales.Size = new System.Drawing.Size(115, 15);
            this.btnGetMateriales.TabIndex = 18;
            this.btnGetMateriales.Text = "Listar Materiales";
            // 
            // btnDelMaterial
            // 
            this.btnDelMaterial.AutoSize = true;
            this.btnDelMaterial.BackColor = System.Drawing.Color.Transparent;
            this.btnDelMaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelMaterial.ForeColor = System.Drawing.Color.White;
            this.btnDelMaterial.Location = new System.Drawing.Point(81, 237);
            this.btnDelMaterial.Name = "btnDelMaterial";
            this.btnDelMaterial.Size = new System.Drawing.Size(118, 15);
            this.btnDelMaterial.TabIndex = 17;
            this.btnDelMaterial.Text = "Eliminar Material";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(286, 312);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 24;
            this.textBox11.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(286, 285);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 23;
            this.textBox12.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // btnGetOP
            // 
            this.btnGetOP.AutoSize = true;
            this.btnGetOP.BackColor = System.Drawing.Color.Transparent;
            this.btnGetOP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetOP.ForeColor = System.Drawing.Color.White;
            this.btnGetOP.Location = new System.Drawing.Point(81, 317);
            this.btnGetOP.Name = "btnGetOP";
            this.btnGetOP.Size = new System.Drawing.Size(194, 15);
            this.btnGetOP.TabIndex = 22;
            this.btnGetOP.Text = "Listar ordenes de produccion";
            this.btnGetOP.Click += new System.EventHandler(this.label11_Click);
            // 
            // btnAddOC
            // 
            this.btnAddOC.AutoSize = true;
            this.btnAddOC.BackColor = System.Drawing.Color.Transparent;
            this.btnAddOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddOC.ForeColor = System.Drawing.Color.White;
            this.btnAddOC.Location = new System.Drawing.Point(81, 290);
            this.btnAddOC.Name = "btnAddOC";
            this.btnAddOC.Size = new System.Drawing.Size(160, 15);
            this.btnAddOC.TabIndex = 21;
            this.btnAddOC.Text = "Nueva orden de compra";
            this.btnAddOC.Click += new System.EventHandler(this.label12_Click);
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(286, 338);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 26;
            this.textBox13.TextChanged += new System.EventHandler(this.textBox13_TextChanged);
            // 
            // btnBack
            // 
            this.btnBack.AutoSize = true;
            this.btnBack.BackColor = System.Drawing.Color.Transparent;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(81, 343);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(39, 15);
            this.btnBack.TabIndex = 25;
            this.btnBack.Text = "Atras";
            this.btnBack.Click += new System.EventHandler(this.label13_Click);
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(286, 364);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 20);
            this.textBox14.TabIndex = 28;
            this.textBox14.TextChanged += new System.EventHandler(this.textBox14_TextChanged);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(81, 369);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(58, 15);
            this.lblName.TabIndex = 27;
            this.lblName.Text = "Nombre";
            this.lblName.Click += new System.EventHandler(this.label14_Click);
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(286, 390);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 20);
            this.textBox15.TabIndex = 30;
            this.textBox15.TextChanged += new System.EventHandler(this.textBox15_TextChanged);
            // 
            // lblCosto
            // 
            this.lblCosto.AutoSize = true;
            this.lblCosto.BackColor = System.Drawing.Color.Transparent;
            this.lblCosto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCosto.ForeColor = System.Drawing.Color.White;
            this.lblCosto.Location = new System.Drawing.Point(81, 395);
            this.lblCosto.Name = "lblCosto";
            this.lblCosto.Size = new System.Drawing.Size(43, 15);
            this.lblCosto.TabIndex = 29;
            this.lblCosto.Text = "Costo";
            this.lblCosto.Click += new System.EventHandler(this.label15_Click);
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(286, 525);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 20);
            this.textBox16.TabIndex = 40;
            // 
            // lblLargo
            // 
            this.lblLargo.AutoSize = true;
            this.lblLargo.BackColor = System.Drawing.Color.Transparent;
            this.lblLargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLargo.ForeColor = System.Drawing.Color.White;
            this.lblLargo.Location = new System.Drawing.Point(81, 526);
            this.lblLargo.Name = "lblLargo";
            this.lblLargo.Size = new System.Drawing.Size(44, 15);
            this.lblLargo.TabIndex = 39;
            this.lblLargo.Text = "Largo";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(286, 499);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 20);
            this.textBox17.TabIndex = 38;
            // 
            // lblAlto
            // 
            this.lblAlto.AutoSize = true;
            this.lblAlto.BackColor = System.Drawing.Color.Transparent;
            this.lblAlto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlto.ForeColor = System.Drawing.Color.White;
            this.lblAlto.Location = new System.Drawing.Point(81, 500);
            this.lblAlto.Name = "lblAlto";
            this.lblAlto.Size = new System.Drawing.Size(31, 15);
            this.lblAlto.TabIndex = 37;
            this.lblAlto.Text = "Alto";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(286, 473);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(100, 20);
            this.textBox18.TabIndex = 36;
            // 
            // lblMadera
            // 
            this.lblMadera.AutoSize = true;
            this.lblMadera.BackColor = System.Drawing.Color.Transparent;
            this.lblMadera.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadera.ForeColor = System.Drawing.Color.White;
            this.lblMadera.Location = new System.Drawing.Point(81, 474);
            this.lblMadera.Name = "lblMadera";
            this.lblMadera.Size = new System.Drawing.Size(56, 15);
            this.lblMadera.TabIndex = 35;
            this.lblMadera.Text = "Madera";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(286, 447);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(100, 20);
            this.textBox19.TabIndex = 34;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(286, 420);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(100, 20);
            this.textBox20.TabIndex = 33;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.AutoSize = true;
            this.btnConfirmar.BackColor = System.Drawing.Color.Transparent;
            this.btnConfirmar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.ForeColor = System.Drawing.Color.White;
            this.btnConfirmar.Location = new System.Drawing.Point(81, 448);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(70, 15);
            this.btnConfirmar.TabIndex = 32;
            this.btnConfirmar.Text = "Confirmar";
            // 
            // lblStock
            // 
            this.lblStock.AutoSize = true;
            this.lblStock.BackColor = System.Drawing.Color.Transparent;
            this.lblStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStock.ForeColor = System.Drawing.Color.White;
            this.lblStock.Location = new System.Drawing.Point(81, 421);
            this.lblStock.Name = "lblStock";
            this.lblStock.Size = new System.Drawing.Size(42, 15);
            this.lblStock.TabIndex = 31;
            this.lblStock.Text = "Stock";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(286, 603);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(100, 20);
            this.textBox22.TabIndex = 46;
            // 
            // lblPrecio
            // 
            this.lblPrecio.AutoSize = true;
            this.lblPrecio.BackColor = System.Drawing.Color.Transparent;
            this.lblPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecio.ForeColor = System.Drawing.Color.White;
            this.lblPrecio.Location = new System.Drawing.Point(81, 604);
            this.lblPrecio.Name = "lblPrecio";
            this.lblPrecio.Size = new System.Drawing.Size(48, 15);
            this.lblPrecio.TabIndex = 45;
            this.lblPrecio.Text = "Precio";
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(286, 577);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(100, 20);
            this.textBox23.TabIndex = 44;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(286, 551);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(100, 20);
            this.textBox24.TabIndex = 42;
            // 
            // lblProfundidad
            // 
            this.lblProfundidad.AutoSize = true;
            this.lblProfundidad.BackColor = System.Drawing.Color.Transparent;
            this.lblProfundidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfundidad.ForeColor = System.Drawing.Color.White;
            this.lblProfundidad.Location = new System.Drawing.Point(81, 552);
            this.lblProfundidad.Name = "lblProfundidad";
            this.lblProfundidad.Size = new System.Drawing.Size(85, 15);
            this.lblProfundidad.TabIndex = 41;
            this.lblProfundidad.Text = "Profundidad";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(286, 760);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(100, 20);
            this.textBox25.TabIndex = 58;
            // 
            // btnGenerar
            // 
            this.btnGenerar.AutoSize = true;
            this.btnGenerar.BackColor = System.Drawing.Color.Transparent;
            this.btnGenerar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerar.ForeColor = System.Drawing.Color.White;
            this.btnGenerar.Location = new System.Drawing.Point(81, 760);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(59, 15);
            this.btnGenerar.TabIndex = 57;
            this.btnGenerar.Text = "Generar";
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(286, 734);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(100, 20);
            this.textBox26.TabIndex = 56;
            // 
            // btnQuitar
            // 
            this.btnQuitar.AutoSize = true;
            this.btnQuitar.BackColor = System.Drawing.Color.Transparent;
            this.btnQuitar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitar.ForeColor = System.Drawing.Color.White;
            this.btnQuitar.Location = new System.Drawing.Point(81, 734);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(46, 15);
            this.btnQuitar.TabIndex = 55;
            this.btnQuitar.Text = "Quitar";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(286, 708);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(100, 20);
            this.textBox27.TabIndex = 54;
            // 
            // btnAdd
            // 
            this.btnAdd.AutoSize = true;
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(81, 708);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(57, 15);
            this.btnAdd.TabIndex = 53;
            this.btnAdd.Text = "Agregar";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(286, 682);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(100, 20);
            this.textBox28.TabIndex = 52;
            // 
            // lblInforme
            // 
            this.lblInforme.AutoSize = true;
            this.lblInforme.BackColor = System.Drawing.Color.Transparent;
            this.lblInforme.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInforme.ForeColor = System.Drawing.Color.White;
            this.lblInforme.Location = new System.Drawing.Point(81, 682);
            this.lblInforme.Name = "lblInforme";
            this.lblInforme.Size = new System.Drawing.Size(56, 15);
            this.lblInforme.TabIndex = 51;
            this.lblInforme.Text = "Informe";
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(286, 656);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(100, 20);
            this.textBox29.TabIndex = 50;
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.BackColor = System.Drawing.Color.Transparent;
            this.lblCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.ForeColor = System.Drawing.Color.White;
            this.lblCantidad.Location = new System.Drawing.Point(81, 656);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(64, 15);
            this.lblCantidad.TabIndex = 49;
            this.lblCantidad.Text = "Cantidad";
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(286, 629);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(100, 20);
            this.textBox30.TabIndex = 48;
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaterial.ForeColor = System.Drawing.Color.White;
            this.lblMaterial.Location = new System.Drawing.Point(81, 630);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(60, 15);
            this.lblMaterial.TabIndex = 47;
            this.lblMaterial.Text = "Material";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(286, 786);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 20);
            this.textBox21.TabIndex = 60;
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.BackColor = System.Drawing.Color.Transparent;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.Location = new System.Drawing.Point(81, 786);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(61, 15);
            this.btnEliminar.TabIndex = 59;
            this.btnEliminar.Text = "Eliminar";
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(286, 812);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(100, 20);
            this.textBox31.TabIndex = 62;
            // 
            // lblMueble
            // 
            this.lblMueble.AutoSize = true;
            this.lblMueble.BackColor = System.Drawing.Color.Transparent;
            this.lblMueble.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMueble.ForeColor = System.Drawing.Color.White;
            this.lblMueble.Location = new System.Drawing.Point(81, 812);
            this.lblMueble.Name = "lblMueble";
            this.lblMueble.Size = new System.Drawing.Size(55, 15);
            this.lblMueble.TabIndex = 61;
            this.lblMueble.Text = "Mueble";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(286, 838);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(100, 20);
            this.textBox32.TabIndex = 64;
            // 
            // btnDetalle
            // 
            this.btnDetalle.AutoSize = true;
            this.btnDetalle.BackColor = System.Drawing.Color.Transparent;
            this.btnDetalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetalle.ForeColor = System.Drawing.Color.White;
            this.btnDetalle.Location = new System.Drawing.Point(81, 838);
            this.btnDetalle.Name = "btnDetalle";
            this.btnDetalle.Size = new System.Drawing.Size(53, 15);
            this.btnDetalle.TabIndex = 63;
            this.btnDetalle.Text = "Detalle";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(286, 864);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(100, 20);
            this.textBox33.TabIndex = 70;
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.BackColor = System.Drawing.Color.Transparent;
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.ForeColor = System.Drawing.Color.White;
            this.lblFecha.Location = new System.Drawing.Point(81, 864);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(46, 15);
            this.lblFecha.TabIndex = 69;
            this.lblFecha.Text = "Fecha";
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(286, 890);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(100, 20);
            this.textBox34.TabIndex = 68;
            // 
            // lblOrdenes
            // 
            this.lblOrdenes.AutoSize = true;
            this.lblOrdenes.BackColor = System.Drawing.Color.Transparent;
            this.lblOrdenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrdenes.ForeColor = System.Drawing.Color.White;
            this.lblOrdenes.Location = new System.Drawing.Point(81, 890);
            this.lblOrdenes.Name = "lblOrdenes";
            this.lblOrdenes.Size = new System.Drawing.Size(61, 15);
            this.lblOrdenes.TabIndex = 67;
            this.lblOrdenes.Text = "Ordenes";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.White;
            this.lblUsuario.Location = new System.Drawing.Point(81, 578);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(57, 15);
            this.lblUsuario.TabIndex = 65;
            this.lblUsuario.Text = "Usuario";
            // 
            // AtrasIdioma
            // 
            this.AtrasIdioma.Location = new System.Drawing.Point(255, 950);
            this.AtrasIdioma.Name = "AtrasIdioma";
            this.AtrasIdioma.Size = new System.Drawing.Size(75, 23);
            this.AtrasIdioma.TabIndex = 71;
            this.AtrasIdioma.Text = "Atras";
            this.AtrasIdioma.UseVisualStyleBackColor = true;
            this.AtrasIdioma.Click += new System.EventHandler(this.AtrasIdioma_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(286, 51);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 21);
            this.comboBox1.TabIndex = 72;
            // 
            // txtIdioma
            // 
            this.txtIdioma.Location = new System.Drawing.Point(286, 916);
            this.txtIdioma.Name = "txtIdioma";
            this.txtIdioma.Size = new System.Drawing.Size(100, 20);
            this.txtIdioma.TabIndex = 74;
            // 
            // btnIdioma
            // 
            this.btnIdioma.AutoSize = true;
            this.btnIdioma.BackColor = System.Drawing.Color.Transparent;
            this.btnIdioma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIdioma.ForeColor = System.Drawing.Color.Transparent;
            this.btnIdioma.Location = new System.Drawing.Point(81, 916);
            this.btnIdioma.Name = "btnIdioma";
            this.btnIdioma.Size = new System.Drawing.Size(51, 15);
            this.btnIdioma.TabIndex = 73;
            this.btnIdioma.Text = "Idioma";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::AdminUIAddIdioma.Properties.Resources.fondo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(472, 337);
            this.Controls.Add(this.txtIdioma);
            this.Controls.Add(this.btnIdioma);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.AtrasIdioma);
            this.Controls.Add(this.textBox33);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.textBox34);
            this.Controls.Add(this.lblOrdenes);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.textBox32);
            this.Controls.Add(this.btnDetalle);
            this.Controls.Add(this.textBox31);
            this.Controls.Add(this.lblMueble);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.textBox25);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.textBox26);
            this.Controls.Add(this.btnQuitar);
            this.Controls.Add(this.textBox27);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.textBox28);
            this.Controls.Add(this.lblInforme);
            this.Controls.Add(this.textBox29);
            this.Controls.Add(this.lblCantidad);
            this.Controls.Add(this.textBox30);
            this.Controls.Add(this.lblMaterial);
            this.Controls.Add(this.textBox22);
            this.Controls.Add(this.lblPrecio);
            this.Controls.Add(this.textBox23);
            this.Controls.Add(this.textBox24);
            this.Controls.Add(this.lblProfundidad);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.lblLargo);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.lblAlto);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.lblMadera);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.lblStock);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.lblCosto);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.btnGetOP);
            this.Controls.Add(this.btnAddOC);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.btnGetMateriales);
            this.Controls.Add(this.btnDelMaterial);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.btnUpdMaterial);
            this.Controls.Add(this.btnAddMaterial);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.btnGetMuebles);
            this.Controls.Add(this.btnDelMueble);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.btnUpdMueble);
            this.Controls.Add(this.btnAddMueble);
            this.Controls.Add(this.txtNombreIdioma);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AltaIdioma);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Alta idioma";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AltaIdioma;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombreIdioma;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label btnUpdMueble;
        private System.Windows.Forms.Label btnAddMueble;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label btnGetMuebles;
        private System.Windows.Forms.Label btnDelMueble;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label btnUpdMaterial;
        private System.Windows.Forms.Label btnAddMaterial;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label btnGetMateriales;
        private System.Windows.Forms.Label btnDelMaterial;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label btnGetOP;
        private System.Windows.Forms.Label btnAddOC;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label btnBack;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label lblCosto;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label lblLargo;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label lblAlto;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label lblMadera;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label btnConfirmar;
        private System.Windows.Forms.Label lblStock;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label lblPrecio;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label lblProfundidad;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label btnGenerar;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label btnQuitar;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label btnAdd;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label lblInforme;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label btnEliminar;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label lblMueble;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label btnDetalle;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label lblOrdenes;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Button AtrasIdioma;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox txtIdioma;
        private System.Windows.Forms.Label btnIdioma;
    }
}

