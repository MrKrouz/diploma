﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIUpdMat
{
    public partial class ProduccionUIUpdMat : FormBase.FormBase
    {
        public ProduccionUIUpdMat()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            ActualizarCB();
        }

        private void ActualizarCB()
        {
            var coreProd = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = coreProd.GetMateriales();
            comboBox1.Text = string.Empty;
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox3.Text = string.Empty;
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var coreProd = new CoreProduccion.bllProduccion();
            var mat = coreProd.GetMateriales().Find(m => m.Id == (int)comboBox1.SelectedValue);
            textBox1.Text = mat.Descripcion;
            textBox2.Text = mat.Precio.ToString();
            textBox3.Text = mat.Stock.ToString();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            #region ValidacionDeCampos
            bool flag = true;
            if (textBox1.Text == string.Empty)
            {
                flag = false;
                MessageBox.Show("Debe ingresar una descripcion");
            }
            double item = 0;
            if (Double.TryParse(textBox2.Text, out item) == false)
            {
                flag = false;
                MessageBox.Show("Debe ingresar un numero");
            }
            int item2 = 0;
            if (int.TryParse(textBox3.Text, out item2) == false)
            {
                flag = false;
                MessageBox.Show("Debe ingresar un numero");
            }
            #endregion
            if (flag == true)
            {
                var coreProd = new CoreProduccion.bllProduccion();
                coreProd.UpdMaterial(new EE.MateriaPrima { Id = (int)comboBox1.SelectedValue, Descripcion = textBox1.Text, Precio = Convert.ToDouble(textBox2.Text), Stock = Convert.ToInt32(textBox3.Text) });
                ActualizarCB();
                MessageBox.Show("Material actualizado");
            }
        }
        private void CerrarForm()
        {

            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CerrarForm();
        }
    }
}
