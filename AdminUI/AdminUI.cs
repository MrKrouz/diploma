﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdminUIGI;

namespace AdminUI
{
    public partial class AdminUI : FormBase.FormBase
    {
        public AdminUI()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new AdminUIAddIdioma.Form1();
            frm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new AdminUIGI.AdminUIGI();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new AdminUIAR.AdminUIAR();
            frm.Show();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new AdminUIDB.AdminUIDB();
            frm.Show();
        }

        private void AdminUI_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f=> f.Name == "Home").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
