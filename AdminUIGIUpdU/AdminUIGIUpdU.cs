﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIGIUpdU
{
    public partial class AdminUIGIUpdU : Form
    {
        public AdminUIGIUpdU()
        {
            InitializeComponent();
        }

        private void AdminUIGIUpdU_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "UserName";
            comboBox2.DataSource = new CoreSec.CoreSec().GetUsers();
            comboBox2.Text = string.Empty;
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = new CoreLang.CoreLang().GetLangs();
            comboBox1.Text = string.Empty;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            var usuario = coreSec.GetUsers().Where(u => u.Id == (int)comboBox2.SelectedValue).FirstOrDefault();
            textBox2.Text = new SecUtils.Crypto().DesEncriptar(usuario.Password);
            textBox3.Text = usuario.Nombre;
            textBox4.Text = usuario.Apellido;
            textBox5.Text = usuario.Dni.ToString();
            comboBox1.SelectedValue = usuario.Idioma.ID;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            #region validacion de campos
            var user = new EE.Usuario();
            bool flag = true;
            if (textBox2.Text != string.Empty)
            {
                user.Password = new SecUtils.Crypto().Encriptar(textBox2.Text);

            }
            else
            {
                MessageBox.Show("Ingrese la contraseña");
                flag = false;

            }
            if (textBox3.Text != string.Empty)
            {
                user.Nombre = textBox3.Text;
            }
            else
            {
                MessageBox.Show("Ingrese el nombre");
                flag = false;

            }
            if (textBox4.Text != string.Empty)
            {
                user.Apellido = textBox4.Text;
            }
            else
            {
                MessageBox.Show("Ingrese el apellido");
                flag = false;

            }
            int numero;
            if (textBox5.Text != string.Empty && int.TryParse(textBox5.Text, out numero) == true)
            {
                user.Dni = numero;
            }
            else
            {
                MessageBox.Show("Ingrese el dni sin puntos ni guiones");
                flag = false;

            }
            if (comboBox1.Text != string.Empty)
            {
                user.Idioma = (EE.Idioma)comboBox1.SelectedItem;
            }
            else
            {
                MessageBox.Show("Seleccione un idioma base");
                flag = false;
            }
            #endregion
            if (flag == true)
            {
                var coreSec = new CoreSec.CoreSec();
                coreSec.UpdUser(user);
                MessageBox.Show("Usuario actualizado");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUIMid").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }
    }
}
