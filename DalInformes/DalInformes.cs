﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalInformes
{
    public class DalInformes
    {
        public void GenerarInforme(EE.InformeMRP Informe)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GenerarInforme, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdUsuario", 1);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            Informe.Id = (int)returnV.Value;
            cn.Close();
            foreach (var item in Informe.Detalle)
            {
                CargarDetalle(item, Informe.Id);
            }
        }
        private void CargarDetalle(EE.DetalleMRP detalle, int IdInforme)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().CargarDetalle, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@IdInforme", IdInforme);
            cmd.Parameters.AddWithValue("@IdElemento", detalle.Id);
            cmd.Parameters.AddWithValue("@Precio", detalle.Precio);
            cmd.Parameters.AddWithValue("@Stock", detalle.Stock);
            cmd.Parameters.AddWithValue("@Detalle", detalle.Descripcion);
            SqlParameter returnV = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            returnV.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnV);
            cmd.ExecuteNonQuery();
            detalle.IdDetalle = (int)returnV.Value;
            cn.Close();
        }
        public EE.InformeMRP GetInforme(int Id)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetInforme, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            EE.InformeMRP informe = new EE.InformeMRP();
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", Id);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                informe.Id = dr.GetInt32(0);
                informe.FechaGenerado = dr.GetDateTimeOffset(1).DateTime;
            }
            cn.Close();
            GetDetalles(informe);
            return informe;
        }
        private void GetDetalles(EE.InformeMRP informe)
        {
            var cn = new SqlConnection(new Utilidades.ConnectionString().GetConnection());
            var cmd = new SqlCommand(new Utilidades.StoredProcedures().GetDetalle, cn) { CommandType = System.Data.CommandType.StoredProcedure };
            cn.Open();
            cmd.Parameters.AddWithValue("@Id", informe.Id);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                informe.Detalle.Add(new EE.DetalleMRP { IdDetalle = dr.GetInt32(0), Id = dr.GetInt32(1), Descripcion = dr.GetString(2), Precio = dr.GetDouble(3), Stock = dr.GetInt32(4) });
            }
            cn.Close();
        }

    }
}
