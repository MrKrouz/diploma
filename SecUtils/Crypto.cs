﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.IO;

namespace SecUtils
{
    public class Crypto
    {
        private static byte[] getKey()
        {
            return new byte[] { 173, 112, 113, 177, 85, 138, 219, 112, 229, 99, 46, 117, 105, 194, 84, 90, 212, 171, 104, 197, 21, 165, 89, 12, 70, 139, 57, 196, 91, 121, 235, 32 };
        }
        private static byte[] getIV()
        {
            return new byte[] { 183, 193, 45, 135, 55, 13, 146, 44, 136, 153, 114, 230, 126, 242, 17, 30 };
        }
        public string CalculateMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
        public byte[] Encriptar(string texto)
        {
            if (texto == null || texto.Length <= 0) throw new ArgumentNullException("texto");
            byte[] encrypted;
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = getKey();
                aesAlg.IV = getIV();
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(texto);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }
        public string DesEncriptar(byte[] texto)
        {
            if (texto == null || texto.Length <= 0) throw new ArgumentNullException("texto");
            string plainText = null;
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = getKey();
                aesAlg.IV = getIV();

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(texto))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plainText = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plainText;
        }
    }
}
