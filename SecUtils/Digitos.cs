﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;

namespace SecUtils
{
    public class Digitos
    {
        public string GenerarDVHs(EE.Usuario user)
        {
            return new Crypto().CalculateMD5Hash(user.ToString());
        }

        public string GenerarDVV(List<EE.Usuario> users)
        {
            string final = "";
            foreach (var item in users)
            {
                final += GenerarDVHs(item);
            }
            return new Crypto().CalculateMD5Hash(final);
        }
    }
}
