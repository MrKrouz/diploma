﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EE;

namespace SecUtils
{
    public sealed class Singleton
    {
        Singleton()
        {
        }
        private static readonly object padlock = new object();
        private static Singleton instance = null;
        public static EE.Observador watcher = new Observador();
        private Usuario userInstance = null;
        public static Singleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Singleton();
                    }
                    return instance;
                }
            }
        }
        public void LogIn(Usuario user)
        {
            this.userInstance = user;
        }
        public void LogOut()
        {
            this.userInstance = null;
        }
        public Usuario GetInstance()
        {
            return this.userInstance;
        }


    }
}
