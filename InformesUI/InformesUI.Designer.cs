﻿namespace InformesUI
{
    partial class InformesUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InformesUI));
            this.btnMateriaPrima = new System.Windows.Forms.Button();
            this.btnMuebles = new System.Windows.Forms.Button();
            this.btnVerInforme = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMateriaPrima
            // 
            this.btnMateriaPrima.Location = new System.Drawing.Point(43, 29);
            this.btnMateriaPrima.Name = "btnMateriaPrima";
            this.btnMateriaPrima.Size = new System.Drawing.Size(137, 23);
            this.btnMateriaPrima.TabIndex = 0;
            this.btnMateriaPrima.Text = "Informe materia prima";
            this.btnMateriaPrima.UseVisualStyleBackColor = true;
            this.btnMateriaPrima.Click += new System.EventHandler(this.btnMateriaPrima_Click);
            // 
            // btnMuebles
            // 
            this.btnMuebles.Location = new System.Drawing.Point(43, 58);
            this.btnMuebles.Name = "btnMuebles";
            this.btnMuebles.Size = new System.Drawing.Size(137, 23);
            this.btnMuebles.TabIndex = 1;
            this.btnMuebles.Text = "Informe muebles";
            this.btnMuebles.UseVisualStyleBackColor = true;
            this.btnMuebles.Click += new System.EventHandler(this.btnMuebles_Click);
            // 
            // btnVerInforme
            // 
            this.btnVerInforme.Location = new System.Drawing.Point(43, 88);
            this.btnVerInforme.Name = "btnVerInforme";
            this.btnVerInforme.Size = new System.Drawing.Size(137, 23);
            this.btnVerInforme.TabIndex = 2;
            this.btnVerInforme.Text = "Ver informe";
            this.btnVerInforme.UseVisualStyleBackColor = true;
            this.btnVerInforme.Click += new System.EventHandler(this.btnVerInforme_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(43, 117);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(137, 27);
            this.btnBack.TabIndex = 27;
            this.btnBack.Text = "Atras";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // InformesUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::InformesUI.Properties.Resources.fondo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(222, 168);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnVerInforme);
            this.Controls.Add(this.btnMuebles);
            this.Controls.Add(this.btnMateriaPrima);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InformesUI";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.InformesUI_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMateriaPrima;
        private System.Windows.Forms.Button btnMuebles;
        private System.Windows.Forms.Button btnVerInforme;
        private System.Windows.Forms.Button btnBack;
    }
}

