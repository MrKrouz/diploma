﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InformesUI
{
    public partial class InformesUI : Form
    {
        public InformesUI()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void btnMateriaPrima_Click(object sender, EventArgs e)
        {
            var frm = new InformesUIMP.InformesUIMP();
            frm.Show();
            this.Hide();
        }

        private void btnMuebles_Click(object sender, EventArgs e)
        {
            var frm = new InformesUIMueble.InformesUIMueble();
            frm.Show();
            this.Hide();
        }

        private void btnVerInforme_Click(object sender, EventArgs e)
        {
            var frm = new InformesUIVerInforme.InformeUIVerInforme();
            frm.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "Home").Show();
            this.Hide();
        }

        private void InformesUI_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;

        }
    }
}
