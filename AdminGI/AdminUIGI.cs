﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EE;

namespace AdminUIGI
{
    public partial class AdminUIGI : FormBase.FormBase
    {

        Usuario user = new Usuario();

        public AdminUIGI()
        {
            InitializeComponent();
        }

        private void AdminUIGI_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = new CoreLang.CoreLang().GetLangs();
            comboBox1.Text = string.Empty;
        }

        private void btnProfile_Click(object sender, EventArgs e)
        {

        }

        private void btnAsignar_Click(object sender, EventArgs e)
        {
            //var perfilNuevo = new Perfil();
            //perfilNuevo.Descripcion = txtperfil.Text;
            //foreach (TreeNode item in treeView1.Nodes)
            //{
            //    if (item.Checked)
            //    {
            //        var perfilAdd = new Perfil();
            //        perfilAdd.Descripcion = item.Text;
            //        foreach (TreeNode item2 in item.Nodes)
            //        {
            //            if (item2.Checked)
            //            {
            //                var permiso = new Permiso();
            //                permiso.Descripcion = item2.Text;
            //                perfilAdd.AddRole(permiso);
            //            }
            //        }
            //        perfilNuevo.AddRole(perfilAdd);
            //    }
            //}
            //user.Perfil = perfilNuevo;
            //var coreSec = new CoreSec.CoreSec();
            //if (coreSec.GetPerfiles().Find(p=> p.Descripcion == perfilNuevo.Descripcion)!= null)
            //{
            //    perfilNuevo.Id = coreSec.GetPerfiles().Find(p=> p.Descripcion == perfilNuevo.Descripcion).Id;
            //    coreSec.ModificarPerfil(perfilNuevo);
            //}
            //else
            //{
            //    coreSec.AltaPerfil(perfilNuevo, user.Id);
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUIMid").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            #region validacion de campos

            bool flag = true;
            if (textBox1.Text != string.Empty)
            {
                user.UserName = textBox1.Text;
            }
            else
            {
                MessageBox.Show("Ingrese el nombre de usuario");
                flag = false;

            }
            if (textBox2.Text != string.Empty)
            {
                user.Password = new SecUtils.Crypto().Encriptar(textBox2.Text);

            }
            else
            {
                MessageBox.Show("Ingrese la contraseña");
                flag = false;

            }
            if (textBox3.Text != string.Empty)
            {
                user.Nombre = textBox3.Text;
            }
            else
            {
                MessageBox.Show("Ingrese el nombre");
                flag = false;

            }
            if (textBox4.Text != string.Empty)
            {
                user.Apellido = textBox4.Text;
            }
            else
            {
                MessageBox.Show("Ingrese el apellido");
                flag = false;

            }
            int numero;
            if (textBox5.Text != string.Empty && int.TryParse(textBox5.Text, out numero) == true)
            {
                user.Dni = numero;
            }
            else
            {
                MessageBox.Show("Ingrese el dni sin puntos ni guiones");
                flag = false;

            }
            if (comboBox1.Text != string.Empty)
            {
                user.Idioma = (EE.Idioma)comboBox1.SelectedItem;
            }
            else
            {
                MessageBox.Show("Seleccione un idioma base");
                flag = false;
            }
            #endregion
            if (flag == true)
            {
                var coreSec = new CoreSec.CoreSec();
                coreSec.AddUser(user);
                MessageBox.Show("Usuario generado");
            }
        }

        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
        }

        private void btnLang_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }
    }
}
