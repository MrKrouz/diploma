﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIDB
{
    public partial class AdminUIDB : Form
    {
        public AdminUIDB()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            coreSec.CreateBackUp();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var coreSec = new CoreSec.CoreSec();
            coreSec.RestoreBackUp();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }

        private void AdminUIDB_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;

        }
    }
}
