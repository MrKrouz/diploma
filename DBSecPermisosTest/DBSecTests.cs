﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DBSec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass()]
    public class DBSecTests
    {
        [TestMethod()]
        public void AltaPerfil()
        {
            EE.Perfil perfil = new EE.Perfil();
            EE.Perfil perfil2 = new EE.Perfil();
            EE.Perfil perfil3 = new EE.Perfil();
            EE.Permiso permiso = new EE.Permiso();
            EE.Permiso permiso2 = new EE.Permiso();
            EE.Permiso permiso3= new EE.Permiso();
            EE.Permiso permiso4 = new EE.Permiso();

            perfil.Descripcion = "Primer Perfil";
            perfil2.Descripcion = "Segundo Perfil";
            perfil3.Descripcion = "Tercer Perfil";

            permiso.Descripcion = "Primer permiso";
            permiso2.Descripcion = "Segundo permiso";
            permiso3.Descripcion = "Tercer permiso";
            permiso4.Descripcion = "Cuarto permiso";

            perfil.AddRole(perfil);
            perfil.AddRole(perfil2);
            perfil2.AddRole(permiso2);
            perfil2.AddRole(permiso3);
            perfil2.AddRole(perfil3);
            perfil3.AddRole(permiso4);
        }
    }
}