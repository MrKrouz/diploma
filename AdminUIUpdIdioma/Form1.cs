﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminUIUpdIdioma
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            var lista = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures);
            comboBox1.ValueMember = "Name";
            comboBox1.DisplayMember = "Name";
            comboBox1.DataSource = lista;
            comboBox1.Text = string.Empty;

            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Descripcion";
            comboBox2.DataSource = new CoreLang.CoreLang().GetLangs();
            comboBox2.Text = string.Empty;
        }

        private void AltaIdioma_Click(object sender, EventArgs e)
        {
            var idioma = (EE.Idioma)comboBox2.SelectedItem;
            idioma.Cultura = (System.Globalization.CultureInfo)comboBox1.SelectedItem;
            idioma.ListarPalabras().Where(p => p.Control == "btnUpdMueble").FirstOrDefault().Descripcion = textBox3.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnAddMueble").FirstOrDefault().Descripcion = textBox4.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnGetMuebles").FirstOrDefault().Descripcion = textBox5.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnDelMueble").FirstOrDefault().Descripcion = textBox6.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnUpdMaterial").FirstOrDefault().Descripcion = textBox7.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnAddMaterial").FirstOrDefault().Descripcion = textBox8.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnGetMateriales").FirstOrDefault().Descripcion = textBox9.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnDelMaterial").FirstOrDefault().Descripcion = textBox10.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnGetOP").FirstOrDefault().Descripcion = textBox11.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnAddOC").FirstOrDefault().Descripcion = textBox12.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnBack").FirstOrDefault().Descripcion = textBox13.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblName").FirstOrDefault().Descripcion = textBox14.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblCosto").FirstOrDefault().Descripcion = textBox15.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblLargo").FirstOrDefault().Descripcion = textBox16.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblAlto").FirstOrDefault().Descripcion = textBox17.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblMadera").FirstOrDefault().Descripcion = textBox18.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnConfirmar").FirstOrDefault().Descripcion = textBox19.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblStock").FirstOrDefault().Descripcion = textBox20.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnEliminar").FirstOrDefault().Descripcion = textBox21.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblPrecio").FirstOrDefault().Descripcion = textBox22.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblUsuario").FirstOrDefault().Descripcion = textBox23.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblProfundidad").FirstOrDefault().Descripcion = textBox24.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnGenerar").FirstOrDefault().Descripcion = textBox25.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnQuitar").FirstOrDefault().Descripcion = textBox26.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnAdd").FirstOrDefault().Descripcion = textBox27.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblInforme").FirstOrDefault().Descripcion = textBox28.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblCantidad").FirstOrDefault().Descripcion = textBox29.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblMaterial").FirstOrDefault().Descripcion = textBox30.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblMueble").FirstOrDefault().Descripcion = textBox31.Text;
            idioma.ListarPalabras().Where(p => p.Control == "btnDetalle").FirstOrDefault().Descripcion = textBox32.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblFecha").FirstOrDefault().Descripcion = textBox33.Text;
            idioma.ListarPalabras().Where(p => p.Control == "lblOrdenes").FirstOrDefault().Descripcion = textBox34.Text;
            idioma.ListarPalabras().Where(p => p.Control == "Idioma").FirstOrDefault().Descripcion = txtIdioma.Text;
            bool flag = true;
            foreach (Control item in this.Controls)
            {
                if (item is TextBox)
                {
                    if (item.Text == string.Empty)
                    {
                        MessageBox.Show("Complete todos los campos");
                        flag = false;
                        break;
                    }
                }
            }
            if (flag == true)
            {
                var coreIdioma = new CoreLang.CoreLang();
                coreIdioma.UpdLang(idioma);
            }

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            EE.Idioma idioma = (EE.Idioma)comboBox2.SelectedItem;
            try
            {
                comboBox1.SelectedItem = idioma.Cultura;
                textBox3.Text = idioma.ListarPalabras().Where(p => p.Control == "btnUpdMueble").FirstOrDefault().Descripcion;
                textBox4.Text = idioma.ListarPalabras().Where(p => p.Control == "btnAddMueble").FirstOrDefault().Descripcion;
                textBox5.Text = idioma.ListarPalabras().Where(p => p.Control == "btnGetMuebles").FirstOrDefault().Descripcion;
                textBox6.Text = idioma.ListarPalabras().Where(p => p.Control == "btnDelMueble").FirstOrDefault().Descripcion;
                textBox7.Text = idioma.ListarPalabras().Where(p => p.Control == "btnUpdMaterial").FirstOrDefault().Descripcion;
                textBox8.Text = idioma.ListarPalabras().Where(p => p.Control == "btnAddMaterial").FirstOrDefault().Descripcion;
                textBox9.Text = idioma.ListarPalabras().Where(p => p.Control == "btnGetMateriales").FirstOrDefault().Descripcion;
                textBox10.Text = idioma.ListarPalabras().Where(p => p.Control == "btnDelMaterial").FirstOrDefault().Descripcion;
                textBox11.Text = idioma.ListarPalabras().Where(p => p.Control == "btnGetOP").FirstOrDefault().Descripcion;
                textBox12.Text = idioma.ListarPalabras().Where(p => p.Control == "btnAddOC").FirstOrDefault().Descripcion;
                textBox13.Text = idioma.ListarPalabras().Where(p => p.Control == "btnBack").FirstOrDefault().Descripcion;
                textBox14.Text = idioma.ListarPalabras().Where(p => p.Control == "lblName").FirstOrDefault().Descripcion;
                textBox15.Text = idioma.ListarPalabras().Where(p => p.Control == "lblCosto").FirstOrDefault().Descripcion;
                textBox16.Text = idioma.ListarPalabras().Where(p => p.Control == "lblLargo").FirstOrDefault().Descripcion;
                textBox17.Text = idioma.ListarPalabras().Where(p => p.Control == "lblAlto").FirstOrDefault().Descripcion;
                textBox18.Text = idioma.ListarPalabras().Where(p => p.Control == "lblMadera").FirstOrDefault().Descripcion;
                textBox19.Text = idioma.ListarPalabras().Where(p => p.Control == "btnConfirmar").FirstOrDefault().Descripcion;
                textBox20.Text = idioma.ListarPalabras().Where(p => p.Control == "lblStock").FirstOrDefault().Descripcion;
                textBox21.Text = idioma.ListarPalabras().Where(p => p.Control == "btnEliminar").FirstOrDefault().Descripcion;
                textBox22.Text = idioma.ListarPalabras().Where(p => p.Control == "lblPrecio").FirstOrDefault().Descripcion;
                textBox23.Text = idioma.ListarPalabras().Where(p => p.Control == "lblUsuario").FirstOrDefault().Descripcion;
                textBox24.Text = idioma.ListarPalabras().Where(p => p.Control == "lblProfundidad").FirstOrDefault().Descripcion;
                textBox25.Text = idioma.ListarPalabras().Where(p => p.Control == "btnGenerar").FirstOrDefault().Descripcion;
                textBox26.Text = idioma.ListarPalabras().Where(p => p.Control == "btnQuitar").FirstOrDefault().Descripcion;
                textBox27.Text = idioma.ListarPalabras().Where(p => p.Control == "btnAdd").FirstOrDefault().Descripcion;
                textBox28.Text = idioma.ListarPalabras().Where(p => p.Control == "lblInforme").FirstOrDefault().Descripcion;
                textBox29.Text = idioma.ListarPalabras().Where(p => p.Control == "lblCantidad").FirstOrDefault().Descripcion;
                textBox30.Text = idioma.ListarPalabras().Where(p => p.Control == "lblMaterial").FirstOrDefault().Descripcion;
                textBox31.Text = idioma.ListarPalabras().Where(p => p.Control == "lblMueble").FirstOrDefault().Descripcion;
                textBox32.Text = idioma.ListarPalabras().Where(p => p.Control == "btnDetalle").FirstOrDefault().Descripcion;
                textBox33.Text = idioma.ListarPalabras().Where(p => p.Control == "lblFecha").FirstOrDefault().Descripcion;
                textBox34.Text = idioma.ListarPalabras().Where(p => p.Control == "lblOrdenes").FirstOrDefault().Descripcion;
                txtIdioma.Text = idioma.ListarPalabras().Where(p => p.Control == "Idioma").FirstOrDefault().Descripcion;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AtrasIdioma_Click(object sender, EventArgs e)
        {

        }

        private void AtrasIdioma_Click_1(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "AdminUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);
            this.Close();

        }
    }
}
