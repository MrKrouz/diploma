﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProduccionUIDelMueble
{
    public partial class ProduccionUIDelMueble : FormBase.FormBase
    {
        public ProduccionUIDelMueble()
        {
            SecUtils.Singleton.watcher.Subscribirse(this);
            InitializeComponent();
            SecUtils.Singleton.watcher.ActualizarForms(SecUtils.Singleton.Instance.GetInstance().Idioma);

        }

        private void ActualizarCB()
        {
            CoreProduccion.bllProduccion core = new CoreProduccion.bllProduccion();
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Descripcion";
            comboBox1.DataSource = core.GetMuebles();
            comboBox1.Text = string.Empty;
        }

        private void btnDelMat_Click(object sender, EventArgs e)
        {
            CoreProduccion.bllProduccion core = new CoreProduccion.bllProduccion();
            if (comboBox1.Text != string.Empty)
            {
                core.DelMueble(new EE.Mueble { Id = (int)comboBox1.SelectedValue });
                ActualizarCB();
                MessageBox.Show("Mueble eliminado");
            }
            else
            {
                MessageBox.Show("Se debe seleccionar un material para eliminar");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SecUtils.Singleton.watcher.DevolverSubscriptos().Find(f => f.Name == "ProduccionUI").Show();
            SecUtils.Singleton.watcher.DeSubscribirse(this);

            this.Close();
        }

        private void ProduccionUIDelMueble_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
            ActualizarCB();
        }
    }
}
