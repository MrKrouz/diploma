﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreProduccion
{
    public class bllProduccion
    {
        #region Compra

        public void GenerarOrdenDeCompra(EE.OrdenDeCompra OrdenDeCompra)
        {
            new DalProduccion.DalProduccion().GenerarOrdenDeCompra(OrdenDeCompra);
        }
        //public void EliminarOrdenDeCompra(EE.OrdenDeCompra OrdenDeCompra)
        //{
        //    new DalProduccion.DalProduccion().EliminarOrdenDeCompra(OrdenDeCompra);
        //}
        //public void ModificarOrdenDeCompra(EE.OrdenDeCompra OrdenDeCompra)
        //{
        //    new DalProduccion.DalProduccion().ModificarOrdenDeCompra(OrdenDeCompra);
        //}
        //public List<EE.OrdenDeCompra> ListarOrdenesDeCompra()
        //{
        //    var lst = new DalProduccion.DalProduccion().ListarOrdenesDeCompra();
        //    foreach (var item in lst)
        //    {
        //        foreach (var item2 in item.Detalle)
        //        {
        //            item2.Material = GetMateriales().Find(m => m.Id == item2.Material.Id);
        //        }
        //    }
        //    return lst;
        //}
        public List<EE.OrdenDeProduccion> ListarOrdenesDeProduccion()
        {
            var lista = new DalProduccion.DalProduccion().ListarOrdenesDeProduccion();
            var sec = new CoreSec.CoreSec();
            foreach(var item in lista)
            {
                item.Empleado = sec.GetUsers().Find(u => u.Id == item.Empleado.Id);
                foreach(var item2 in item.Detalle)
                {
                    item2.Mueble = GetMuebles().Find(m => m.Id == item2.Mueble.Id);
                }
            }
            return lista;
        }
        #endregion
        #region Material
        public void AddMaterial(EE.MateriaPrima MP)
        {
            new DalProduccion.DalProduccion().AddMaterial(MP);
        }
        public void UpdMaterial(EE.MateriaPrima MP)
        {
            new DalProduccion.DalProduccion().UpdMaterial(MP);
        }
        public void DelMaterial(EE.MateriaPrima MP)
        {
            new DalProduccion.DalProduccion().DelMaterial(MP);
        }
        public List<EE.MateriaPrima> GetMateriales()
        {
            return new DalProduccion.DalProduccion().GetMateriales();
        }
        #endregion
        #region Muebles
        public void AddMueble(EE.Mueble Mueble)
        {
            new DalProduccion.DalProduccion().AddMueble(Mueble);
        }
        public void UpdMueble(EE.Mueble Mueble)
        {
            new DalProduccion.DalProduccion().UpdMueble(Mueble);
        }
        public void DelMueble(EE.Mueble Mueble)
        {
            new DalProduccion.DalProduccion().DelMueble(Mueble);
        }
        public List<EE.Mueble> GetMuebles()
        {
            return new DalProduccion.DalProduccion().GetMuebles();
        }
        #endregion
    }
}
